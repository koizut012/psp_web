<?php
$thisurl=dirname(__FILE__);require_once($_SERVER['DOCUMENT_ROOT'].'/common/inc/config.php');
$page_include_title = '会社概要｜東京・大阪・名古屋での恋活・婚活パーティー・街コンならPREMIUM STATUS PARTY';
$page_include_description = '当社は、婚活・恋活パーティーや1店舗型街コンを東京・横浜・大阪・神戸・京都・名古屋・札幌・福岡で行っております。男性はハイステイタスの方対象となっています。';
$page_include_keywords = '婚活パーティー,恋活,街コン,出会い,東京';
$page_include_robots = '';
$page_include_canonical = '/company/';
$page_include_h1 = '会社概要 | 東京・大阪・名古屋での婚活・恋活パーティー・街コンはプレミアムステイタスパーティー';
$localCSS = array(
	ROOT.'/s_css/company.css',
);
$localJS = array(
	//ROOT.'/js/index.js',
);

//ページカテゴリ
$page_cat = 'company';

// パンくず
	$topicpath = array(
 	array(
 		'name' => '会社概要',//名前
 		'href' => '/company/',//パス
 		'count' => '2',//階層※2階層目から
 	),
 );
?>

	<?php require_once('common/inc/top.php');?>
	<article id="Company" class="company">
		<header class="base">
			<h2 class="page_tit">
				<img src="<?php echo ROOT;?>/img/company/title.png" width="147" height="93" alt="会社概要">
			</h2>
		</header>
		<div class="contentsWrap">
			<div class="mod_wrap04">
				<div class="contents">
					<div class="table_box">
						<dl>
							<dt>会社名</dt>
							<dd><a href="http://www.fandr.jp/" target="_blank">株式会社フュージョン アンド リレーションズ</a></dd>
						</dl>
						<dl>
							<dt>所在地</dt>
							<dd>〒162-0821<br>東京都新宿区津久戸町3番19号えひらビル2階</dd>
						</dl>
						<dl>
							<dt>連絡先</dt>
							<dd>
								<dl class="inline">
									<dt>電話：</dt>
									<dd><a data-tel="03-5206-8220">03-5206-8220</a></dd>
									<dt>Fax：</dt>
									<dd>03-3260-3890</dd>
								</dl>
							</dd>
						</dl>
						<dl>
							<dt>営業時間</dt>
							<dd>11:00〜20:00 <br>月曜火曜定休日<br>※祝日の場合は営業</dd>
						</dl>
						<dl>
							<dt>会社URL</dt>
							<dd>
								<ul>
									<li><a href="http://www.fandr.jp/" target="_blank">http://www.fandr.jp/</a></li>
								</ul>
							</dd>
						</dl>
						<dl>
							<dt>サービス一覧</dt>
							<dd>
								<ul class="link_list">
									<li><a href="http://www.fandr.jp/" target="_blank"><span>株式会社フュージョン アンド リレーションズ</span></a><br>カンパニーサイト</li>
									<li><a href="http://www.statusparty.jp/" target="_blank"><span>プレミアムステイタス</span></a><br>立食フリー形式の婚活・恋活パーティー</li>
									<li><a href="http://www.fandr.jp/service/dears_party" target="_blank"><span>ディアーズパーティー</span></a><br>着席全員会話形式の婚活パーティー<br>※URLは2017年7月以降に公開予定</li>
									<li><a href="http://mrmiss.asia/" target="_blank"><span>ミスター＆ミス</span></a><br>写真審査通過者限定のマッチングサービス</li>
									<li><a href="http://statusclub.jp/" target="_blank"><span>プレミアムメンバーズ</span></a><br>写真審査通過者限定の結婚相手紹介サービス</li>
									<li><a href="http://www.jieitaiclub.jp/" target="_blank"><span>自衛隊クラブ</span></a><br>20代中心自衛隊員・防衛生大生限定</li>
									<li><a href="http://jieitai-bridal.jp/" target="_blank"><span>自衛隊ブライダル</span></a><br>30代中心自衛隊員限定婚活サービス</li>
									<li><a href="http://www.jieitaiclub.jp/bestbody/" target="_blank"><span>自衛隊ベストボディ部</span></a><br>自衛隊員・防衛大生限定の美ボディコンテスト＆サークル</li>
									<li><a href="http://www.w-premium.jp/" target="_blank"><span>ダブルプレミアム</span></a><br>客室乗務員（CA）vsハイステイタス男性</li>
									<li><a href="http://breeze-club.jp/" target="_blank"><span>ブリーズクラブ</span></a><br>男性30代40代中心社会人サークル</li>
									<li><a href="https://1000.style/" target="_blank"><span>プレミアムエリート</span></a><br>年収1000万円以上vs審査通過28歳以下女性</li>
									<li><a href="http://privateclub.jp/" target="_blank"><span>フュージョンパーティー＆ブライダル</span></a><br>1年以内婚約結婚希望者向け</li>
									<li><a href="https://white-p.jp" target="_blank"><span>ホワイトパートナーズ</span></a><br>女性看護師と男性ハイステイタス限定の恋愛・婚活マッチングサービス</li>
								</ul>
							</dd>
						</dl>
						<dl>
							<dt>設立</dt>
							<dd>2007年7月17日</dd>
						</dl>
						<dl>
							<dt>資本金</dt>
							<dd>9,900,000円</dd>
						</dl>
						<dl>
							<dt>役員</dt>
							<dd>
								<dl class="inline">
									<dt>代表取締役社長：</dt>
									<dd>有井 清次</dd>
								</dl>
								<dl class="inline">
									<dt>代表取締役：</dt>
									<dd>林 圭一</dd>
								</dl>
								<dl class="inline">
									<dt>取締役：</dt>
									<dd>島岡 学</dd>
								</dl>
							</dd>
						</dl>
						<dl>
							<dt>事業内容</dt>
							<dd>
								<ol class="num_list">
									<li>パーティー・合コンの企画、製作及びコーディネート</li>
									<li>イベント請負及びプロデュース</li>
									<li>ブライダルプロデュース、関連の映像製作及び編集</li>
									<li>会員制クラブの運用</li>
									<li>集客代理店業</li>
									<li>広告業および広告代理店業</li>
									<li>インターネットによるポータルサイト運営及び通信販売</li>
									<li>前各号に付帯する一切の業務</li>
								</ol>
							</dd>
						</dl>
					</div>
				</div>
			</div>
		</div>
	</article>
	<?php require_once('common/inc/bottom.php');?>