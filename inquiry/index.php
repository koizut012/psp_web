<?php
$thisurl=dirname(__FILE__);require_once($_SERVER['DOCUMENT_ROOT'].'/common/inc/config.php');
$page_include_title = 'お問い合わせ｜婚活パーティー・恋活パーティーならPREMIUM STATUS PARTY';
$page_include_description = 'お問い合わせの確認画面です。「プレミアムステイタス」の婚活パーティー、恋活パーティーにご興味がある方はお気軽にお問い合わせください。プライベートライフの向上を目的としたハイステイタス対象の出会いパーティーを開催いたしております。';
$page_include_keywords = '婚活パーティー,恋活パーティー,出会い,東京,大阪';
$page_include_robots = '';
$page_include_canonical = '/inquiry/';
$page_include_h1 = 'お問い合わせ | 東京、大阪、名古屋での婚活パーティー・恋活パーティーなら、プレミアムステイタスパーティー';
$localCSS = array(
	ROOT.'/s_css/inquiry.css',
);
$localJS = array(
	//ROOT.'/js/index.js',
);

//ページカテゴリ
$page_cat = 'inquiry';

// パンくず
	$topicpath = array(
 	array(
 		'name' => 'お問い合わせ',//名前
 		'href' => '/inquiry/',//パス
 		'count' => '2',//階層※2階層目から
 	),
 );
?>

	<?php require_once('common/inc/top.php');?>
	<article id="Inquiry" class="inquiry">
		<header class="base">
			<h2 class="page_tit">
				<img src="<?php echo ROOT;?>/img/inquiry/title.png" width="218" height="93" alt="お問い合わせ">
			</h2>
		</header>

		<div class="contentsWrap">
			<div class="mod_wrap04">
				<form id="Form" autocomplete="on" enctype="multipart/form-data" name="form" method="post" action="./" class="formArea">
					<div class="rowWrap">
						<dl class="element" data-need="" data-form-unique="radio_sex">
							<dt><span>性別</span></dt>
							<dd>
								<div class="itemWrap radioWrap radio_sexBox">
									<span class="radio">
									<input type="radio" name="radio_sex"  value="男性" id="radio_sex0">
									<label for="radio_sex0"><span>男性</span></label>
									</span>
									<span class="radio">
									<input type="radio" name="radio_sex"  value="女性" id="radio_sex1">
									<label for="radio_sex1"><span>女性</span></label>
									</span>
								</div>
							</dd>
						</dl>
					</div>

					<div class="rowWrap">
						<dl class="element" data-need="" data-form-unique="text_mail">
							<dt><span>Eメール</span></dt>
							<dd>
								<div class="itemWrap textWrap text_mailBox w03 spW01"><input type="email" name="text_mail" id="text_mail" value="" size="15" autocomplete="email" placeholder="例：info@00000（半角英数字）"></div>
								<p class="info">※ 携帯でドメイン指定受信を設定の方はstatusparty.jp又はinfo@statusparty.jpをご登録お願いします。<br> ※お返事には最大2営業日頂戴しておりますが、万が一返答がない場合は、お手数ですが下記迄お問合せください。
								</p>
							</dd>
						</dl>
					</div>

					<div class="rowWrap">
						<dl class="element" data-form-unique="textarea_inq">
							<dt><span>ご質問等</span></dt>
							<dd>
								<div class="itemWrap textareaWrap textarea_inqBox"><textarea name="textarea_inq" id="textarea_inq" rows="10" cols="40" placeholder=""></textarea></div>
							</dd>
						</dl>
					</div>

					<div class="link">
						<label for="ConfirmBtn" class="Confirm mod_btn07">
						<input id="ConfirmBtn" value="入力内容を確認する" type="submit">
					</label>
					</div>
				</form>
			</div>
		</div>

	</article>
	<?php require_once('common/inc/bottom.php');?>