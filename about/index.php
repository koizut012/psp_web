<?php
$thisurl=dirname(__FILE__);require_once($_SERVER['DOCUMENT_ROOT'].'/common/inc/config.php');
$page_include_title = 'プレミアムステイタスとは？｜婚活パーティー・1店舗型街コン・ビジネスエリートとの出会いはPREMIUM STATUS PARTY';
$page_include_description = '東京・大阪・名古屋でのハイステイタス対象の婚活＆恋活パーティーと1店舗型街コンなら「PREMIUM STATUS PARTY」。上場企業・外資企業・経営者・医師・公務員などを対象とした出会いパーティーを開催しております。ご予約はお気軽に。';
$page_include_keywords = '婚活パーティー,街コン,恋活パーティー,出会い,東京,大阪';
$page_include_robots = '';
$page_include_canonical = '/about/';
$page_include_h1 = 'PREMIUM STATUS PARTYのご紹介 | 東京・大阪・名古屋での婚活&恋活パーティー、街コン、ビジネスエリートとの出会いはプレミアムステイタスパーティー';
$localCSS = array (
	ROOT.'/s_css/about.css',
);
$localJS = array(
	ROOT.'/js/about.js',
);

//ページカテゴリ
$page_cat = 'about';

// パンくず
 $topicpath = array(
	array(
		'name' => 'プレミアムステイタスとは',//名前
		'href' => '/about/',//パス
		'count' => '2',//階層※2階層目から
	),
);
?>

	<?php require_once('common/inc/top.php');?>
	<article>
		<header class="mv" data-lf-area data-lf-pc="<?php echo ROOT;?>/img/about/mv.jpg" data-lf-sp="<?php echo ROOT;?>/img/about/mv_sp.jpg">
			<h2 class="page_tit">
				<picture>
					<source srcset="<?php echo ROOT;?>/img/about/title_sp.png" <?php echo SC_SP;?>>
					<img src="<?php echo ROOT;?>/img/about/title.png" width="388" height="93" alt="プレミアムステイタスとは">
				</picture>
			</h2>
		</header>

		<div class="contentsWrap">
			<div class="mod_wrap01">
				<p class="catch">海外のプールサイドパーティーが原点<br>ビジネスエリートの<br data-sc-sp>プライベート充実がコンセプト</p>
				<section class="sec_concept">
					<div class="wrap">
						<h3 class="sec_tit02 concept"><span>初めての方</span></h3>
						<p>プレミアムステイタスパーティーは、大手企業・外資企業・経営者・医師・会計士・税理士・公務員・自衛隊員など社会の第一線で活躍されているビジネスエリートの男性、OL・看護師・美容系・客室乗務員・アパレル系等の女性のプライベート充実を目的としています。<br> 職場以外での異性の友達作り、婚活、恋活の機会をお届けするスタイリッシュな婚活・恋活パーティー＆1店舗型街コンのブランドです。
							<br> 東京・横浜・大阪・神戸・京都・名古屋・仙台・札幌・福岡やその他主要エリアにて開催しています。
							<br> デザイナーズレストラン、ダイニングバー、ゲストハウス、ラウンジなどの非日常的な空間を使用していますので、シチュエーションやシーズナリティもお楽しみ頂けます。
							<br> 女性の方はご友人の結婚式や披露宴以外で、普段よりドレスアップ出来る場としてもご利用ください。
							<br> 初めて参加される方や、お一人でご参加される方にも安心してお越しいただけるように、スタッフがアテンダーとなって、しっかりとエスコート致します。
							<br> フレンドリーなおもてなしで、心から楽しんでいただける出会いや婚活の機会、素敵な空間をご提供いたします。立食フリー形式の婚活＆恋活パーティー（1店舗型街コン）が中心となりますが、着席全員会話型のパーティーも開催しています。
							<br> お一人でも参加出来ますので、是非、一度お越しください。
						</p>
					</div>
					<figure data-lf-area>
						<div class="ph main" data-lf="<?php echo ROOT;?>/img/about/concept_ph01.jpg"></div>
						<div class="ph sub" data-lf="<?php echo ROOT;?>/img/about/concept_ph02.jpg"></div>
					</figure>
				</section>
			</div>

			<section class="sec_greeting">
				<div class="in">
					<div class="mod_wrap01">
						<div class="wrap">
							<h3 class="sec_tit02 greeting"><span>弊社代表の挨拶</span></h3>
							<p>私が会社員時代に海外出張へ行った際、会合後のアフターパーティーとして、リゾートホテルなどで開催されるプールサイドパーティーやテラスがあるレストランでの食事会によく招待頂きました。プールサイドでカッコいいバーテンダーがカクテルを作り、シンガーがジャズやボサノバを歌う、といったシチュエーションでのビジネス交流で、本当に感動した覚えがあります。<br> まるで映画のワンシーンのようで、すごくお洒落だなーとカルチャーショックを受けました。
								<br> 日本でも、そんな非日常の空間で素敵な方々と時間を過ごせたら、どんなに楽しいだろうという思いがあって、プレミアムステイタスパーティーを立ち上げました。
								<br> 普段、仕事に頑張りすぎてる方も多いと思いますが、たまには当社のパーティーで、ワクワクする時間や素敵な出会い、シチュエーションを楽しんで頂ければと思います。
							</p>
							<figure>
								<div class="ph" data-lf-area data-lf="<?php echo ROOT;?>/img/about/greeting_ph.jpg"></div>
								<figcaption>株式会社フュージョンアンドリレーションズ　<br data-sc-sp>代表取締役社長　<br data-sc-sp>有井清次</figcaption>
							</figure>
						</div>
					</div>
				</div>
			</section>

			<section class="sec_point">
				<header>
					<h3><img src="<?php echo ROOT;?>/img/about/point_petan.png" alt="プレミアムステイタスの特徴"></h3>
					<p class="read">現在、婚活パーティー・恋活パーティー・街コンなど、<br data-sc-pc> 多くの出会いパーティーが開催されていますが、
						<br data-sc-pc> プレミアムステイタスパーティーの特徴をまとめてみました。
						<br> 初めて当サイトにお越し頂いた方は是非ご覧ください。
					</p>
				</header>

				<div class="in">
					<div class="mod_wrap01">
						<div class="point_box">

							<div class="detail_box p01">
								<div class="wrap">
									<h3 class="title">男性はハイステイタスの方限定</h3>
									<p class="txt">男性の参加資格を上場企業・大手企業・外資企業・公務員・医師・経営者・弁護士・会計士・税理士の方などハイステイタス／ビジネスエリートに限定しています。エリート企画を開催しているところはありますが、当社は全ての企画に男性参加資格を設けています。</p>
								</div>
								<figure data-lf-area data-lf="<?php echo ROOT;?>/img/about/point_ph01.jpg" class="ph"></figure>
							</div>

							<div class="detail_box p02">
								<div class="wrap">
									<h3 class="title">資格証・身分証100％提示</h3>
									<p class="txt">男性は資格を限定しているだけでなく、それを証明する名刺・社員証・国家資格証明書・源泉徴収票などの資格証明書、かつ男女共に免許証や保険証などの本人証明書は提示100％としています。見かけだけの資格限定企画ではなく、信頼頂くためにも身分証・資格証の提示は徹底しています。資格に該当していても、ご来場時にご提示頂けない方は、ご参加はお断りしています。</p>
								</div>
								<figure data-lf-area data-lf="<?php echo ROOT;?>/img/about/point_ph02.jpg" class="ph"></figure>
							</div>

							<div class="detail_box p03">
								<div class="wrap">
									<h3 class="title">200名パーティーを毎週開催</h3>
									<p class="txt">業界で唯一。男性をハイステイタス／ビジネスエリートに限定した200名～300名の恋活婚活パーティー＆1店舗型街コンを毎週末開催しています。人数が多いほど楽しい、その分出会いが沢山あるというのはシンプルな事実です。当社では資格限定の大人数パーティーにこだわっています。もちろん、40名50名や100名規模の企画もございますので、パーティースケジュールでご確認ください。</p>
								</div>
								<?php /*<figure data-lf-area data-lf="<?php echo ROOT;?>/img/about/point_ph03.jpg" class="ph"></figure>*/ ;?>
								<figure data-lf-area class="mov">
									<video poster="<?php echo ROOT;?>/img/about/point_ph03.jpg" controls id="video">
										<source src="<?php echo ROOT;?>/img/about/point_mov03.mp4">
									</video>
									<div class="video-btn" id="video-btn"></div>
								</figure>
							</div>

							<div class="detail_box p04">
								<div class="wrap">
									<h3 class="title">自社主催パーティー年間1,200本</h3>
									<p class="txt">最近、パーティーポータルサイト的なWEBサイトが多く、自社でほとんど又は全く開催していないのに、あたかも自社で沢山開催しているかのようなところが沢山あります。当社は2007年の創業時より、自社にて企画、会場開拓、当日の運営まで、100％自社で主催する恋活婚活パーティー＆1店舗型街コンブランドです。自社開催のパーティーを年間1,200本以上開催、動員80,000名以上。パーティーに対する思いが違います。</p>
								</div>
								<figure data-lf-area data-lf="<?php echo ROOT;?>/img/about/point_ph04.jpg" class="ph"></figure>
							</div>

							<div class="detail_box p05">
								<div class="wrap">
									<h3 class="title">立食・着席形式の2スタイル</h3>
									<p class="txt">当社は2007年の創業以来、立食形式で自由に交流頂く恋活婚活パーティーが中心で展開していましたが、強いご要望にお答えして、着席全員会話のパーティーも開催しています。<br> 実は、当社の役員は婚活パーティー業界20年以上の経験があり、当社創業以前は着席全員会話パーティー（当時はお見合いパーティー／ねるとんパーティーと呼ばれていました）の経験が豊富です。
										<br> 立食フリースタイルか着席全員会話がいいかは、ご参加される方の好みで大きく分かれますので、スケジュールにて検索してみてください。
									</p>
								</div>
								<figure data-lf-area data-lf="<?php echo ROOT;?>/img/about/point_ph05.jpg" class="ph"></figure>
							</div>

							<div class="detail_box p06">
								<div class="wrap">
									<h3 class="title">ドレスコードを設け質を維持</h3>
									<p class="txt">ドレスコードは「スマートカジュアル」で統一しています。堅すぎず、カジュアル過ぎず、異性とホテルでデート出来るレベルの少しオシャレなカジュアル。具体的には男性はジャケット（ノーネクタイ可）やドレスシャツ、短パンやTシャツ不可など、最低限のドレスコードを設け、パーティーの雰囲気・質向上を目指します。女性もワンピースやそれに準ずるような服装でお願いしています。もし、会場の現場スタッフが基準以下と判断した場合は、例えご予約後に来場頂いても、参加をお断りしています。</p>
								</div>
								<figure data-lf-area data-lf="<?php echo ROOT;?>/img/about/point_ph06.jpg" class="ph"></figure>
							</div>

							<div class="detail_box p07">
								<div class="wrap">
									<h3 class="title">ワインやカクテル・お食事も</h3>
									<p class="txt">男女の出会いの場に、余計なものは不要という考えもありますが、当社では会場となるレストランのお食事やワイン・ビール・カクテル・ソフトドリンクなど、飲食もお楽しみ頂きたいと考えます。会場によっては、女性に人気のスイーツ（デザート）。正直、二次会のように十分な量ではありませんが、異性との交流時のエッセンスの一つとしてであれば、お楽しみ頂ける内容かと思います。</p>
								</div>
								<figure data-lf-area data-lf="<?php echo ROOT;?>/img/about/point_ph07.jpg" class="ph"></figure>
							</div>

							<div class="detail_box p08">
								<div class="wrap">
									<h3 class="title">婚活パーティー10年以上の実績</h3>
									<p class="txt">当社は2007年より恋活婚活パーティー＆1店舗型街コンを開催。更に役員は20年以上婚活業界で素敵な出会い・婚活の機会をお届けしています。一部の方はご存知かもしれませんが、ねるとんパーティー・お見合いパーティーと呼ばれていた時代から出会いの場をご提供しております。昨今の街コンブームで俄に婚活パーティーを手がけているのではなく、役員・社員一同、強い思いを持って、出会いの場・婚活パーティーを企画運用しております。</p>
								</div>
								<figure data-lf-area data-lf="<?php echo ROOT;?>/img/about/point_ph08.jpg" class="ph"></figure>
							</div>
						</div>
					</div>
				</div>

			</section>
		</div>
	</article>

	<?php require_once('common/inc/bottom.php');?>