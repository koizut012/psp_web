<?php
$thisurl=dirname(__FILE__);require_once($_SERVER['DOCUMENT_ROOT'].'/common/inc/config.php');
$page_include_title = '特定商取引に関する法律表記｜婚活パーティー・恋活パーティーならPREMIUM STATUS PARTY';
$page_include_description = '「プレミアムステイタス」の特定商取引に関する法律表記です。プライベートライフの向上を目的とした出会いパーティー・婚活パーティーを開催いたしております。';
$page_include_keywords = '婚活パーティー,恋活,出会い,東京,名古屋';
$page_include_robots = '';
$page_include_canonical = '/notation/';
$page_include_h1 = '特定商取引に関する法律表記 | 東京、大阪、名古屋での婚活パーティー・恋活パーティーなら、プレミアムステイタスパーティー';
$localCSS = array(
	ROOT.'/s_css/notation.css',
);
$localJS = array(
	//ROOT.'/js/index.js',
);

//ページカテゴリ
$page_cat = 'notation';

// パンくず
 $topicpath = array(
 	array(
 		'name' => '特定商取引に関する法律表記',//名前
 		'href' => '/notation/',//パス
 		'count' => '2',//階層※2階層目から
 	),
 );
?>

	<?php require_once('common/inc/top.php');?>
	<article id="Notation" class="notation">
		<header class="base">
			<h2 class="page_tit">
				<img src="<?php echo ROOT;?>/img/notation/title.png" width="492" height="93" alt="特定商取引に関する法律表記">
			</h2>
		</header>

		<div class="contentsWrap">
			<div class="mod_wrap04">
				<div class="contents">
					<div class="table_box">
						<dl>
							<dt>会社名</dt>
							<dd>株式会社フュージョン アンド リレーションズ</dd>
						</dl>
						<dl>
							<dt>役員</dt>
							<dd>
								<dl class="inline">
									<dt>代表取締役社長：</dt>
									<dd>有井 清次</dd>
								</dl>
								<dl class="inline">
									<dt>代表取締役：</dt>
									<dd>林 圭一</dd>
								</dl>
							</dd>
						</dl>
						<dl>
							<dt>所在地</dt>
							<dd>東京都新宿区津久戸町3番19号 えひらビル2階</dd>
						</dl>
						<dl>
							<dt>連絡先</dt>
							<dd>
								<dl class="inline">
									<dt>電話：</dt>
									<dd><a data-tel="03-5206-8220">03-5206-8220</a></dd>
									<dt>FAX：</dt>
									<dd>03-3260-3890</dd>
									<dt>E-mail：</dt>
									<dd><a href="mailto:info@statusparty.jp">info@statusparty.jp</a></dd>
								</dl>
							</dd>
						</dl>
						<dl>
							<dt>会社URL</dt>
							<dd>
								<ul>
									<li><a href="http://www.fandr.jp/" target="_blank">http://www.fandr.jp/</a></li>
								</ul>
							</dd>
						</dl>
						<dl>
							<dt>提供サービス<br data-sc-pc>以外の<br data-sc-pc>必要代金</dt>
							<dd>
								<ul class="dot">
									<li>パーティー参加費にかかる消費税</li>
									<li>パーティー・イベントをキャンセルした場合、パーティー・イベントキャンセル料金とその振込に伴う金融機関への当社における振込手数料</li>
								</ul>
							</dd>
						</dl>
						<dl>
							<dt>申込期限</dt>
							<dd>申込期限は特にありませんが、会場のキャパ（定員）に達した場合や男女比調整が必要と当社が判断した時点で申込を締め切ります。なお、ご希望のパーティーが申込可能か締切かにつきましては<a href="/schedule/index.php">【スケジュール＆予約ページ】</a>を参照下さい。 </dd>
						</dl>
						<dl>
							<dt>お支払い時期</dt>
							<dd>
								<ul>
									<li>パーティー、イベントへの参加受付時</li>
									<li>パーティーキャンセル料金は、翌営業日より3日以内 </li>
								</ul>
							</dd>
						</dl>
						<dl>
							<dt>お支払い方法</dt>
							<dd>各パーティー・イベント会場受付にて現金でのお支払い、又はクレジットカードによる事前精算(※)又はコンビニ決済となります。 ※ご利用可能なカードはVISA、MASTER、JCB、その他インターナショナルカードとなります。カード精算の場合はweb申込時のみとなり、会場での精算は出来ません。また、その他インターナショナルカードをご利用のお客様は、為替相場の変動および為替手数料などにより、請求金額は申し込み金額とは若干異なる場合がございます。コンビニ決済の場合は、各コンビニにてお支払い頂きます。その際の決済手数料についてはお客様負担となります。</dd>
						</dl>
						<dl>
							<dt>お引渡し時期</dt>
							<dd>各パーティー・イベントの開催日</dd>
						</dl>
						<dl>
							<dt>お申込方法と<br data-sc-pc>サポート</dt>
							<dd>
								<p>オンライン（フォーム）、E-mail、電話のいずれか</p>
								<p>※メール及び電話でのお問合せの場合は以下にお願いします。</p>
								<dl class="disc">
									<dt>メールによるお問合せ</dt>
									<dd><a href="mailto:info@statusparty.jp">info@statusparty.jp</a>（お返事には最大48時間頂戴しております）</dd>
									<dt>電話メールによるお問合せ</dt>
									<dd><a data-tel="03-5206-8288">03-5206-8288</a>　時間11:00～20:00（月曜火曜定休　祝の場合は営業） </dd>
								</dl>
							</dd>
						</dl>
					</div>
				</div>
			</div>
		</div>
	</article>
	<?php require_once('common/inc/bottom.php');?>