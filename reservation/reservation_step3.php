<?php
$thisurl=dirname(__FILE__);require_once($_SERVER['DOCUMENT_ROOT'].'/common/inc/config.php');
$page_include_title = 'パーティー予約｜出会い・婚活パーティーならPREMIUM STATUSPARTY';
$page_include_description = '';
$page_include_keywords = '婚活パーティー・恋活パーティー,東京,大阪,名古屋';
$page_include_robots = '';
$page_include_canonical = '/reservation/';
$page_include_h1 = '●●●●● | 東京、大阪、名古屋での出会い・婚活パーティーなら PREMIUM STATUSPARTY';
$localCSS = array(
	ROOT.'/s_css/reservation.css',
);
$localJS = array(
	//ROOT.'/js/reservation.js',
);

//ページカテゴリ
$page_cat = 'reservation';

// パンくず
 $topicpath = array(
 	array(
 		'name' => 'パーティー予約',//名前
 		'href' => '/reservation/',//パス
 		'count' => '2',//階層※2階層目から
 	),
 );
?>

	<?php require_once('common/inc/top.php');?>
	<article id="Reservation" class="reservation step03">
		<header class="base">
			<h2 class="page_tit">
				<img src="<?php echo ROOT;?>/img/reservation/title.png" width="240" height="92" alt="パーティー予約">
			</h2>
		</header>

		<div class="contentsWrap">
			<div class="mod_wrap01">

				<div class="flow_box">
					<p class="flow02"><img src="<?php echo ROOT;?>/img/reservation/flow02.png" width="543" height="56" alt=""></p>
				</div>

				<div class="status_box">
					<div class="date_box">
						<span class="date">10/18日（水）20:00～22:00</span>
						<span class="place">会場：Cafe Julliet</span>
					</div>
					<p class="status01">女性定員！キャンセル待ち</p>
					<p class="status02">【50名着席全員会話☆スマートビジネスマン】</p>
					<p class="status03">男性35歳以下医師・上場・年収700万円以上vs女性32歳以下パーティー</p>
				</div>

				<div class="mod_wrap04">
					<form id="Form" autocomplete="on" enctype="multipart/form-data" name="form" method="post" action="./" class="formArea">
						<div class="rowWrap">
							<dl class="element" data-need="" data-form-unique="selectArea">
								<dt><span>参加人数</span></dt>
								<dd>
									<div class="itemWrap selectWrap selectAreaWrap">
										<span class="prepend">本人含め</span>
										<span>2</span>
										<span class="append">人</span>
									</div>
								</dd>
							</dl>
						</div>

						<div class="rowWrap">
							<dl class="element" data-need="">
								<dt><span>氏名</span></dt>
								<dd><span>飯田橋　太郎</span></dd>
							</dl>
						</div>

						<div class="rowWrap">
							<dl class="element" data-need="">
								<dt><span>フリガナ</span></dt>
								<dd><span>イイダバシ　タロウ</span></dd>
							</dl>
						</div>

						<div class="rowWrap">
							<dl class="element" data-need="" data-form-unique="radio_sex">
								<dt><span>性別</span></dt>
								<dd><span>男性</span></dd>
							</dl>
						</div>

						<div class="rowWrap">
							<dl class="element" data-need="" data-form-unique="text_tel">
								<dt><span>携帯番号</span></dt>
								<dd><span>08012345678</span></dd>
							</dl>
						</div>

						<div class="rowWrap">
							<dl class="element" data-need="" data-form-unique="text_mail">
								<dt><span>Eメール</span></dt>
								<dd><span>info@00000.jp</span></dd>
							</dl>
						</div>

						<div class="rowWrap">
							<dl class="element" data-need="">
								<dt><span>生年月日</span></dt>
								<dd><span>1994</span><span class="append">年</span><span>12</span><span class="append">月</span><span>22</span><span class="append">日</span><span>23</span><span class="append">歳</span></dd>
							</dl>
						</div>

						<div class="rowWrap">
							<dl class="element" data-need="" data-form-unique="selectRegion">
								<dt><span>都道府県</span></dt>
								<dd><span>東京</span></dd>
							</dl>
						</div>


						<div class="special_box male">
							<dl class="elementWrap" data-need="">
								<dt><span>資格区分</span><br data-sc-sp>該当している区分から1つまたは複数選択してください。</dt>
								<dd>
									<div class="rowWrap">
										<dl class="element" data-need="" data-form-unique="radio_trigger">
											<dt><span>【区分1】</span></dt>
											<dd><span>上場企業</span></dd>
										</dl>
									</div>
									<div class="rowWrap">
										<dl class="element" data-need="" data-form-unique="radio_trigger">
											<dt><span>【区分2】</span></dt>
											<dd><span>代表取締役</span></dd>
										</dl>
									</div>
									<div class="rowWrap">
										<dl class="element" data-need="" data-form-unique="radio_trigger">
											<dt><span>【区分3】</span></dt>
											<dd><span>年収400万円以上</span></dd>
										</dl>
									</div>
								</dd>
							</dl>
						</div>

						<div class="rowWrap">
							<dl class="element" data-need="" data-form-unique="radio_trigger">
								<dt><span>当サイトを知った<br data-sc-pc>きっかけ</span></dt>
								<dd><span>友人</span></dd>
							</dl>
						</div>

						<div class="rowWrap">
							<dl class="element" data-need="" data-form-unique="text_pw">
								<dt><span>パスワード</span></dt>
								<dd><span>0000000</span></dd>
							</dl>
						</div>

						<div class="rowWrap">
							<dl class="element" data-form-unique="textarea_inq">
								<dt><span>ご質問等</span></dt>
								<dd><span>質問はいります。</span></dd>
							</dl>
						</div>

						<div class="rowWrap">
							<dl class="element" data-need="" data-need-needcheckval="1" data-form-unique="checkbox_pay">
								<dt><span>決済方法</span></dt>
								<dd><span>クレジットカード</span></dd>
							</dl>
						</div>

						<!--/-->
						<div class="friend_box">
							<p class="step_txt">ご友人1</p>
							<div class="rowWrap">
								<dl class="element" data-need="">
									<dt><span>氏名</span></dt>
									<dd><span>神楽坂　五郎</span></dd>
								</dl>
							</div>

							<div class="rowWrap">
								<dl class="element" data-need="">
									<dt><span>フリガナ</span></dt>
									<dd><span>カグラザカ　ゴロウ</span></dd>
								</dl>
							</div>

							<div class="rowWrap">
								<dl class="element" data-need="" data-form-unique="radio_sex">
									<dt><span>性別</span></dt>
									<dd><span>男性</span></dd>
								</dl>
							</div>

							<div class="rowWrap">
								<dl class="element" data-need="">
									<dt><span>年齢</span></dt>
									<dd><span>24</span><span class="append">歳</span></dd>
								</dl>
							</div>

							<div class="rowWrap">
								<dl class="element" data-need="" data-form-unique="selectRegion">
									<dt><span>都道府県</span></dt>
									<dd><span>埼玉</span></dd>
								</dl>
							</div>

							<div class="special_box male">
								<dl class="elementWrap" data-need="">
									<dt><span>資格区分</span><br data-sc-sp>該当している区分から1つまたは複数選択してください。</dt>
									<dd>
										<div class="rowWrap">
											<dl class="element" data-need="" data-form-unique="radio_trigger">
												<dt><span>【区分1】</span></dt>
												<dd><span>上場企業</span></dd>
											</dl>
										</div>
										<div class="rowWrap">
											<dl class="element" data-need="" data-form-unique="radio_trigger">
												<dt><span>【区分2】</span></dt>
												<dd><span>代表取締役</span></dd>
											</dl>
										</div>
										<div class="rowWrap">
											<dl class="element" data-need="" data-form-unique="radio_trigger">
												<dt><span>【区分3】</span></dt>
												<dd><span>年収400万円以上</span></dd>
											</dl>
										</div>
									</dd>
								</dl>
							</div>
						</div>

						<div class="memo_box">
							<p class="tit">【注意事項】</p>
							<dl>
								<dt class="disc">キャンセル規程</dt>
								<dd>各企画毎にキャンセル規定が異なります。お申し込み頂いた企画のキャンセル料をご確認ください。キャンセル料は定価100％となります。</dd>
								<dt class="disc">証明書ご提示</dt>
								<dd>男性：証明書＋身分証明書の2点提示必須。（例：社員証＋免許証）<br>女性：身分証提示必須</dd>
								<dt class="disc">ドレスコード</dt>
								<dd>当社規定のドレスコードにてご参加頂けない場合（ご友人含む）でも、定価のキャンセル料が発生いたしますので、<b class="c01">参加条件</b>と<b class="c01">ドレスコード</b>をご確認のうえご予約願います。<br>（該当資格や服装について不明な点はお問い合わせください）</dd>
								<dt class="disc">独身者限定</dt>
								<dd>本企画は独身者限定パーティーのため、既婚者のご参加はできません。<br> 万が一既婚者の参加が発覚した場合は、当社の信頼を著しく呈数損する営 業妨害として<b>50</b>万円の損害賠償金を請求いたします。</dd>
							</dl>
						</div>

						<div class="price_box">
							<p class="price">参加費合計：11,000円</p>
							<ul class="memo">
								<li class="atn">※参加費合計は最大料金で表示しております。</li>
								<li>※該当割引については会場にて割引いたします。</li>
							</ul>
						</div>

						<div class="link">
							<p class="memo">上記の申し込み内容・注意事項、及び<b><a href="/rule/" class="txtlink">参加規約</a></b>について承諾の上、お申し込みください。</p>
							<div class="element" data-need="" data-need-needcheckval="1" data-form-unique="checkbox_conf">
								<div class="itemWrap checkboxWrap checkbox_confBox">
									<span class="checkbox">
									<input type="checkbox" name="checkbox_conf[]" value="独身であることを成約し、上記に承諾して申し込む" id="checkbox_conf0">
									<label for="checkbox_conf0"><span><b>独身であることを成約し、<br data-sc-sp>上記に承諾して申し込む</b></span></label>
									</span>
								</div>
							</div>

							<div class="wrap">
								<label for="ConfirmBtn" class="Confirm mod_btn07">
								<input id="ConfirmBtn" value="予約する" type="submit">
							</label>

								<label for="BackBtn" class="back mod_btn07 tp02">
								<input id="BackBtn" type="button" value="戻る" onClick="history.back()">
							</label>
							</div>
						</div>

					</form>
				</div>
			</div>
		</div>
	</article>
	<?php require_once('common/inc/bottom.php');?>