<?php
$thisurl=dirname(__FILE__);require_once($_SERVER['DOCUMENT_ROOT'].'/common/inc/config.php');
$page_include_title = 'パーティー予約｜出会い・婚活パーティーならPREMIUM STATUSPARTY';
$page_include_description = '';
$page_include_keywords = '婚活パーティー・恋活パーティー,東京,大阪,名古屋';
$page_include_robots = '';
$page_include_canonical = '/reservation/';
$page_include_h1 = '●●●●● | 東京、大阪、名古屋での出会い・婚活パーティーなら PREMIUM STATUSPARTY';
$localCSS = array(
	ROOT.'/s_css/reservation.css',
);
$localJS = array(
	ROOT.'/js/reservation.js',
);

//ページカテゴリ
$page_cat = 'reservation';

// パンくず
 $topicpath = array(
 	array(
 		'name' => 'パーティー予約',//名前
 		'href' => '/reservation/',//パス
 		'count' => '2',//階層※2階層目から
 	),
 );
?>

	<?php require_once('common/inc/top.php');?>
	<article id="Reservation" class="reservation step02">
		<header class="base">
			<h2 class="page_tit">
				<img src="<?php echo ROOT;?>/img/reservation/title.png" width="240" height="92" alt="パーティー予約">
			</h2>
		</header>

		<div class="contentsWrap">
			<div class="mod_wrap01">
				<div class="flow_box">
					<p class="flow01"><img src="<?php echo ROOT;?>/img/reservation/flow01.png" width="543" height="56" alt=""></p>
				</div>
				<p class="step_txt">続いてご友人の情報を入力ください。</p>
				<div class="mod_wrap04">
					<form id="Form" autocomplete="on" enctype="multipart/form-data" name="form" method="post" action="./" class="formArea">

						<div class="rowWrap">
							<dl class="element" data-need="">
								<dt><span>氏名</span></dt>
								<dd>
									<div class="itemWrap textWrap name01_wrap w01" data-form-unique="name01">
										<label for="name01"><input name="name01" id="name01" value="" data-calender placeholder="姓" type="text"></label></div>

									<div class="itemWrap textWrap name02_wrap w01" data-form-unique="name02">
										<label for="name02"><input name="name02" id="name02" value="" data-calender placeholder="名" type="text"></label></div>
								</dd>
							</dl>
						</div>

						<div class="rowWrap">
							<dl class="element" data-need="">
								<dt><span>フリガナ</span></dt>
								<dd>
									<div class="itemWrap textWrap kana01_wrap w01" data-form-unique="kana01">
										<label for="kana01"><input name="kana01" id="kana01" value="" data-calender placeholder="セイ" type="text"></label></div>

									<div class="itemWrap textWrap kana02_wrap w01" data-form-unique="kana02">
										<label for="kana02"><input name="kana02" id="kana02" value="" data-calender placeholder="メイ" type="text"></label></div>
								</dd>
							</dl>
						</div>

						<div class="rowWrap">
							<dl class="element" data-need="" data-form-unique="radio_sex">
								<dt><span>性別</span></dt>
								<dd>
									<div class="itemWrap radioWrap radio_sexBox">
										<span class="radio">
										<input type="radio" name="radio_sex"  value="male" id="radio_sex0">
										<label for="radio_sex0"><span>男性</span></label>
										</span>
										<span class="radio">
										<input type="radio" name="radio_sex"  value="female" id="radio_sex1">
										<label for="radio_sex1"><span>女性</span></label>
										</span>
									</div>
								</dd>
							</dl>
						</div>

						<div class="rowWrap">
							<dl class="element" data-need="">
								<dt><span>年齢</span></dt>
								<dd>
									<div class="itemWrap textWrap age_wrap w05" data-form-unique="age">
										<label for="age"><input name="age" id="age" value="" data-calender placeholder="" type="text"><span class="append">歳</span></label></div>
								</dd>
							</dl>
						</div>

						<div class="rowWrap">
							<dl class="element" data-need="" data-form-unique="selectRegion">
								<dt><span>都道府県</span></dt>
								<dd>
									<div class="itemWrap selectWrap selectRegionWrap w06">
										<span class="select"><select id="selectRegion" name="selectRegion">
										<option value="">都道府県</option>
										<option value="北海道">北海道</option>
										<option value="青森県">青森県</option>
										<option value="岩手県">岩手県</option>
										<option value="宮城県">宮城県</option>
										<option value="秋田県">秋田県</option>
										<option value="山形県">山形県</option>
										<option value="福島県">福島県</option>
										<option value="茨城県">茨城県</option>
										<option value="栃木県">栃木県</option>
										<option value="群馬県">群馬県</option>
										<option value="埼玉県">埼玉県</option>
										<option value="千葉県">千葉県</option>
										<option value="東京都">東京都</option>
										<option value="神奈川県">神奈川県</option>
										<option value="新潟県">新潟県</option>
										<option value="富山県">富山県</option>
										<option value="石川県">石川県</option>
										<option value="福井県">福井県</option>
										<option value="山梨県">山梨県</option>
										<option value="長野県">長野県</option>
										<option value="岐阜県">岐阜県</option>
										<option value="静岡県">静岡県</option>
										<option value="愛知県">愛知県</option>
										<option value="三重県">三重県</option>
										<option value="滋賀県">滋賀県</option>
										<option value="京都府">京都府</option>
										<option value="大阪府">大阪府</option>
										<option value="兵庫県">兵庫県</option>
										<option value="奈良県">奈良県</option>
										<option value="和歌山県">和歌山県</option>
										<option value="鳥取県">鳥取県</option>
										<option value="島根県">島根県</option>
										<option value="岡山県">岡山県</option>
										<option value="広島県">広島県</option>
										<option value="山口県">山口県</option>
										<option value="徳島県">徳島県</option>
										<option value="香川県">香川県</option>
										<option value="愛媛県">愛媛県</option>
										<option value="高知県">高知県</option>
										<option value="福岡県">福岡県</option>
										<option value="佐賀県">佐賀県</option>
										<option value="長崎県">長崎県</option>
										<option value="熊本県">熊本県</option>
										<option value="大分県">大分県</option>
										<option value="宮崎県">宮崎県</option>
										<option value="鹿児島県">鹿児島県</option>
										<option value="沖縄県">沖縄県</option>
										<optgroup label=""></optgroup>
									</select>
									</span>
									</div>
								</dd>
							</dl>
						</div>

						<!--男性区分-->
						<div class="special_box male" id="Male" style="display:none;">
							<dl class="elementWrap" data-need="">
								<dt><span>資格区分</span><br data-sc-sp>該当している区分から1つまたは複数選択してください。</dt>
								<dd>
									<div class="rowWrap">
										<dl class="element" data-need="" data-form-unique="radio_trigger">
											<dt><span>【区分1】</span></dt>
											<dd>
												<div class="itemWrap radioWrap radio_segmentA_Box">
													<span class="radio">
													<input type="radio" name="radio_segmentA"  value="上場企業" id="radio_segmentA0">
													<label for="radio_segmentA0"><span>上場企業</span></label>
													</span>
													<span class="radio">
													<input type="radio" name="radio_segmentA"  value="一部上場企業" id="radio_segmentA1">
													<label for="radio_segmentA1"><span>一部上場企業</span></label>
													</span>
													<span class="radio">
													<input type="radio" name="radio_segmentA"  value="大手企業" id="radio_segmentA2">
													<label for="radio_segmentA2"><span>大手企業</span></label>
													</span>
													<span class="radio">
													<input type="radio" name="radio_segmentA"  value="外資企業" id="radio_segmentA3">
													<label for="radio_segmentA3"><span>外資企業</span></label>
													</span><br data-sc-pc>
													<span class="radio">
													<input type="radio" name="radio_segmentA"  value="国家公務員（自衛隊員以外）" id="radio_segmentA4">
													<label for="radio_segmentA4"><span>国家公務員（自衛隊員以外）</span></label>
													</span>
													<span class="radio">
													<input type="radio" name="radio_segmentA"  value="自衛隊員" id="radio_segmentA5">
													<label for="radio_segmentA5"><span>自衛隊員</span></label>
													</span>
													<span class="radio">
													<input type="radio" name="radio_segmentA"  value="地方公務員" id="radio_segmentA6">
													<label for="radio_segmentA6"><span>地方公務員</span></label>
													</span><br data-sc-pc>
													<span class="otherwrap w06">
													<span class="radio">
														<input type="radio" name="radio_segmentA" value="その他" id="radio_segmentA7">
														<label for="radio_segmentA7"><span>その他</span></label>
													</span>
													<input type="text" name="radio_segmentA_other" id="radio_segmentA_other" placeholder="その他はこちらに入力" value="">
													</span>
													<p class="memo">※上場企業：50％以上出資の子会社含む　※大手企業：売上100億円以上　<br data-sc-pc>※外国資本が50％以上の企業　※国家公務員：自衛隊以外</p>
												</div>
											</dd>
										</dl>
									</div>
									<div class="rowWrap">
										<dl class="element" data-need="" data-form-unique="radio_trigger">
											<dt><span>【区分2】</span></dt>
											<dd>
												<div class="itemWrap radioWrap radio_segmentB_Box">
													<span class="radio">
													<input type="radio" name="radio_segmentB"  value="医師" id="radio_segmentB0">
													<label for="radio_segmentB0"><span>医師</span></label>
													</span>
													<span class="radio">
													<input type="radio" name="radio_segmentB"  value="歯科医師" id="radio_segmentB1">
													<label for="radio_segmentB1"><span>歯科医師</span></label>
													</span>
													<span class="radio">
													<input type="radio" name="radio_segmentB"  value="代表取締役" id="radio_segmentB2">
													<label for="radio_segmentB2"><span>代表取締役</span></label>
													</span>
													<span class="radio">
													<input type="radio" name="radio_segmentB"  value="取締役" id="radio_segmentB3">
													<label for="radio_segmentB3"><span>取締役</span></label>
													</span>
													<span class="radio">
													<input type="radio" name="radio_segmentB"  value="税理士" id="radio_segmentB4">
													<label for="radio_segmentB4"><span>税理士</span></label>
													</span>
													<span class="radio">
													<input type="radio" name="radio_segmentB"  value="会計士" id="radio_segmentB5">
													<label for="radio_segmentB5"><span>会計士</span></label>
													</span>
													<span class="radio">
													<input type="radio" name="radio_segmentB"  value="弁護士" id="radio_segmentB6">
													<label for="radio_segmentB6"><span>弁護士</span></label>
													</span><br data-sc-pc>
													<span class="radio">
													<input type="radio" name="radio_segmentB"  value="自衛隊" id="radio_segmentB7">
													<label for="radio_segmentB7"><span>自衛隊</span></label>
													</span>
													<span class="otherwrap w06">
													<span class="radio">
														<input type="radio" name="radio_segmentB" value="その他" id="radio_segmentB8">
														<label for="radio_segmentB8"><span>その他</span></label>
													</span><br data-sc-sp>
													<input type="text" name="radio_segmentB_other" id="radio_segmentB_other" placeholder="その他はこちらに入力" value="">
													</span>
													<p class="memo">※代表取締役 or 取締役：資本機1000万円以上 or 売上1億円以上の企業　<br data-sc-pc>※医師：獣医除く</p>
												</div>
											</dd>
										</dl>
									</div>
									<div class="rowWrap">
										<dl class="element" data-need="" data-form-unique="radio_trigger">
											<dt><span>【区分3】</span></dt>
											<dd>
												<div class="itemWrap radioWrap radio_segmentC_Box">
													<span class="radio">
													<input type="radio" name="radio_segmentC"  value="年収400万円以上" id="radio_segmentC0">
													<label for="radio_segmentC0"><span>年収400万円以上</span></label>
													</span>
													<span class="radio">
													<input type="radio" name="radio_segmentC"  value="年収500万円以上" id="radio_segmentC1">
													<label for="radio_segmentC1"><span>年収500万円以上</span></label>
													</span>
													<span class="radio">
													<input type="radio" name="radio_segmentC"  value="年収600万円以上" id="radio_segmentC2">
													<label for="radio_segmentC2"><span>年収600万円以上</span></label>
													</span>
													<span class="radio">
													<input type="radio" name="radio_segmentC"  value="年収700万円以上" id="radio_segmentC3">
													<label for="radio_segmentC3"><span>年収700万円以上</span></label>
													</span>
													<span class="radio">
													<input type="radio" name="radio_segmentC"  value="年収800万円以上" id="radio_segmentC4">
													<label for="radio_segmentC4"><span>年収800万円以上</span></label>
													</span>
													<span class="radio">
													<input type="radio" name="radio_segmentC"  value="年収900万円以上" id="radio_segmentC5">
													<label for="radio_segmentC5"><span>年収900万円以上</span></label>
													</span>
													<span class="radio">
													<input type="radio" name="radio_segmentC"  value="年収1000万円以上" id="radio_segmentC6">
													<label for="radio_segmentC6"><span>年収1000万円以上</span></label>
													</span>
													<span class="radio">
													<input type="radio" name="radio_segmentC"  value="年収1200万円以上" id="radio_segmentC7">
													<label for="radio_segmentC7"><span>年収1200万円以上</span></label>
													</span>
													<span class="radio">
													<input type="radio" name="radio_segmentC"  value="年収2000万円以上" id="radio_segmentC8">
													<label for="radio_segmentC8"><span>年収2000万円以上</span></label>
													</span>
													<p class="memo">※税引き前の支給額</p>
												</div>
											</dd>
										</dl>
									</div>
								</dd>
							</dl>
						</div>
						<!--/男性区分-->

						<!--女性区分-->
						<div class="special_box female" id="Female" style="display:none;">
							<dl class="elementWrap" data-need="">
								<dt><span>資格区分</span><br data-sc-sp>該当している区分から1つまたは複数選択してください。</dt>
								<dd>
									<div class="rowWrap">
										<dl class="element" data-need="" data-form-unique="radio_trigger">
											<dt><span>【区分1】</span></dt>
											<dd>
												<div class="itemWrap radioWrap radio_segmentD_Box">
													<span class="radio">
													<input type="radio" name="radio_segmentD"  value="事務" id="radio_segmentD0">
													<label for="radio_segmentD0"><span>事務</span></label>
													</span>
													<span class="radio">
													<input type="radio" name="radio_segmentD"  value="受付・秘書" id="radio_segmentD1">
													<label for="radio_segmentD1"><span>受付・秘書</span></label>
													</span>
													<span class="radio">
													<input type="radio" name="radio_segmentD"  value="医師" id="radio_segmentD2">
													<label for="radio_segmentD2"><span>医師</span></label>
													</span>
													<span class="radio">
													<input type="radio" name="radio_segmentD"  value="看護師" id="radio_segmentD3">
													<label for="radio_segmentD3"><span>看護師</span></label>
													</span>
													<span class="radio">
													<input type="radio" name="radio_segmentD"  value="保育士" id="radio_segmentD4">
													<label for="radio_segmentD4"><span>保育士</span></label>
													</span>
													<span class="radio">
													<input type="radio" name="radio_segmentD"  value="客室乗務員" id="radio_segmentD5">
													<label for="radio_segmentD5"><span>客室乗務員</span></label>
													</span>
													<span class="radio">
													<input type="radio" name="radio_segmentD"  value="アパレル" id="radio_segmentD6">
													<label for="radio_segmentD6"><span>アパレル</span></label>
													</span><br data-sc-pc>
													<span class="radio">
													<input type="radio" name="radio_segmentD"  value="美容部員" id="radio_segmentD7">
													<label for="radio_segmentD7"><span>美容部員</span></label>
													</span>
													<span class="radio">
													<input type="radio" name="radio_segmentD"  value="学生" id="radio_segmentD8">
													<label for="radio_segmentD8"><span>学生</span></label>
													</span>
													<span class="otherwrap w06">
													<span class="radio">
														<input type="radio" name="radio_segmentD" value="その他" id="radio_segmentD9">
														<label for="radio_segmentD9"><span>その他</span></label>
													</span><br data-sc-sp>
													<input type="text" name="radio_segmentD_other" id="radio_segmentD_other" placeholder="その他はこちらに入力" value="">
													</span>
												</div>
											</dd>
										</dl>
									</div>
									<div class="rowWrap">
										<dl class="element" data-need="" data-form-unique="radio_trigger">
											<dt><span>【区分2】</span></dt>
											<dd>
												<div class="itemWrap radioWrap radio_segmentE_Box">
													<span class="radio">
													<input type="radio" name="radio_segmentE"  value="上場企業" id="radio_segmentE0">
													<label for="radio_segmentE0"><span>上場企業</span></label>
													</span>
													<span class="radio">
													<input type="radio" name="radio_segmentE"  value="一部上場企業" id="radio_segmentE1">
													<label for="radio_segmentE1"><span>一部上場企業</span></label>
													</span>
													<span class="radio">
													<input type="radio" name="radio_segmentE"  value="大手企業" id="radio_segmentE2">
													<label for="radio_segmentE2"><span>大手企業</span></label>
													</span>
													<span class="radio">
													<input type="radio" name="radio_segmentE"  value="外資企業" id="radio_segmentE3">
													<label for="radio_segmentE3"><span>外資企業</span></label>
													</span><br data-sc-pc>
													<span class="radio">
													<input type="radio" name="radio_segmentE"  value="国家公務員（自衛隊員以外）" id="radio_segmentE4">
													<label for="radio_segmentE4"><span>国家公務員（自衛隊員以外）</span></label>
													</span>
													<span class="radio">
													<input type="radio" name="radio_segmentE"  value="自衛隊員" id="radio_segmentE5">
													<label for="radio_segmentE5"><span>自衛隊員</span></label>
													</span>
													<span class="radio">
													<input type="radio" name="radio_segmentE"  value="地方公務員" id="radio_segmentE6">
													<label for="radio_segmentE6"><span>地方公務員</span></label>
													</span><br data-sc-pc>
													<span class="otherwrap w06">
													<span class="radio">
														<input type="radio" name="radio_segmentE" value="その他" id="radio_segmentE7">
														<label for="radio_segmentE7"><span>その他</span></label><br data-sc-sp>
													</span>
													<input type="text" name="radio_segmentE_other" id="radio_segmentE_other" placeholder="その他はこちらに入力" value="">
													</span>
													<p class="memo">※上場企業：50％以上出資の子会社含む　※大手企業：売上100億円以上<br data-sc-pc>※外国資本が50％以上の企業</p>
												</div>
											</dd>
										</dl>
									</div>
									<div class="rowWrap">
										<dl class="element" data-need="" data-form-unique="radio_trigger">
											<dt><span>【区分3】</span></dt>
											<dd>
												<div class="itemWrap radioWrap radio_segmentF_Box">
													<span class="radio">
													<input type="radio" name="radio_segmentF"  value="高校" id="radio_segmentF0">
													<label for="radio_segmentF0"><span>高校</span></label>
													</span>
													<span class="radio">
													<input type="radio" name="radio_segmentF"  value="専門" id="radio_segmentF1">
													<label for="radio_segmentF1"><span>専門</span></label>
													</span>
													<span class="radio">
													<input type="radio" name="radio_segmentF"  value="短大" id="radio_segmentF2">
													<label for="radio_segmentF2"><span>短大</span></label>
													</span>
													<span class="otherwrap w06">
													<span class="radio">
														<input type="radio" name="radio_segmentF" value="四大" id="radio_segmentF3">
														<label for="radio_segmentF3"><span>四大</span></label>
													</span><br data-sc-sp>
													<input type="text" name="radio_segmentF_other" id="radio_segmentF_other" placeholder="大学名" value="">
													</span><br>
													<span class="otherwrap w06">
													<span class="radio">
														<input type="radio" name="radio_segmentF" value="その他" id="radio_segmentF4">
														<label for="radio_segmentF4"><span>その他</span></label>
													</span><br data-sc-sp>
													<input type="text" name="radio_segmentF_other2" id="radio_segmentF_other2" placeholder="その他はこちらに入力" value="">
													</span><br data-sc-pc>
												</div>
											</dd>
										</dl>
									</div>
								</dd>
							</dl>
						</div>
						<!--/女性区分-->

						<div class="link">
							<label for="ConfirmBtn" class="Confirm mod_btn07">
							<input id="ConfirmBtn" value="予約する" type="submit">
						</label>
						</div>

					</form>
				</div>
			</div>
		</div>
	</article>
	<?php require_once('common/inc/bottom.php');?>