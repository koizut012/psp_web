<?php
$thisurl=dirname(__FILE__);require_once($_SERVER['DOCUMENT_ROOT'].'/common/inc/config.php');
$page_include_title = 'パーティー予約｜出会い・婚活パーティーならPREMIUM STATUSPARTY';
$page_include_description = '';
$page_include_keywords = '婚活パーティー・恋活パーティー,東京,大阪,名古屋';
$page_include_robots = '';
$page_include_canonical = '/reservation/';
$page_include_h1 = '●●●●● | 東京、大阪、名古屋での出会い・婚活パーティーなら PREMIUM STATUSPARTY';
$localCSS = array(
	ROOT.'/common/js/jquery/datepicker/jquery-ui.min.css',
	ROOT.'/common/js/jquery/datepicker/jquery-ui.theme.min.css',
	ROOT.'/s_css/reservation.css',
);
$localJS = array(
	ROOT.'/common/js/jquery/datepicker/jquery-ui.min.js',
	ROOT.'/common/js/jquery/datepicker/jquery.ui.datepicker-ja.min.js',
	ROOT.'/js/form.js',
	ROOT.'/js/reservation.js',
);

//ページカテゴリ
$page_cat = 'reservation';

// パンくず
 $topicpath = array(
 	array(
 		'name' => 'パーティー予約',//名前
 		'href' => '/reservation/',//パス
 		'count' => '2',//階層※2階層目から
 	),
 );
?>

	<?php require_once('common/inc/top.php');?>
	<article id="Reservation" class="reservation step01">
		<header class="base">
			<h2 class="page_tit">
				<img src="<?php echo ROOT;?>/img/reservation/title.png" width="240" height="92" alt="パーティー予約">
			</h2>
		</header>

		<div class="contentsWrap">
			<div class="mod_wrap01">
				<div class="status_box">
					<div class="date_box">
						<span class="date">10/18日（水）20:00～22:00</span>
						<span class="place">会場：Cafe Julliet</span>
					</div>
					<p class="status01">女性定員！キャンセル待ち</p>
					<p class="status02">【50名着席全員会話☆スマートビジネスマン】</p>
					<p class="status03">男性35歳以下医師・上場・年収700万円以上vs女性32歳以下パーティー</p>
				</div>

				<ul class="tabMenu">
					<li class="current"><span>初めて<br data-sc-sp>参加される方</span></li>
					<li><span>過去に<br data-sc-sp>参加された方</span></li>
				</ul>
				<!--/#Firsttime-->
				<div class="tabArea" id="Firsttime">
					<div class="mod_wrap04">
						<form id="Form" autocomplete="on" enctype="multipart/form-data" name="form" method="post" action="./" class="formArea">
							<div class="rowWrap">
								<dl class="element" data-need="" data-form-unique="selectArea">
									<dt><span>参加人数</span></dt>
									<dd>
										<div class="itemWrap selectWrap selectAreaWrap">
											<span class="prepend">本人含め</span>
											<span class="select">
										<select id="selectArea" name="selectArea">
										<option value="">-</option>
										<option value="1">1</option>
										<option value="2">2</option>
										<option value="3">3</option>
										<optgroup label=""></optgroup>
										</select>
										</span>
											<span class="append">人</span>
										</div>
									</dd>
								</dl>
							</div>
							<div class="rowWrap">
								<dl class="element" data-need="">
									<dt><span>氏名</span></dt>
									<dd>
										<div class="itemWrap textWrap name01_wrap w01" data-form-unique="name01">
											<label for="name01"><input name="name01" id="name01" value="" placeholder="姓" type="text"></label></div>

										<div class="itemWrap textWrap name02_wrap w01" data-form-unique="name02">
											<label for="name02"><input name="name02" id="name02" value="" placeholder="名" type="text"></label></div>
									</dd>
								</dl>
							</div>
							<div class="rowWrap">
								<dl class="element" data-need="">
									<dt><span>フリガナ</span></dt>
									<dd>
										<div class="itemWrap textWrap kana01_wrap w01" data-form-unique="kana01">
											<label for="kana01"><input name="kana01" id="kana01" value="" placeholder="セイ" type="text"></label></div>

										<div class="itemWrap textWrap kana02_wrap w01" data-form-unique="kana02">
											<label for="kana02"><input name="kana02" id="kana02" value="" placeholder="メイ" type="text"></label></div>
									</dd>
								</dl>
							</div>
							<div class="rowWrap">
								<dl class="element" data-need="" data-form-unique="radio_sex">
									<dt><span>性別</span></dt>
									<dd>
										<div class="itemWrap radioWrap radio_sexBox">
											<span class="radio">
											<input type="radio" name="radio_sex"  value="male" id="radio_sex0">
											<label for="radio_sex0"><span>男性</span></label>
											</span>
											<span class="radio">
											<input type="radio" name="radio_sex"  value="female" id="radio_sex1">
											<label for="radio_sex1"><span>女性</span></label>
											</span>
										</div>
									</dd>
								</dl>
							</div>
							<div class="rowWrap">
								<dl class="element" data-need="" data-form-unique="text_tel">
									<dt><span>携帯番号</span></dt>
									<dd>
										<div class="itemWrap textWrap text_telBox w02 spW01"><input type="tel" name="text_tel" id="text_tel" value="" size="15" placeholder="例：00000000000（半角英数字　ハイフンなし）"></div>
									</dd>
								</dl>
							</div>
							<div class="rowWrap">
								<dl class="element" data-need="" data-form-unique="text_mail">
									<dt><span>Eメール</span></dt>
									<dd>
										<div class="itemWrap textWrap text_mailBox w03"><input type="email" name="text_mail" id="text_mail" value="" size="15" autocomplete="email" placeholder="例：info@00000（半角英数字）"></div>
										<p class="info">※携帯でドメイン指定受信を設定の方はstatusparty.jp又はinfo@statusparty.jpをご登録お願いします。<br>
											<a href="https://www.statusparty.jp/domain/" target="_blank">ドメイン各社指定方法はこちら</a></p>
									</dd>
								</dl>
							</div>
							<div class="rowWrap">
								<dl class="element" data-need="">
									<dt><span>生年月日(西暦)</span></dt>
									<dd>
										<div class="itemWrap textWrap year_wrap w04 spW03" data-form-unique="year">
											<label for="year"><input name="year" id="year" value="" placeholder="例：1997" type="text"><span class="append">年</span></label></div>

										<div class="itemWrap textWrap month_wrap w05 spW02" data-form-unique="month">
											<label for="month"><input name="month" id="month" value="" placeholder="12" type="text"><span class="append">月</span></label></div>

										<div class="itemWrap textWrap day_wrap w05 spW02" data-form-unique="day">
											<label for="day"><input name="day" id="day" value="" placeholder="24" type="text"><span class="append">日</span></label></div>

										<div class="itemWrap textWrap age_wrap w05 spW02" data-form-unique="age">
											<label for="age"><input name="age" id="age" value="" placeholder="" type="text"><span class="append">歳</span></label></div>
									</dd>
								</dl>
							</div>
							<div class="rowWrap">
								<dl class="element" data-need="" data-form-unique="selectRegion">
									<dt><span>都道府県</span></dt>
									<dd>
										<div class="itemWrap selectWrap selectRegionWrap w06">
											<span class="select"><select id="selectRegion" name="selectRegion">
											<option value="">都道府県</option>
											<option value="北海道">北海道</option>
											<option value="青森県">青森県</option>
											<option value="岩手県">岩手県</option>
											<option value="宮城県">宮城県</option>
											<option value="秋田県">秋田県</option>
											<option value="山形県">山形県</option>
											<option value="福島県">福島県</option>
											<option value="茨城県">茨城県</option>
											<option value="栃木県">栃木県</option>
											<option value="群馬県">群馬県</option>
											<option value="埼玉県">埼玉県</option>
											<option value="千葉県">千葉県</option>
											<option value="東京都">東京都</option>
											<option value="神奈川県">神奈川県</option>
											<option value="新潟県">新潟県</option>
											<option value="富山県">富山県</option>
											<option value="石川県">石川県</option>
											<option value="福井県">福井県</option>
											<option value="山梨県">山梨県</option>
											<option value="長野県">長野県</option>
											<option value="岐阜県">岐阜県</option>
											<option value="静岡県">静岡県</option>
											<option value="愛知県">愛知県</option>
											<option value="三重県">三重県</option>
											<option value="滋賀県">滋賀県</option>
											<option value="京都府">京都府</option>
											<option value="大阪府">大阪府</option>
											<option value="兵庫県">兵庫県</option>
											<option value="奈良県">奈良県</option>
											<option value="和歌山県">和歌山県</option>
											<option value="鳥取県">鳥取県</option>
											<option value="島根県">島根県</option>
											<option value="岡山県">岡山県</option>
											<option value="広島県">広島県</option>
											<option value="山口県">山口県</option>
											<option value="徳島県">徳島県</option>
											<option value="香川県">香川県</option>
											<option value="愛媛県">愛媛県</option>
											<option value="高知県">高知県</option>
											<option value="福岡県">福岡県</option>
											<option value="佐賀県">佐賀県</option>
											<option value="長崎県">長崎県</option>
											<option value="熊本県">熊本県</option>
											<option value="大分県">大分県</option>
											<option value="宮崎県">宮崎県</option>
											<option value="鹿児島県">鹿児島県</option>
											<option value="沖縄県">沖縄県</option>
											<optgroup label=""></optgroup>
										</select>
										</span>
										</div>
									</dd>
								</dl>
							</div>


							<!--男性区分-->
							<div class="special_box male" id="Male" style="display:none;">
								<dl class="elementWrap" data-need="">
									<dt><span>資格区分</span><br data-sc-sp>該当している区分から1つまたは複数選択してください。</dt>
									<dd>
										<div class="rowWrap">
											<dl class="element" data-need="" data-form-unique="radio_trigger">
												<dt><span>【区分1】</span></dt>
												<dd>
													<div class="itemWrap radioWrap radio_segmentA_Box">
														<span class="radio">
														<input type="radio" name="radio_segmentA"  value="上場企業" id="radio_segmentA0">
														<label for="radio_segmentA0"><span>上場企業</span></label>
														</span>
														<span class="radio">
														<input type="radio" name="radio_segmentA"  value="一部上場企業" id="radio_segmentA1">
														<label for="radio_segmentA1"><span>一部上場企業</span></label>
														</span>
														<span class="radio">
														<input type="radio" name="radio_segmentA"  value="大手企業" id="radio_segmentA2">
														<label for="radio_segmentA2"><span>大手企業</span></label>
														</span>
														<span class="radio">
														<input type="radio" name="radio_segmentA"  value="外資企業" id="radio_segmentA3">
														<label for="radio_segmentA3"><span>外資企業</span></label>
														</span><br data-sc-pc>
														<span class="radio">
														<input type="radio" name="radio_segmentA"  value="国家公務員（自衛隊員以外）" id="radio_segmentA4">
														<label for="radio_segmentA4"><span>国家公務員（自衛隊員以外）</span></label>
														</span>
														<span class="radio">
														<input type="radio" name="radio_segmentA"  value="自衛隊員" id="radio_segmentA5">
														<label for="radio_segmentA5"><span>自衛隊員</span></label>
														</span>
														<span class="radio">
														<input type="radio" name="radio_segmentA"  value="地方公務員" id="radio_segmentA6">
														<label for="radio_segmentA6"><span>地方公務員</span></label>
														</span><br data-sc-pc>
														<span class="otherwrap w06">
														<span class="radio">
															<input type="radio" name="radio_segmentA" value="その他" id="radio_segmentA7">
															<label for="radio_segmentA7"><span>その他</span></label>
														</span>
														<input type="text" name="radio_segmentA_other" id="radio_segmentA_other" placeholder="その他はこちらに入力" value="">
														</span>
														<p class="memo">※上場企業：50％以上出資の子会社含む　※大手企業：売上100億円以上　<br data-sc-pc>※外国資本が50％以上の企業　※国家公務員：自衛隊以外</p>
													</div>
												</dd>
											</dl>
										</div>
										<div class="rowWrap">
											<dl class="element" data-need="" data-form-unique="radio_trigger">
												<dt><span>【区分2】</span></dt>
												<dd>
													<div class="itemWrap radioWrap radio_segmentB_Box">
														<span class="radio">
														<input type="radio" name="radio_segmentB"  value="医師" id="radio_segmentB0">
														<label for="radio_segmentB0"><span>医師</span></label>
														</span>
														<span class="radio">
														<input type="radio" name="radio_segmentB"  value="歯科医師" id="radio_segmentB1">
														<label for="radio_segmentB1"><span>歯科医師</span></label>
														</span>
														<span class="radio">
														<input type="radio" name="radio_segmentB"  value="代表取締役" id="radio_segmentB2">
														<label for="radio_segmentB2"><span>代表取締役</span></label>
														</span>
														<span class="radio">
														<input type="radio" name="radio_segmentB"  value="取締役" id="radio_segmentB3">
														<label for="radio_segmentB3"><span>取締役</span></label>
														</span>
														<span class="radio">
														<input type="radio" name="radio_segmentB"  value="税理士" id="radio_segmentB4">
														<label for="radio_segmentB4"><span>税理士</span></label>
														</span>
														<span class="radio">
														<input type="radio" name="radio_segmentB"  value="会計士" id="radio_segmentB5">
														<label for="radio_segmentB5"><span>会計士</span></label>
														</span>
														<span class="radio">
														<input type="radio" name="radio_segmentB"  value="弁護士" id="radio_segmentB6">
														<label for="radio_segmentB6"><span>弁護士</span></label>
														</span><br data-sc-pc>
														<span class="radio">
														<input type="radio" name="radio_segmentB"  value="自衛隊" id="radio_segmentB7">
														<label for="radio_segmentB7"><span>自衛隊</span></label>
														</span>
														<span class="otherwrap w06">
														<span class="radio">
															<input type="radio" name="radio_segmentB" value="その他" id="radio_segmentB8">
															<label for="radio_segmentB8"><span>その他</span></label>
														</span><br data-sc-sp>
														<input type="text" name="radio_segmentB_other" id="radio_segmentB_other" placeholder="その他はこちらに入力" value="">
														</span>
														<p class="memo">※代表取締役 or 取締役：資本機1000万円以上 or 売上1億円以上の企業　<br data-sc-pc>※医師：獣医除く</p>
													</div>
												</dd>
											</dl>
										</div>
										<div class="rowWrap">
											<dl class="element" data-need="" data-form-unique="radio_trigger">
												<dt><span>【区分3】</span></dt>
												<dd>
													<div class="itemWrap radioWrap radio_segmentC_Box">
														<span class="radio">
														<input type="radio" name="radio_segmentC"  value="年収400万円以上" id="radio_segmentC0">
														<label for="radio_segmentC0"><span>年収400万円以上</span></label>
														</span>
														<span class="radio">
														<input type="radio" name="radio_segmentC"  value="年収500万円以上" id="radio_segmentC1">
														<label for="radio_segmentC1"><span>年収500万円以上</span></label>
														</span>
														<span class="radio">
														<input type="radio" name="radio_segmentC"  value="年収600万円以上" id="radio_segmentC2">
														<label for="radio_segmentC2"><span>年収600万円以上</span></label>
														</span>
														<span class="radio">
														<input type="radio" name="radio_segmentC"  value="年収700万円以上" id="radio_segmentC3">
														<label for="radio_segmentC3"><span>年収700万円以上</span></label>
														</span>
														<span class="radio">
														<input type="radio" name="radio_segmentC"  value="年収800万円以上" id="radio_segmentC4">
														<label for="radio_segmentC4"><span>年収800万円以上</span></label>
														</span>
														<span class="radio">
														<input type="radio" name="radio_segmentC"  value="年収900万円以上" id="radio_segmentC5">
														<label for="radio_segmentC5"><span>年収900万円以上</span></label>
														</span>
														<span class="radio">
														<input type="radio" name="radio_segmentC"  value="年収1000万円以上" id="radio_segmentC6">
														<label for="radio_segmentC6"><span>年収1000万円以上</span></label>
														</span>
														<span class="radio">
														<input type="radio" name="radio_segmentC"  value="年収1200万円以上" id="radio_segmentC7">
														<label for="radio_segmentC7"><span>年収1200万円以上</span></label>
														</span>
														<span class="radio">
														<input type="radio" name="radio_segmentC"  value="年収2000万円以上" id="radio_segmentC8">
														<label for="radio_segmentC8"><span>年収2000万円以上</span></label>
														</span>
														<p class="memo">※税引き前の支給額</p>
													</div>
												</dd>
											</dl>
										</div>
									</dd>
								</dl>
							</div>
							<!--/男性区分-->
							

							<!--女性区分-->
							<div class="special_box female" id="Female" style="display:none;">
								<dl class="elementWrap" data-need="">
									<dt><span>資格区分</span><br data-sc-sp>該当している区分から1つまたは複数選択してください。</dt>
									<dd>
										<div class="rowWrap">
											<dl class="element" data-need="" data-form-unique="radio_trigger">
												<dt><span>【区分1】</span></dt>
												<dd>
													<div class="itemWrap radioWrap radio_segmentD_Box">
														<span class="radio">
														<input type="radio" name="radio_segmentD"  value="事務" id="radio_segmentD0">
														<label for="radio_segmentD0"><span>事務</span></label>
														</span>
														<span class="radio">
														<input type="radio" name="radio_segmentD"  value="受付・秘書" id="radio_segmentD1">
														<label for="radio_segmentD1"><span>受付・秘書</span></label>
														</span>
														<span class="radio">
														<input type="radio" name="radio_segmentD"  value="医師" id="radio_segmentD2">
														<label for="radio_segmentD2"><span>医師</span></label>
														</span>
														<span class="radio">
														<input type="radio" name="radio_segmentD"  value="看護師" id="radio_segmentD3">
														<label for="radio_segmentD3"><span>看護師</span></label>
														</span>
														<span class="radio">
														<input type="radio" name="radio_segmentD"  value="保育士" id="radio_segmentD4">
														<label for="radio_segmentD4"><span>保育士</span></label>
														</span>
														<span class="radio">
														<input type="radio" name="radio_segmentD"  value="客室乗務員" id="radio_segmentD5">
														<label for="radio_segmentD5"><span>客室乗務員</span></label>
														</span>
														<span class="radio">
														<input type="radio" name="radio_segmentD"  value="アパレル" id="radio_segmentD6">
														<label for="radio_segmentD6"><span>アパレル</span></label>
														</span><br data-sc-pc>
														<span class="radio">
														<input type="radio" name="radio_segmentD"  value="美容部員" id="radio_segmentD7">
														<label for="radio_segmentD7"><span>美容部員</span></label>
														</span>
														<span class="radio">
														<input type="radio" name="radio_segmentD"  value="学生" id="radio_segmentD8">
														<label for="radio_segmentD8"><span>学生</span></label>
														</span>
														<span class="otherwrap w06">
														<span class="radio">
															<input type="radio" name="radio_segmentD" value="その他" id="radio_segmentD9">
															<label for="radio_segmentD9"><span>その他</span></label>
														</span><br data-sc-sp>
														<input type="text" name="radio_segmentD_other" id="radio_segmentD_other" placeholder="その他はこちらに入力" value="">
														</span>
													</div>
												</dd>
											</dl>
										</div>
										<div class="rowWrap">
											<dl class="element" data-need="" data-form-unique="radio_trigger">
												<dt><span>【区分2】</span></dt>
												<dd>
													<div class="itemWrap radioWrap radio_segmentE_Box">
														<span class="radio">
														<input type="radio" name="radio_segmentE"  value="上場企業" id="radio_segmentE0">
														<label for="radio_segmentE0"><span>上場企業</span></label>
														</span>
														<span class="radio">
														<input type="radio" name="radio_segmentE"  value="一部上場企業" id="radio_segmentE1">
														<label for="radio_segmentE1"><span>一部上場企業</span></label>
														</span>
														<span class="radio">
														<input type="radio" name="radio_segmentE"  value="大手企業" id="radio_segmentE2">
														<label for="radio_segmentE2"><span>大手企業</span></label>
														</span>
														<span class="radio">
														<input type="radio" name="radio_segmentE"  value="外資企業" id="radio_segmentE3">
														<label for="radio_segmentE3"><span>外資企業</span></label>
														</span><br data-sc-pc>
														<span class="radio">
														<input type="radio" name="radio_segmentE"  value="国家公務員（自衛隊員以外）" id="radio_segmentE4">
														<label for="radio_segmentE4"><span>国家公務員（自衛隊員以外）</span></label>
														</span>
														<span class="radio">
														<input type="radio" name="radio_segmentE"  value="自衛隊員" id="radio_segmentE5">
														<label for="radio_segmentE5"><span>自衛隊員</span></label>
														</span>
														<span class="radio">
														<input type="radio" name="radio_segmentE"  value="地方公務員" id="radio_segmentE6">
														<label for="radio_segmentE6"><span>地方公務員</span></label>
														</span><br data-sc-pc>
														<span class="otherwrap w06">
														<span class="radio">
															<input type="radio" name="radio_segmentE" value="その他" id="radio_segmentE7">
															<label for="radio_segmentE7"><span>その他</span></label><br data-sc-sp>
														</span>
														<input type="text" name="radio_segmentE_other" id="radio_segmentE_other" placeholder="その他はこちらに入力" value="">
														</span>
														<p class="memo">※上場企業：50％以上出資の子会社含む　※大手企業：売上100億円以上<br data-sc-pc>※外国資本が50％以上の企業</p>
													</div>
												</dd>
											</dl>
										</div>
										<div class="rowWrap">
											<dl class="element" data-need="" data-form-unique="radio_trigger">
												<dt><span>【区分3】</span></dt>
												<dd>
													<div class="itemWrap radioWrap radio_segmentF_Box">
														<span class="radio">
														<input type="radio" name="radio_segmentF"  value="高校" id="radio_segmentF0">
														<label for="radio_segmentF0"><span>高校</span></label>
														</span>
														<span class="radio">
														<input type="radio" name="radio_segmentF"  value="専門" id="radio_segmentF1">
														<label for="radio_segmentF1"><span>専門</span></label>
														</span>
														<span class="radio">
														<input type="radio" name="radio_segmentF"  value="短大" id="radio_segmentF2">
														<label for="radio_segmentF2"><span>短大</span></label>
														</span>
														<span class="otherwrap w06">
														<span class="radio">
															<input type="radio" name="radio_segmentF" value="四大" id="radio_segmentF3">
															<label for="radio_segmentF3"><span>四大</span></label>
														</span><br data-sc-sp>
														<input type="text" name="radio_segmentF_other" id="radio_segmentF_other" placeholder="大学名" value="">
														</span><br>
														<span class="otherwrap w06">
														<span class="radio">
															<input type="radio" name="radio_segmentF" value="その他" id="radio_segmentF4">
															<label for="radio_segmentF4"><span>その他</span></label>
														</span><br data-sc-sp>
														<input type="text" name="radio_segmentF_other2" id="radio_segmentF_other2" placeholder="その他はこちらに入力" value="">
														</span><br data-sc-pc>
													</div>
												</dd>
											</dl>
										</div>
									</dd>
								</dl>
							</div>
							<!--/女性区分-->

							<div class="rowWrap">
								<dl class="element" data-need="" data-form-unique="radio_trigger">
									<dt><span>当サイトを知った<br data-sc-pc>きっかけ</span></dt>
									<dd>
										<div class="itemWrap radioWrap radio_triggerBox">
											<span class="radio">
											<input type="radio" name="radio_trigger"  value="友人" id="radio_trigger0">
											<label for="radio_trigger0"><span>友人</span></label>
											</span>
											<span class="radio">
											<input type="radio" name="radio_trigger"  value="Yahoo検索" id="radio_trigger1">
											<label for="radio_trigger1"><span>Yahoo検索</span></label>
											</span>
											<span class="radio">
											<input type="radio" name="radio_trigger"  value="Google検索" id="radio_trigger2">
											<label for="radio_trigger2"><span>Google検索</span></label>
											</span>
											<span class="radio">
											<input type="radio" name="radio_trigger"  value="Facebook" id="radio_trigger3">
											<label for="radio_trigger3"><span>Facebook</span></label>
											</span>
											<span class="radio">
											<input type="radio" name="radio_trigger"  value="Instagram" id="radio_trigger4">
											<label for="radio_trigger4"><span>Instagram</span></label>
											</span>
											<span class="radio">
											<input type="radio" name="radio_trigger"  value="Twitter" id="radio_trigger5">
											<label for="radio_trigger5"><span>Twitter</span></label>
											</span><br data-sc-pc>

											<span class="radio">
											<input type="radio" name="radio_trigger"  value="スタッフ" id="radio_trigger6">
											<label for="radio_trigger6"><span>スタッフ</span></label>
											</span>
											<span class="otherwrap w06">
											<span class="radio">
												<input type="radio" name="radio_trigger" value="その他" id="radio_trigger7">
												<label for="radio_trigger7"><span>その他</span></label>
											</span><br data-sc-sp>
											<input type="text" name="radio_trigger_other" id="radio_trigger_other" placeholder="その他はこちらに入力" value="">
											</span>
										</div>
									</dd>
								</dl>
							</div>

							<div class="rowWrap">
								<dl class="element" data-need="" data-form-unique="text_pw">
									<dt><span>パスワード</span></dt>
									<dd>
										<div class="itemWrap textWrap text_pwBox w01 spW01"><input type="text" name="text_pw" id="text_pw" value=""><span class="append" data-sc-pc>※6桁の半角英数字を組み合わせて入力してください。</span>
											<p class="memo" data-sc-sp>※6桁の半角英数字を組み合わせて入力してください。</p>
										</div>
									</dd>
								</dl>
							</div>

							<div class="rowWrap">
								<dl class="element" data-form-unique="textarea_inq">
									<dt><span>ご質問等</span></dt>
									<dd>
										<div class="itemWrap textareaWrap textarea_inqBox spW01"><textarea name="textarea_inq" id="textarea_inq" rows="10" cols="40" placeholder=""></textarea></div>
									</dd>
								</dl>
							</div>

							<div class="option_box">
								<div class="rowWrap">
									<dl class="element" data-need="" data-form-unique="radio_days">
										<dt>毎日1名年間365名を紹介する<br data-sc-sp>マッチングサービス<br>登録費・月会費・紹介料無料の<br data-sc-sp>当社姉妹サービス「Days」に</dt>
										<dd>
											<div class="itemWrap radioWrap radio_daysBox">
												<span class="radio">
											<input type="radio" name="radio_days"  value="登録する" id="radio_days0">
											<label for="radio_days0"><span>登録する</span></label>
												</span>
												<span class="radio">
											<input type="radio" name="radio_days"  value="登録しない" id="radio_days1">
											<label for="radio_days1"><span>登録しない</span></label>
												</span><br data-sc-sp>
												<span class="radio">
											<input type="radio" name="radio_days"  value="詳細を見る" id="radio_days2">
											<label for="radio_days2"><span>詳細を見る</span></label>
												</span>
												<span class="radio">
											<input type="radio" name="radio_days"  value="今後表示しない" id="radio_days3">
											<label for="radio_days3"><span>今後表示しない</span></label>
												</span>
											</div>
										</dd>
									</dl>
									<p class="memo">※登録するを選択したお客様には、本パーティーの予約完了後にご登録頂いたメールアドレスにマッチングサービスDaysのご登録URLをお送りいたします。</p>
								</div>
							</div>

							<div class="link">
								<label for="ConfirmBtn" class="Confirm mod_btn07">
								<input id="ConfirmBtn" value="次のステップへ" type="submit">
							</label>
								<p class="memo">次のステップを押すと上記内容は保存され次回ご予約時に<br data-sc-pc>「過去に参加された方」からログインできます。</p>
							</div>

						</form>
					</div>
				</div><!--/#Firsttime tabArea-->
				
				<!--#Member tabArea-->
				<div class="tabArea" id="Member">
					<div class="mod_wrap04">
						<form id="Form0" autocomplete="on" enctype="multipart/form-data" name="form" method="post" action="./" class="formArea">

							<div class="rowWrap">
								<dl class="element" data-need="" data-form-unique="text_mail0">
									<dt><span>Eメール</span></dt>
									<dd>
										<div class="itemWrap textWrap text_mailBox w03 spW01"><input type="email" name="text_mail0" id="text_mail0" value="" size="15" autocomplete="email" placeholder="例：info@00000（半角英数字）"></div>
									</dd>
								</dl>
							</div>

							<div class="rowWrap">
								<dl class="element" data-need="" data-form-unique="text_pw0">
									<dt><span>パスワード</span></dt>
									<dd>
										<div class="itemWrap textWrap text_pwBox w01 spW01"><input type="text" name="text_pw0" id="text_pw0" value=""><span class="append" data-sc-pc>※6桁の半角英数字を入力してください。</span>
											<p class="memo" data-sc-sp>※6桁の半角英数字を入力してください。</p>
										</div>
									</dd>
								</dl>
							</div>

							<div class="link">
								<label for="ConfirmBtn0" class="Confirm mod_btn07">
								<input id="ConfirmBtn0" value="予約する" type="submit">
							</label>
							</div>

						</form>
					</div>
				</div><!--/#Member tabArea-->

			</div>
		</div>
	</article>
	<?php require_once('common/inc/bottom.php');?>