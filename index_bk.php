<?php
$thisurl=dirname(__FILE__);require_once($_SERVER['DOCUMENT_ROOT'].'/common/inc/config.php');
$page_include_title = '恋活・婚活パーティー・街コンはエリート対象のプレミアムステイタス';
$page_include_description = '恋活・婚活パーティーと1店舗型の街コンを大手企業・経営者・医師・士業・公務員など20代30代ハイステイタス男性を対象に開催。東京・横浜・大阪・京都・神戸・名古屋・札幌・福岡で質の高い出会いを希望する方はプレミアムステイタスパーティー。';
$page_include_keywords = '恋活,婚活,街コン,パーティー,出会い';
$page_include_robots = '';
$page_include_canonical = '/';
$page_include_h1 = '東京、大阪、名古屋、札幌、福岡の恋活・婚活パーティー・1店舗型の街コンなら、ハイステイタス対象のプレミアムステイタスパーティー';
$localCSS = array(
	ROOT.'/common/js/jquery/datepicker/jquery-ui.min.css',
	ROOT.'/common/js/jquery/datepicker/jquery-ui.theme.min.css',
	ROOT.'/s_css/index.css',
);
$localJS = array(
	ROOT.'/common/js/jquery/datepicker/jquery-ui.min.js',
	ROOT.'/common/js/jquery/datepicker/jquery.ui.datepicker-ja.min.js',
	ROOT.'/js/index.js',
);

//ページカテゴリ
$page_cat = 'top_page';

?>

	<?php require_once('common/inc/top.php');?>
	<div class="mvArea">
		<div data-htmlmodule data-slidearea02 data-lf-area>
			<div class="mod_wrap02">
				<p class="catch">
					<picture>
						<source srcset="img/mv_txt_sp.png" <?php echo SC_SP;?>>
						<img src="img/mv_txt.png" width="670" height="220" alt="大手企業・経営者など男性資格限定上質な恋活パーティー">
					</picture>
				</p>
				<p class="petan">
					<picture>
						<source srcset="img/mv_petan_sp.png" <?php echo SC_SP;?>>
						<img src="img/mv_petan.png" width="566" height="157" alt="">
					</picture>
				</p>
			</div>
			<div class="btns01">
				<button class="prev"></button>
				<button class="next"></button>
			</div>
			<div class="imgs">
				<div class="img" data-lf="<?php echo ROOT;?>/img/mv01.jpg"></div>
				<div class="img" data-lf="<?php echo ROOT;?>/img/mv02.jpg"></div>
				<div class="img" data-lf="<?php echo ROOT;?>/img/mv03.jpg"></div>
				<div class="img" data-lf="<?php echo ROOT;?>/img/mv04.jpg"></div>
				<div class="img" data-lf="<?php echo ROOT;?>/img/mv05.jpg"></div>
				<div class="img" data-lf="<?php echo ROOT;?>/img/mv06.jpg"></div>
				<div class="img" data-lf="<?php echo ROOT;?>/img/mv07.jpg"></div>
				<div class="img" data-lf="<?php echo ROOT;?>/img/mv08.jpg"></div>
                <div class="img" data-lf="<?php echo ROOT;?>/img/mv09.jpg"></div>
			</div>
			<div class="pager"></div>
		</div>
	</div>

	<section id="Party" class="sec_party">
		<header>
			<h2 class="sec_tit party"><span class="alt">パーティ検索</span></h2>
		</header>
		<div class="in">
			<div class="mod_wrap01">

				<div class="left">
					<dl class="searchArea" data-sc-pc>
						<dt class="search_tit"><span>エリアから探す</span></dt>
						<dd class="detail">
							<dl>
								<dt class="areaLarge">
								<a href="/schedule/area.php?" class="mod_btn04"><span>関東</span></a>
							</dt>
								<dd class="areaSmall">
									<ul class="areaList">
										<li><a href="/schedule/area.php?"><span>銀座</span></a></li>
										<li><a href="/schedule/area.php?"><span>新宿</span></a></li>
										<li><a href="/schedule/area.php?"><span>恵比寿</span></a></li>
										<li><a href="/schedule/area.php?"><span>六本木</span></a></li>
										<li><a href="/schedule/area.php?"><span>霞ヶ関</span></a></li>
										<li><a href="/schedule/area.php?"><span>品川</span></a></li>
										<li><a href="/schedule/area.php?"><span>東京</span></a></li>
										<li><a href="/schedule/area.php?"><span>目黒</span></a></li>
										<li><a href="/schedule/area.php?"><span>表参道</span></a></li>
										<li><a href="/schedule/area.php?"><span>渋谷</span></a></li>
										<li><a href="/schedule/area.php?"><span>横浜</span></a></li>
										<li><a href="/schedule/area.php?"><span>大宮</span></a></li>
										<li><a href="/schedule/area.php?"><span>千葉</span></a></li>
									</ul>
								</dd>
							</dl>
						</dd>
						<dd class="detail">
							<dl>
								<dt class="areaLarge">
								<a href="/schedule/area.php?" class="mod_btn04"><span>東海</span></a>
							</dt>
								<dd class="areaSmall single">
									<ul class="areaList">
										<li><a href="/schedule/area.php?"><span>名古屋</span></a></li>
										<li><a href="/schedule/area.php?"><span>岐阜</span></a></li>
									</ul>
								</dd>
							</dl>
						</dd>
						<dd class="detail">
							<dl>
								<dt class="areaLarge">
								<a href="/schedule/area.php?" class="mod_btn04"><span>関西</span></a>
							</dt>
								<dd class="areaSmall">
									<ul class="areaList">
										<li><a href="/schedule/area.php?"><span>大阪</span></a></li>
										<li><a href="/schedule/area.php?"><span>神戸</span></a></li>
										<li><a href="/schedule/area.php?"><span>京都</span></a></li>
										<li><a href="/schedule/area.php?"><span>和歌山</span></a></li>
										<li><a href="/schedule/area.php?"><span>奈良</span></a></li>
									</ul>
								</dd>
							</dl>
						</dd>
						<dd class="detail">
							<dl>
								<dt class="areaLarge">
								<a href="/schedule/area.php?" class="mod_btn04"><span>信越・北陸</span></a>
							</dt>
								<dd class="areaSmall single">
									<ul class="areaList">
										<li><a href="/schedule/area.php?"><span>新潟</span></a></li>
										<li><a href="/schedule/area.php?"><span>金沢</span></a></li>
									</ul>
								</dd>
							</dl>
						</dd>
						<dd class="detail">
							<dl>
								<dt class="areaLarge">
								<a href="/schedule/area.php?" class="mod_btn04"><span>北海道・東北</span></a>
							</dt>
								<dd class="areaSmall single">
									<ul class="areaList">
										<li><a href="/schedule/area.php?"><span>札幌</span></a></li>
										<li><a href="/schedule/area.php?"><span>仙台</span></a></li>
										<li><a href="/schedule/area.php?"><span>石巻</span></a></li>
										<li><a href="/schedule/area.php?"><span>大崎</span></a></li>
									</ul>
								</dd>
							</dl>
						</dd>
						<dd class="detail">
							<dl>
								<dt class="areaLarge">
								<a href="/schedule/area.php?" class="mod_btn04"><span>九州・沖縄</span></a>
							</dt>
								<dd class="areaSmall single">
									<ul class="areaList">
										<li><a href="/schedule/area.php?"><span>福岡</span></a></li>
									</ul>
								</dd>
							</dl>
						</dd>
					</dl>
					<dl class="searchArea" data-sc-sp>
						<dt class="search_tit"><span>エリアから探す</span></dt>
						<dd class="detail">
							<ul class="areaLarge">
								<li>
									<a href="/schedule/area.php?" class="mod_btn04 large">
										<span>東京・横浜</span>
									</a>
								</li>
								<li>
									<a href="/schedule/area.php?" class="mod_btn04 large">
										<span>大阪・神戸・京都</span>
									</a>
								</li>
								<li>
									<a href="/schedule/area.php?" class="mod_btn04 large">
										<span>名古屋</span>
									</a>
								</li>
								<li>
									<a href="/schedule/area.php?" class="mod_btn04 large">
										<span>その他開催エリア</span>
									</a>
								</li>
							</ul>
						</dd>
					</dl>
					<dl class="searchMap" data-sc-sp>
						<dt class="search_tit"><span>地図から探す</span></dt>
						<dd class="detail">
							<div class="map">
								<ul class="areaMap">
									<li class="p01 w03"><a href="/schedule/area.php?"><span>北海道・東北</span></a></li>
									<li class="p02 w02"><a href="/schedule/area.php?"><span>信越・北陸</span></a></li>
									<li class="p03 w02"><a href="/schedule/area.php?"><span>九州・沖縄</span></a></li>
									<li class="p04"><a href="/schedule/area.php?"><span>関東</span></a></li>
									<li class="p05"><a href="/schedule/area.php?"><span>東海</span></a></li>
									<li class="p06"><a href="/schedule/area.php?"><span>関西</span></a></li>
								</ul>
							</div>
						</dd>
					</dl>
				</div>

				<div class="right">
					<dl class="searchDate">
						<dt class="search_tit"><span>日程から探す</span></dt>
						<dd class="detail">
							<form class="formArea" id="Form" autocomplete="on" enctype="multipart/form-data" name="form" method="post" action="./">
								<dl class="element" data-need="" data-form-unique="selectArea">
									<dt><span>開催地</span></dt>
									<dd>
										<div class="itemWrap selectWrap selectAreaWrap">
											<span class="select"><select id="selectArea" name="selectArea"  >
									<option value="">--</option>
									<option value="プルダウン１">プルダウン１</option>
									<option value="プルダウン２">プルダウン２</option>
									<option value="プルダウン３">プルダウン３</option>
									<optgroup label=""></optgroup>
									</select>
									</span>
										</div>
									</dd>
								</dl>

								<dl class="element">
									<dt><span>日付</span></dt>
									<dd>
										<div class="itemWrap textWrap calender01_wrap" data-form-unique="calender01">
											<label for="calender01"><input name="calender01" id="calender01" class="cal" value="" data-calender placeholder="" type="text"></label></div>
										<div class="itemWrap textWrap calender02_wrap" data-form-unique="calender02">
											<label for="calender02"><input name="calender02" id="calender02" class="cal" value="" data-calender placeholder="" type="text"></label></div>
									</dd>
								</dl>

								<dl class="element" data-need="" data-need-needcheckval="1" data-form-unique="checkbox_week">
									<dt><span>曜日</span></dt>
									<dd>
										<div class="itemWrap checkboxWrap checkbox_weekBox">
											<span class="checkbox">
											<input type="checkbox" name="checkbox_week[]" value="月" id="checkbox_week0">
											<label for="checkbox_week0"><span>月</span></label>
											</span>
											<span class="checkbox">
											<input type="checkbox" name="checkbox_week[]" value="火" id="checkbox_week1">
											<label for="checkbox_week1"><span>火</span></label>
											</span>
											<span class="checkbox">
											<input type="checkbox" name="checkbox_week[]" value="水" id="checkbox_week2">
											<label for="checkbox_week2"><span>水</span></label>
											</span>
											<span class="checkbox">
											<input type="checkbox" name="checkbox_week[]" value="木" id="checkbox_week3">
											<label for="checkbox_week3"><span>木</span></label>
											</span>
											<span class="checkbox">
											<input type="checkbox" name="checkbox_week[]" value="金" id="checkbox_week4">
											<label for="checkbox_week4"><span>金</span></label>
											</span>
											<span class="checkbox">
											<input type="checkbox" name="checkbox_week[]" value="土" id="checkbox_week5">
											<label for="checkbox_week5"><span>土</span></label>
											</span>
											<span class="checkbox">
											<input type="checkbox" name="checkbox_week[]" value="日" id="checkbox_week6">
											<label for="checkbox_week6"><span>日</span></label>
											</span>
										</div>

										<label for="searchBtn" class="search mod_btn01 bgc04">
										<input id="searchBtn" value="検 索" type="submit">
									</label>
									</dd>
								</dl>

							</form>
						</dd>
					</dl>
					<dl class="searchEvent">
						<dt class="search_tit"><span>企画から探す</span></dt>
						<dd class="detail">
							<ul>
								<li>
									<a href="/schedule/event.php?" class="mod_btn04 large bgc01">
										<span>立食<br>100〜300名</span>
									</a>
								</li>
								<li>
									<a href="/schedule/event.php?" class="mod_btn04 large bgc01">
										<span>立食<br>100名以下</span>
									</a>
								</li>
								<li>
									<a href="/schedule/event.php?" class="mod_btn04 large bgc01">
										<span>着席<br>全員会話</span>
									</a>
								</li>
								<li>
									<a href="/schedule/event.php?" class="mod_btn04 large bgc01">
										<span>20代中心</span>
									</a>
								</li>
								<li>
									<a href="/schedule/event.php?" class="mod_btn04 large bgc01">
										<span>30代40代<br>以上</span>
									</a>
								</li>
								<li>
									<a href="/schedule/event.php?" class="mod_btn04 large bgc01">
										<span>特別企画</span>
									</a>
								</li>
							</ul>
						</dd>
					</dl>
					<dl class="searchDetail">
						<dt class="search_tit"><span>詳細から探す</span></dt>
						<dd class="detail">
							<div class="link">
								<a href="/schedule/?" class="mod_btn04 bgc01"><span>検索</span></a>
							</div>
						</dd>
					</dl>
				</div>

			</div>
		</div>
	</section>

	<dl class="regist_box" data-sc-sp>
		<dt><img src="<?php echo ROOT;?>/common/img/regist_tit.png" width="326" height="18" alt="PREMIUM STATUS OFFICIAL"></dt>
		<dd>
			<p class="cp">無料会員登録でご優待情報をGET!</p>
			<a href="<?php echo LINK_AS; ?>" target="_blank" class="store"><span><img src="<?php echo ROOT;?>/common/img/regist_app_store.png" width="166" height="50" alt="AppStore"></span></a>
			<a href="<?php echo LINK_GP; ?>" target="_blank" class="store"><span><img src="<?php echo ROOT;?>/common/img/regist_google_play.png" width="166" height="50" alt="GooglePlay"></span></a>

			<a href="<?php echo LINK_LINE; ?>" target="_blank" class="line"><span><img src="<?php echo ROOT;?>/common/img/regist_icon_line.png" width="48" height="48" alt="" data-sc-pc><img src="<?php echo ROOT;?>/common/img/regist_icon_line_sp.png" width="182" height="47" alt="" data-sc-sp></span></a>
			<span class="qr"><img src="<?php echo ROOT;?>/common/img/regist_qr_line.png" width="86" height="86" alt=""></span>
		</dd>
	</dl>

	<section id="Voice" class="sec_voice">
		<header>
			<h2 class="sec_tit"><span class="alt">パーティー報告＆参加者の声 VOICE</span></h2>
			<p class="read">恋活・婚活パーティー「プレミアムステイタス」のパーティー報告、参加者の声、お勧めパーティーファッションをご覧頂けます。</p>
		</header>

		<div class="mod_wrap02">
			<div class="in">
				<div class="voice_box interview" data-pc-autoheight="vc">
					<a href="/report/detail.php" class="wrapLink"></a>
					<figure data-lf-area data-lf="img/voice_ph01.jpg" class="ph"></figure>
					<dl class="detail">
						<dt data-autoheight="vc_tit">【名古屋】土曜日の夜は栄えの隠れ家で大人のパーティ♪♪<br>好評トヨタ系企業企画！</dt>
						<dd>
							<p>開催日：2017.09.23 SAT 19:00〜21:30</p>
							<p>会　場：Party&amp;Dining「FAMILLA」</p>
						</dd>
					</dl>
				</div>

				<div class="voice_box fashion" data-pc-autoheight="vc">
					<a href="/report/detail.php" class="wrapLink"></a>
					<figure data-lf-area data-lf="img/voice_ph02.jpg" class="ph"></figure>
					<dl class="detail">
						<dt data-autoheight="vc_tit">【名古屋】土曜日の夜は栄えの隠れ家で大人のパーティ♪♪<br>好評トヨタ系企業企画！</dt>
						<dd>
							<p>テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります…</p>
						</dd>
					</dl>
				</div>

				<div class="voice_box report" data-autoheight="vc">
					<a href="/report/detail.php" class="wrapLink"></a>
					<figure data-lf-area data-lf="img/voice_ph03.jpg" class="ph"></figure>
					<dl class="detail">
						<dt data-autoheight="vc_tit">【名古屋】土曜日の夜は栄えの隠れ家で大人のパーティ♪♪<br>好評トヨタ系企業企画！</dt>
						<dd>
							<p>開催日：2017.09.23 SAT 19:00〜21:30</p>
							<p>会　場：Party&amp;Dining「FAMILLA」</p>
						</dd>
					</dl>
				</div>

				<div class="voice_box fashion" data-pc-autoheight="vc">
					<a href="/report/detail.php" class="wrapLink"></a>
					<figure data-lf-area data-lf="img/voice_ph02.jpg" class="ph"></figure>
					<dl class="detail">
						<dt data-autoheight="vc_tit">【名古屋】土曜日の夜は栄えの隠れ家で大人のパーティ♪♪<br>好評トヨタ系企業企画！</dt>
						<dd>
							<p>テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります。テキスト入ります…</p>
						</dd>
					</dl>
				</div>

				<div class="link">
					<a href="/report/" class="mod_btn01 tp02"><span>さらに詳細を見る</span></a>
				</div>

			</div>
		</div>
	</section>

	<section id="About" class="sec_about">
		<div class="in">
			<header>
				<h2 class="sec_tit"><span class="alt">プレミアムステイタスとは ABOUT</span></h2>
				<div class="wrap">
					<p class="read">男性資格限定の恋活＆婚活パーティー</p>
					<p class="txt">プレミアムステイタスパーティーは、東京・横浜・大阪・神戸・京都・名古屋・札幌・福岡等にて、ハイステイタス男性を対象とした立食形式中心の恋活・婚活パーティー＆1店舗型の街コンブランドです。大手企業・外資企業・公務員・医師・経営者等の男性、OL・看護師・美容系・客室乗務員・アパレル系等の女性との出会いをお届けします。</p>
					<figure><img src="img/about_deco_txt.png" width="280" height="112" alt="We offer good quality making"></figure>
				</div>
			</header>

			<div class="mod_wrap01">
				<dl class="point_box">
					<dt>
					<div class="petan">
					<picture>
							<source srcset="img/point_petan_txt_sp.png" <?php echo SC_SP;?>>
							<img src="img/point_petan_txt.png" width="485" height="73" alt="" class="txt">
					</picture>
					</div>
				</dt>
					<dd>
						<div class="detail_box p01" data-autoheight="point">
							<h3 class="title">男性はハイステイタスの<br>方限定</h3>
							<figure data-lf-area data-lf="<?php echo ROOT;?>/img/point_ph01.jpg" class="ph"></figure>
							<p class="txt">男性資格を上場・大手・外資・公務員・医師・経営者・会計士などハイステイタス／ビジネスエリートに限定。質の高い出会いをお届け。</p>
						</div>

						<div class="detail_box p02" data-autoheight="point">
							<h3 class="title">資格証・身分証<br>100%提示</h3>
							<figure data-lf-area data-lf="<?php echo ROOT;?>/img/point_ph02.jpg" class="ph"></figure>
							<p class="txt">男性は名刺・社員証・国家資格証明書・源泉徴収票などの資格証明書、免許証や保険証などの本人証明書は男女共に提示必須で安心。</p>
						</div>

						<div class="detail_box p03" data-autoheight="point">
							<h3 class="title">200名パーティーを<br>毎週開催</h3>
							<figure data-lf-area data-lf="<?php echo ROOT;?>/img/point_ph03.jpg" class="ph"></figure>
							<p class="txt">業界で唯一。男性をハイステイタス／ビジネスエリートに限定した200名〜300名の恋活婚活パーティー＆1店舗型街コンを毎週末開催。</p>
						</div>

						<div class="detail_box p04" data-autoheight="point">
							<h3 class="title">自社主催パーティー<br>年間1,200本！</h3>
							<figure data-lf-area data-lf="<?php echo ROOT;?>/img/point_ph04.jpg" class="ph"></figure>
							<p class="txt">自社主催の恋活婚活パーティー＆1店舗型街コンを年間1,200本以上開催、動員80,000名以上。自社による企画運営にこだわります。</p>
						</div>

						<div class="detail_box p05" data-autoheight="point">
							<h3 class="title">立食・着席形式の<br>2スタイル</h3>
							<figure data-lf-area data-lf="<?php echo ROOT;?>/img/point_ph05.jpg" class="ph"></figure>
							<p class="txt">立食形式で自由に交流頂く恋活婚活パーティーが中心ですが、強いご要望にお答えして、着席全員会話のパーティーも開催しています。</p>
						</div>

						<div class="detail_box p06" data-autoheight="point">
							<h3 class="title">ドレスコードを設け<br>質を維持</h3>
							<figure data-lf-area data-lf="<?php echo ROOT;?>/img/point_ph06.jpg" class="ph"></figure>
							<p class="txt">男性はジャケットやドレスシャツ必須、短パンやTシャツ不可など、ドレスコードを設け、恋活・婚活パーティーの雰囲気・質向上を目指します。</p>
						</div>

						<div class="detail_box p07" data-autoheight="point">
							<h3 class="title">ワインやカクテル・<br>お食事も</h3>
							<figure data-lf-area data-lf="<?php echo ROOT;?>/img/point_ph07.jpg" class="ph"></figure>
							<p class="txt">会場となるレストランのお食事やワイン・ビール・カクテルなどのドリンクもお楽しみ頂けます。パーティー会場によってはスイーツも。</p>
						</div>

						<div class="detail_box p08" data-autoheight="point">
							<h3 class="title">婚活パーティー10年以上の<br>実績</h3>
							<figure data-lf-area data-lf="<?php echo ROOT;?>/img/point_ph08.jpg" class="ph"></figure>
							<p class="txt">当社は2007年より恋活婚活パーティー＆1店舗型街コンを開催。役員は20年以上婚活業界で素敵な出会い・婚活の機会をお届けしています。</p>
						</div>
					</dd>
				</dl>

				<div class="link">
					<a href="/about/" class="mod_btn01 tp02"><span>さらに詳細を見る</span></a>
				</div>
			</div>
		</div>
	</section>

	<section id="Recommend" class="sec_recommend">
		<div class="in" data-lf-area data-lf="img/recommend_bg.jpg">
			<header>
				<h2 class="sec_tit"><span class="alt">おすすめパーティー</span></h2>
				<p class="read">プレミアムステイタスがお届けする東京・大阪・名古屋・福岡でのおすすめ婚活パーティー、男性30代40代中心企画や自衛隊員限定のおすすめパーティーを掲載。</p>
			</header>

			<div class="mod_wrap01">
				<div class="recommend_box" data-autoheight="recomm">
					<a href="/schedule/detail.php" class="wrapLink"></a>
					<p class="area">東京<span class="alex">Tokyo</span></p>
					<dl class="detail">
						<dt>100名特別企画☆資格限定<br>サンセット BBQパーティー</dt>
						<dd>10/23（土）20:00〜22:30</dd>
					</dl>
					<figure class="ph" data-lf-area data-lf="img/recommend_ph01.jpg"></figure>
				</div>

				<div class="recommend_box" data-autoheight="recomm">
					<a href="/schedule/detail.php" class="wrapLink"></a>
					<p class="area">大阪<span class="alex">Osaka</span></p>
					<dl class="detail">
						<dt>男性35歳以下ビジネスエリート<br>VS 女性32歳以下★婚活恋活</dt>
						<dd>10/29（日）16:00〜13:80</dd>
					</dl>
					<figure class="ph" data-lf-area data-lf="img/recommend_ph02.jpg"></figure>
				</div>

				<div class="recommend_box" data-autoheight="recomm">
					<a href="/schedule/detail.php" class="wrapLink"></a>s
					<p class="area">名古屋<span class="alex">Nagoya</span></p>
					<dl class="detail">
						<dt>名古屋70名☆ヤングエリート恋活<br>男性32歳以下＆一部上場企業</dt>
						<dd>10/22（日）17:00〜19:30</dd>
					</dl>
					<figure class="ph" data-lf-area data-lf="img/recommend_ph03.jpg"></figure>
				</div>

				<div class="link">
					<a href="/schedule/" class="mod_btn01 tp02"><span>もっと見る</span></a>
				</div>
			</div>
		</div>
	</section>


	<div class="info_block">
		<div class="mod_wrap01">
			<section id="Blog" class="sec_blog" data-pc-autoheight="infoBlock">
				<header>
					<h2 class="sec_tit tp02"><span class="alt">婚活成功ブログ</span></h2>
					<p class="link"><a href="http://www.statusparty.jp/lightup/" target="_blank" class="mod_btn02 tp02"><span>もっと見る</span></a></p>
				</header>
				<p class="read">婚活パーティー・街コン・合コン、恋人探しや交際に関するお役立ち情報です。素敵な出会いのチャンスをゲットするノウハウも！</p>
				<div class="loop_wrap">
					<div class="info_box">
						<a href="" class="wrapLink"></a>
						<figure class="ph" data-lf-area data-lf="img/blog_ph01.jpg"></figure>
						<p class="category"><span>恋人</span></p>
						<h3 class="title">【恋愛癖は変えられる！】恋愛が長続きしない人の特徴と解決策</h3>
						<p class="date">2017.09.15</p>
					</div>

					<div class="info_box">
						<a href="" class="wrapLink"></a>
						<figure class="ph" data-lf-area data-lf="img/blog_ph02.jpg"></figure>
						<p class="category"><span>恋人</span></p>
						<h3 class="title">【恋愛癖は変えられる！】恋愛が長続きしない人の特徴と解決策</h3>
						<p class="date">2017.09.15</p>
					</div>

					<div class="info_box">
						<a href="" class="wrapLink"></a>
						<figure class="ph" data-lf-area data-lf="img/blog_ph03.jpg"></figure>
						<p class="category"><span>恋人</span></p>
						<h3 class="title">【恋愛癖は変えられる！】恋愛が長続きしない人の特徴と解決策</h3>
						<p class="date">2017.09.15</p>
					</div>
				</div>
			</section>

			<section id="News" class="sec_news" data-pc-autoheight="infoBlock">
				<header>
					<h2 class="sec_tit tp02"><span class="alt">お知らせ</span></h2>
					<?php /*<p class="link"><a href="" class="mod_btn02 tp02"><span>もっと見る</span></a></p>*/ ;?>
				</header>
				<div class="loop_wrap">
					<div class="info_box">
						<a href="" class="wrapLink"></a>
						<p class="date">2017.09.15 <span class="new">新着</span></p>
						<h3 class="title">プレミアムステイタスのサイトリニューアル</h3>
					</div>

					<div class="info_box">
						<a href="" class="wrapLink"></a>
						<p class="date">2017.09.15</p>
						<h3 class="title">年間動員数80,000人突破！</h3>
					</div>

					<div class="info_box">
						<a href="" class="wrapLink"></a>
						<p class="date">2017.09.15</p>
						<h3 class="title">年間動員数80,000人突破！</h3>
					</div>

					<div class="info_box">
						<a href="" class="wrapLink"></a>
						<p class="date">2017.09.15</p>
						<h3 class="title">年間動員数80,000人突破！</h3>
					</div>

					<div class="info_box">
						<a href="" class="wrapLink"></a>
						<p class="date">2017.09.15</p>
						<h3 class="title">年間動員数80,000人突破！</h3>
					</div>
				</div>
				<!--/.loop_wrap-->

				<dl class="regist_box">
					<dt><img src="<?php echo ROOT;?>/common/img/regist_tit.png" width="326" height="18" alt="PREMIUM STATUS OFFICIAL"></dt>
					<dd>
						<p class="cp">無料会員登録でご優待情報をGET!</p>
						<a href="<?php echo LINK_AS; ?>" target="_blank" class="store"><span><img src="<?php echo ROOT;?>/common/img/regist_app_store.png" width="166" height="50" alt="AppStore"></span></a>
						<a href="<?php echo LINK_GP; ?>" target="_blank" class="store"><span><img src="<?php echo ROOT;?>/common/img/regist_google_play.png" width="166" height="50" alt="GooglePlay"></span></a>

						<a href="<?php echo LINK_LINE; ?>" target="_blank" class="line"><span><img src="<?php echo ROOT;?>/common/img/regist_icon_line.png" width="50" height="50" alt="" data-sc-pc><img src="<?php echo ROOT;?>/common/img/regist_icon_line_sp.png" width="182" height="47" alt="" data-sc-sp></span></a>
						<span class="qr"><img src="<?php echo ROOT;?>/common/img/regist_qr_line.png" width="86" height="86" alt=""></span>
					</dd>
				</dl>
			</section>
		</div>
	</div>

	<div class="sns_block">
		<div class="mod_wrap01">
			<div class="sec_insta">
				<header>
					<h2 class="sec_tit tp02"><span class="alt">Instagram</span></h2>
					<p class="link"><a href="<?php echo LINK_INSTA; ?>" target="_blank" class="mod_btn02 tp02"><span>もっと見る</span></a></p>
				</header>

				<div class="instagramlist" data-instagram01>
					<div class="load_anim"></div>
					<div class="contentWrap row01">
						<ul class="content"></ul>
					</div>
					<div class="contentWrap row02">
						<ul class="content"></ul>
					</div>
				</div>
				<p class="txt">プレミアムステイタスの婚活パーティーや上場企業・経営者・公務員・医師など参加者の様子をInstagramに掲載！</p>
			</div>

			<div class="sec_fb">
				<header>
					<h1 class="sec_tit tp02"><span class="alt">Facebook</span></h1>
					<p class="link"><a href="<?php echo LINK_FB; ?>" target="_blank" class="mod_btn02 tp02"><span>もっと見る</span></a></p>
				</header>
				<div class="fb-page" data-href="https://www.facebook.com/premiumstatusparty/" data-tabs="timeline" data-width="500px" data-height="355" data-small-header="false" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true">
					<blockquote cite="https://www.facebook.com/premiumstatusparty/" class="fb-xfbml-parse-ignore"><a href="https://www.facebook.com/premiumstatusparty/">男性エリート限定の200名婚活パーティー：プレミアムステイタス</a></blockquote>
				</div>

				<p class="txt">公式Facebookページでもプレミアムステイタスのパーティーやイベント報告など、気になる情報を掲載！初めての方は必見です。</p>
			</div>
		</div>
	</div>

	<?php require_once('common/inc/bottom.php');?>