/*! modernizr 3.5.0 (Custom Build) | MIT *
 * https://modernizr.com/download/?-cssfilters-csspointerevents-touchevents-videoautoplay-webgl-setclasses !*/
!function(A,e,t){function n(A,e){return typeof A===e}function o(){var A,e,t,o,r,i,l;for(var a in E)if(E.hasOwnProperty(a)){if(A=[],e=E[a],e.name&&(A.push(e.name.toLowerCase()),e.options&&e.options.aliases&&e.options.aliases.length))for(t=0;t<e.options.aliases.length;t++)A.push(e.options.aliases[t].toLowerCase());for(o=n(e.fn,"function")?e.fn():e.fn,r=0;r<A.length;r++)i=A[r],l=i.split("."),1===l.length?Modernizr[l[0]]=o:(!Modernizr[l[0]]||Modernizr[l[0]]instanceof Boolean||(Modernizr[l[0]]=new Boolean(Modernizr[l[0]])),Modernizr[l[0]][l[1]]=o),R.push((o?"":"no-")+l.join("-"))}}function r(A){var e=T.className,t=Modernizr._config.classPrefix||"";if(B&&(e=e.baseVal),Modernizr._config.enableJSClass){var n=new RegExp("(^|\\s)"+t+"no-js(\\s|$)");e=e.replace(n,"$1"+t+"js$2")}Modernizr._config.enableClasses&&(e+=" "+t+A.join(" "+t),B?T.className.baseVal=e:T.className=e)}function i(){return"function"!=typeof e.createElement?e.createElement(arguments[0]):B?e.createElementNS.call(e,"http://www.w3.org/2000/svg",arguments[0]):e.createElement.apply(e,arguments)}function l(A,e){if("object"==typeof A)for(var t in A)x(A,t)&&l(t,A[t]);else{A=A.toLowerCase();var n=A.split("."),o=Modernizr[n[0]];if(2==n.length&&(o=o[n[1]]),"undefined"!=typeof o)return Modernizr;e="function"==typeof e?e():e,1==n.length?Modernizr[n[0]]=e:(!Modernizr[n[0]]||Modernizr[n[0]]instanceof Boolean||(Modernizr[n[0]]=new Boolean(Modernizr[n[0]])),Modernizr[n[0]][n[1]]=e),r([(e&&0!=e?"":"no-")+n.join("-")]),Modernizr._trigger(A,e)}return Modernizr}function a(){var A=e.body;return A||(A=i(B?"svg":"body"),A.fake=!0),A}function s(A,t,n,o){var r,l,s,c,u="modernizr",d=i("div"),p=a();if(parseInt(n,10))for(;n--;)s=i("div"),s.id=o?o[n]:u+(n+1),d.appendChild(s);return r=i("style"),r.type="text/css",r.id="s"+u,(p.fake?p:d).appendChild(r),p.appendChild(d),r.styleSheet?r.styleSheet.cssText=A:r.appendChild(e.createTextNode(A)),d.id=u,p.fake&&(p.style.background="",p.style.overflow="hidden",c=T.style.overflow,T.style.overflow="hidden",T.appendChild(p)),l=t(d,A),p.fake?(p.parentNode.removeChild(p),T.style.overflow=c,T.offsetHeight):d.parentNode.removeChild(d),!!l}function c(A,e){return!!~(""+A).indexOf(e)}function u(A){return A.replace(/([a-z])-([a-z])/g,function(A,e,t){return e+t.toUpperCase()}).replace(/^-/,"")}function d(A,e){return function(){return A.apply(e,arguments)}}function p(A,e,t){var o;for(var r in A)if(A[r]in e)return t===!1?A[r]:(o=e[A[r]],n(o,"function")?d(o,t||e):o);return!1}function f(A){return A.replace(/([A-Z])/g,function(A,e){return"-"+e.toLowerCase()}).replace(/^ms-/,"-ms-")}function h(e,t,n){var o;if("getComputedStyle"in A){o=getComputedStyle.call(A,e,t);var r=A.console;if(null!==o)n&&(o=o.getPropertyValue(n));else if(r){var i=r.error?"error":"log";r[i].call(r,"getComputedStyle returning null, its possible modernizr test results are inaccurate")}}else o=!t&&e.currentStyle&&e.currentStyle[n];return o}function m(e,n){var o=e.length;if("CSS"in A&&"supports"in A.CSS){for(;o--;)if(A.CSS.supports(f(e[o]),n))return!0;return!1}if("CSSSupportsRule"in A){for(var r=[];o--;)r.push("("+f(e[o])+":"+n+")");return r=r.join(" or "),s("@supports ("+r+") { #modernizr { position: absolute; } }",function(A){return"absolute"==h(A,null,"position")})}return t}function g(A,e,o,r){function l(){s&&(delete M.style,delete M.modElem)}if(r=n(r,"undefined")?!1:r,!n(o,"undefined")){var a=m(A,o);if(!n(a,"undefined"))return a}for(var s,d,p,f,h,g=["modernizr","tspan","samp"];!M.style&&g.length;)s=!0,M.modElem=i(g.shift()),M.style=M.modElem.style;for(p=A.length,d=0;p>d;d++)if(f=A[d],h=M.style[f],c(f,"-")&&(f=u(f)),M.style[f]!==t){if(r||n(o,"undefined"))return l(),"pfx"==e?f:!0;try{M.style[f]=o}catch(w){}if(M.style[f]!=h)return l(),"pfx"==e?f:!0}return l(),!1}function w(A,e,t,o,r){var i=A.charAt(0).toUpperCase()+A.slice(1),l=(A+" "+b.join(i+" ")+i).split(" ");return n(e,"string")||n(e,"undefined")?g(l,e,o,r):(l=(A+" "+Y.join(i+" ")+i).split(" "),p(l,e,t))}function y(A,e,n){return w(A,t,t,e,n)}var R=[],E=[],v={_version:"3.5.0",_config:{classPrefix:"",enableClasses:!0,enableJSClass:!0,usePrefixes:!0},_q:[],on:function(A,e){var t=this;setTimeout(function(){e(t[A])},0)},addTest:function(A,e,t){E.push({name:A,fn:e,options:t})},addAsyncTest:function(A){E.push({name:null,fn:A})}},Modernizr=function(){};Modernizr.prototype=v,Modernizr=new Modernizr;var T=e.documentElement,B="svg"===T.nodeName.toLowerCase(),G=v._config.usePrefixes?" -webkit- -moz- -o- -ms- ".split(" "):["",""];v._prefixes=G,Modernizr.addTest("webgl",function(){var e=i("canvas"),t="probablySupportsContext"in e?"probablySupportsContext":"supportsContext";return t in e?e[t]("webgl")||e[t]("experimental-webgl"):"WebGLRenderingContext"in A}),Modernizr.addTest("csspointerevents",function(){var A=i("a").style;return A.cssText="pointer-events:auto","auto"===A.pointerEvents});var F="CSS"in A&&"supports"in A.CSS,C="supportsCSS"in A;Modernizr.addTest("supports",F||C),Modernizr.addTest("video",function(){var A=i("video"),e=!1;try{e=!!A.canPlayType,e&&(e=new Boolean(e),e.ogg=A.canPlayType('video/ogg; codecs="theora"').replace(/^no$/,""),e.h264=A.canPlayType('video/mp4; codecs="avc1.42E01E"').replace(/^no$/,""),e.webm=A.canPlayType('video/webm; codecs="vp8, vorbis"').replace(/^no$/,""),e.vp9=A.canPlayType('video/webm; codecs="vp9"').replace(/^no$/,""),e.hls=A.canPlayType('application/x-mpegURL; codecs="avc1.42E01E"').replace(/^no$/,""))}catch(t){}return e});var x;!function(){var A={}.hasOwnProperty;x=n(A,"undefined")||n(A.call,"undefined")?function(A,e){return e in A&&n(A.constructor.prototype[e],"undefined")}:function(e,t){return A.call(e,t)}}(),v._l={},v.on=function(A,e){this._l[A]||(this._l[A]=[]),this._l[A].push(e),Modernizr.hasOwnProperty(A)&&setTimeout(function(){Modernizr._trigger(A,Modernizr[A])},0)},v._trigger=function(A,e){if(this._l[A]){var t=this._l[A];setTimeout(function(){var A,n;for(A=0;A<t.length;A++)(n=t[A])(e)},0),delete this._l[A]}},Modernizr._q.push(function(){v.addTest=l}),Modernizr.addAsyncTest(function(){function A(i){o++,clearTimeout(e);var a=i&&"playing"===i.type||0!==r.currentTime;return!a&&n>o?void(e=setTimeout(A,t)):(r.removeEventListener("playing",A,!1),l("videoautoplay",a),void(r.parentNode&&r.parentNode.removeChild(r)))}var e,t=200,n=5,o=0,r=i("video"),a=r.style;if(!(Modernizr.video&&"autoplay"in r))return void l("videoautoplay",!1);a.position="absolute",a.height=0,a.width=0;try{if(Modernizr.video.ogg)r.src="data:video/ogg;base64,T2dnUwACAAAAAAAAAABmnCATAAAAAHDEixYBKoB0aGVvcmEDAgEAAQABAAAQAAAQAAAAAAAFAAAAAQAAAAAAAAAAAGIAYE9nZ1MAAAAAAAAAAAAAZpwgEwEAAAACrA7TDlj///////////////+QgXRoZW9yYSsAAABYaXBoLk9yZyBsaWJ0aGVvcmEgMS4xIDIwMDkwODIyIChUaHVzbmVsZGEpAQAAABoAAABFTkNPREVSPWZmbXBlZzJ0aGVvcmEtMC4yOYJ0aGVvcmG+zSj3uc1rGLWpSUoQc5zmMYxSlKQhCDGMYhCEIQhAAAAAAAAAAAAAEW2uU2eSyPxWEvx4OVts5ir1aKtUKBMpJFoQ/nk5m41mUwl4slUpk4kkghkIfDwdjgajQYC8VioUCQRiIQh8PBwMhgLBQIg4FRba5TZ5LI/FYS/Hg5W2zmKvVoq1QoEykkWhD+eTmbjWZTCXiyVSmTiSSCGQh8PB2OBqNBgLxWKhQJBGIhCHw8HAyGAsFAiDgUCw8PDw8PDw8PDw8PDw8PDw8PDw8PDw8PDw8PDw8PDw8PDw8PDw8PDw8PDw8PDw8PDw8PDw8PDw8PDw8PDw8PDw8PDAwPEhQUFQ0NDhESFRUUDg4PEhQVFRUOEBETFBUVFRARFBUVFRUVEhMUFRUVFRUUFRUVFRUVFRUVFRUVFRUVEAwLEBQZGxwNDQ4SFRwcGw4NEBQZHBwcDhATFhsdHRwRExkcHB4eHRQYGxwdHh4dGxwdHR4eHh4dHR0dHh4eHRALChAYKDM9DAwOExo6PDcODRAYKDlFOA4RFh0zV1A+EhYlOkRtZ00YIzdAUWhxXDFATldneXhlSFxfYnBkZ2MTExMTExMTExMTExMTExMTExMTExMTExMTExMTExMTExMTExMTExMTExMTExMTExMTExMTExMTExMTExMTExMTEhIVGRoaGhoSFBYaGhoaGhUWGRoaGhoaGRoaGhoaGhoaGhoaGhoaGhoaGhoaGhoaGhoaGhoaGhoaGhoaGhoaGhESFh8kJCQkEhQYIiQkJCQWGCEkJCQkJB8iJCQkJCQkJCQkJCQkJCQkJCQkJCQkJCQkJCQkJCQkJCQkJCQkJCQREhgvY2NjYxIVGkJjY2NjGBo4Y2NjY2MvQmNjY2NjY2NjY2NjY2NjY2NjY2NjY2NjY2NjY2NjY2NjY2NjY2NjFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRISEhUXGBkbEhIVFxgZGxwSFRcYGRscHRUXGBkbHB0dFxgZGxwdHR0YGRscHR0dHhkbHB0dHR4eGxwdHR0eHh4REREUFxocIBERFBcaHCAiERQXGhwgIiUUFxocICIlJRcaHCAiJSUlGhwgIiUlJSkcICIlJSUpKiAiJSUlKSoqEBAQFBgcICgQEBQYHCAoMBAUGBwgKDBAFBgcICgwQEAYHCAoMEBAQBwgKDBAQEBgICgwQEBAYIAoMEBAQGCAgAfF5cdH1e3Ow/L66wGmYnfIUbwdUTe3LMRbqON8B+5RJEvcGxkvrVUjTMrsXYhAnIwe0dTJfOYbWrDYyqUrz7dw/JO4hpmV2LsQQvkUeGq1BsZLx+cu5iV0e0eScJ91VIQYrmqfdVSK7GgjOU0oPaPOu5IcDK1mNvnD+K8LwS87f8Jx2mHtHnUkTGAurWZlNQa74ZLSFH9oF6FPGxzLsjQO5Qe0edcpttd7BXBSqMCL4k/4tFrHIPuEQ7m1/uIWkbDMWVoDdOSuRQ9286kvVUlQjzOE6VrNguN4oRXYGkgcnih7t13/9kxvLYKQezwLTrO44sVmMPgMqORo1E0sm1/9SludkcWHwfJwTSybR4LeAz6ugWVgRaY8mV/9SluQmtHrzsBtRF/wPY+X0JuYTs+ltgrXAmlk10xQHmTu9VSIAk1+vcvU4ml2oNzrNhEtQ3CysNP8UeR35wqpKUBdGdZMSjX4WVi8nJpdpHnbhzEIdx7mwf6W1FKAiucMXrWUWVjyRf23chNtR9mIzDoT/6ZLYailAjhFlZuvPtSeZ+2oREubDoWmT3TguY+JHPdRVSLKxfKH3vgNqJ/9emeEYikGXDFNzaLjvTeGAL61mogOoeG3y6oU4rW55ydoj0lUTSR/mmRhPmF86uwIfzp3FtiufQCmppaHDlGE0r2iTzXIw3zBq5hvaTldjG4CPb9wdxAme0SyedVKczJ9AtYbgPOzYKJvZZImsN7ecrxWZg5dR6ZLj/j4qpWsIA+vYwE+Tca9ounMIsrXMB4Stiib2SPQtZv+FVIpfEbzv8ncZoLBXc3YBqTG1HsskTTotZOYTG+oVUjLk6zhP8bg4RhMUNtfZdO7FdpBuXzhJ5Fh8IKlJG7wtD9ik8rWOJxy6iQ3NwzBpQ219mlyv+FLicYs2iJGSE0u2txzed++D61ZWCiHD/cZdQVCqkO2gJpdpNaObhnDfAPrT89RxdWFZ5hO3MseBSIlANppdZNIV/Rwe5eLTDvkfWKzFnH+QJ7m9QWV1KdwnuIwTNtZdJMoXBf74OhRnh2t+OTGL+AVUnIkyYY+QG7g9itHXyF3OIygG2s2kud679ZWKqSFa9n3IHD6MeLv1lZ0XyduRhiDRtrNnKoyiFVLcBm0ba5Yy3fQkDh4XsFE34isVpOzpa9nR8iCpS4HoxG2rJpnRhf3YboVa1PcRouh5LIJv/uQcPNd095ickTaiGBnWLKVWRc0OnYTSyex/n2FofEPnDG8y3PztHrzOLK1xo6RAml2k9owKajOC0Wr4D5x+3nA0UEhK2m198wuBHF3zlWWVKWLN1CHzLClUfuoYBcx4b1llpeBKmbayaR58njtE9onD66lUcsg0Spm2snsb+8HaJRn4dYcLbCuBuYwziB8/5U1C1DOOz2gZjSZtrLJk6vrLF3hwY4Io9xuT/ruUFRSBkNtUzTOWhjh26irLEPx4jPZL3Fo3QrReoGTTM21xYTT9oFdhTUIvjqTkfkvt0bzgVUjq/hOYY8j60IaO/0AzRBtqkTS6R5ellZd5uKdzzhb8BFlDdAcrwkE0rbXTOPB+7Y0FlZO96qFL4Ykg21StJs8qIW7h16H5hGiv8V2Cflau7QVDepTAHa6Lgt6feiEvJDM21StJsmOH/hynURrKxvUpQ8BH0JF7BiyG2qZpnL/7AOU66gt+reLEXY8pVOCQvSsBtqZTNM8bk9ohRcwD18o/WVkbvrceVKRb9I59IEKysjBeTMmmbA21xu/6iHadLRxuIzkLpi8wZYmmbbWi32RVAUjruxWlJ//iFxE38FI9hNKOoCdhwf5fDe4xZ81lgREhK2m1j78vW1CqkuMu/AjBNK210kzRUX/B+69cMMUG5bYrIeZxVSEZISmkzbXOi9yxwIfPgdsov7R71xuJ7rFcACjG/9PzApqFq7wEgzNJm2suWESPuwrQvejj7cbnQxMkxpm21lUYJL0fKmogPPqywn7e3FvB/FCNxPJ85iVUkCE9/tLKx31G4CgNtWTTPFhMvlu8G4/TrgaZttTChljfNJGgOT2X6EqpETy2tYd9cCBI4lIXJ1/3uVUllZEJz4baqGF64yxaZ+zPLYwde8Uqn1oKANtUrSaTOPHkhvuQP3bBlEJ/LFe4pqQOHUI8T8q7AXx3fLVBgSCVpMba55YxN3rv8U1Dv51bAPSOLlZWebkL8vSMGI21lJmmeVxPRwFlZF1CpqCN8uLwymaZyjbXHCRytogPN3o/n74CNykfT+qqRv5AQlHcRxYrC5KvGmbbUwmZY/29BvF6C1/93x4WVglXDLFpmbapmF89HKTogRwqqSlGbu+oiAkcWFbklC6Zhf+NtTLFpn8oWz+HsNRVSgIxZWON+yVyJlE5tq/+GWLTMutYX9ekTySEQPLVNQQ3OfycwJBM0zNtZcse7CvcKI0V/zh16Dr9OSA21MpmmcrHC+6pTAPHPwoit3LHHqs7jhFNRD6W8+EBGoSEoaZttTCZljfduH/fFisn+dRBGAZYtMzbVMwvul/T/crK1NQh8gN0SRRa9cOux6clC0/mDLFpmbarmF8/e6CopeOLCNW6S/IUUg3jJIYiAcDoMcGeRbOvuTPjXR/tyo79LK3kqqkbxkkMRAOB0GODPItnX3Jnxro/25Ud+llbyVVSN4ySGIgHA6DHBnkWzr7kz410f7cqO/Syt5KqpFVJwn6gBEvBM0zNtZcpGOEPiysW8vvRd2R0f7gtjhqUvXL+gWVwHm4XJDBiMpmmZtrLfPwd/IugP5+fKVSysH1EXreFAcEhelGmbbUmZY4Xdo1vQWVnK19P4RuEnbf0gQnR+lDCZlivNM22t1ESmopPIgfT0duOfQrsjgG4tPxli0zJmF5trdL1JDUIUT1ZXSqQDeR4B8mX3TrRro/2McGeUvLtwo6jIEKMkCUXWsLyZROd9P/rFYNtXPBli0z398iVUlVKAjFlY437JXImUTm2r/4ZYtMy61hf16RPJIU9nZ1MABAwAAAAAAAAAZpwgEwIAAABhp658BScAAAAAAADnUFBQXIDGXLhwtttNHDhw5OcpQRMETBEwRPduylKVB0HRdF0A";else{if(!Modernizr.video.h264)return void l("videoautoplay",!1);r.src="data:video/mp4;base64,AAAAIGZ0eXBpc29tAAACAGlzb21pc28yYXZjMW1wNDEAAAAIZnJlZQAAAs1tZGF0AAACrgYF//+q3EXpvebZSLeWLNgg2SPu73gyNjQgLSBjb3JlIDE0OCByMjYwMSBhMGNkN2QzIC0gSC4yNjQvTVBFRy00IEFWQyBjb2RlYyAtIENvcHlsZWZ0IDIwMDMtMjAxNSAtIGh0dHA6Ly93d3cudmlkZW9sYW4ub3JnL3gyNjQuaHRtbCAtIG9wdGlvbnM6IGNhYmFjPTEgcmVmPTMgZGVibG9jaz0xOjA6MCBhbmFseXNlPTB4MzoweDExMyBtZT1oZXggc3VibWU9NyBwc3k9MSBwc3lfcmQ9MS4wMDowLjAwIG1peGVkX3JlZj0xIG1lX3JhbmdlPTE2IGNocm9tYV9tZT0xIHRyZWxsaXM9MSA4eDhkY3Q9MSBjcW09MCBkZWFkem9uZT0yMSwxMSBmYXN0X3Bza2lwPTEgY2hyb21hX3FwX29mZnNldD0tMiB0aHJlYWRzPTEgbG9va2FoZWFkX3RocmVhZHM9MSBzbGljZWRfdGhyZWFkcz0wIG5yPTAgZGVjaW1hdGU9MSBpbnRlcmxhY2VkPTAgYmx1cmF5X2NvbXBhdD0wIGNvbnN0cmFpbmVkX2ludHJhPTAgYmZyYW1lcz0zIGJfcHlyYW1pZD0yIGJfYWRhcHQ9MSBiX2JpYXM9MCBkaXJlY3Q9MSB3ZWlnaHRiPTEgb3Blbl9nb3A9MCB3ZWlnaHRwPTIga2V5aW50PTI1MCBrZXlpbnRfbWluPTEwIHNjZW5lY3V0PTQwIGludHJhX3JlZnJlc2g9MCByY19sb29rYWhlYWQ9NDAgcmM9Y3JmIG1idHJlZT0xIGNyZj0yMy4wIHFjb21wPTAuNjAgcXBtaW49MCBxcG1heD02OSBxcHN0ZXA9NCBpcF9yYXRpbz0xLjQwIGFxPTE6MS4wMACAAAAAD2WIhAA3//728P4FNjuZQQAAAu5tb292AAAAbG12aGQAAAAAAAAAAAAAAAAAAAPoAAAAZAABAAABAAAAAAAAAAAAAAAAAQAAAAAAAAAAAAAAAAAAAAEAAAAAAAAAAAAAAAAAAEAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAACAAACGHRyYWsAAABcdGtoZAAAAAMAAAAAAAAAAAAAAAEAAAAAAAAAZAAAAAAAAAAAAAAAAAAAAAAAAQAAAAAAAAAAAAAAAAAAAAEAAAAAAAAAAAAAAAAAAEAAAAAAAgAAAAIAAAAAACRlZHRzAAAAHGVsc3QAAAAAAAAAAQAAAGQAAAAAAAEAAAAAAZBtZGlhAAAAIG1kaGQAAAAAAAAAAAAAAAAAACgAAAAEAFXEAAAAAAAtaGRscgAAAAAAAAAAdmlkZQAAAAAAAAAAAAAAAFZpZGVvSGFuZGxlcgAAAAE7bWluZgAAABR2bWhkAAAAAQAAAAAAAAAAAAAAJGRpbmYAAAAcZHJlZgAAAAAAAAABAAAADHVybCAAAAABAAAA+3N0YmwAAACXc3RzZAAAAAAAAAABAAAAh2F2YzEAAAAAAAAAAQAAAAAAAAAAAAAAAAAAAAAAAgACAEgAAABIAAAAAAAAAAEAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAY//8AAAAxYXZjQwFkAAr/4QAYZ2QACqzZX4iIhAAAAwAEAAADAFA8SJZYAQAGaOvjyyLAAAAAGHN0dHMAAAAAAAAAAQAAAAEAAAQAAAAAHHN0c2MAAAAAAAAAAQAAAAEAAAABAAAAAQAAABRzdHN6AAAAAAAAAsUAAAABAAAAFHN0Y28AAAAAAAAAAQAAADAAAABidWR0YQAAAFptZXRhAAAAAAAAACFoZGxyAAAAAAAAAABtZGlyYXBwbAAAAAAAAAAAAAAAAC1pbHN0AAAAJal0b28AAAAdZGF0YQAAAAEAAAAATGF2ZjU2LjQwLjEwMQ=="}}catch(s){return void l("videoautoplay",!1)}r.setAttribute("autoplay",""),r.style.cssText="display:none",T.appendChild(r),setTimeout(function(){r.addEventListener("playing",A,!1),e=setTimeout(A,t)},0)});var Q=v.testStyles=s;Modernizr.addTest("touchevents",function(){var t;if("ontouchstart"in A||A.DocumentTouch&&e instanceof DocumentTouch)t=!0;else{var n=["@media (",G.join("touch-enabled),("),"heartz",")","{#modernizr{top:9px;position:absolute}}"].join("");Q(n,function(A){t=9===A.offsetTop})}return t});var Z="Moz O ms Webkit",b=v._config.usePrefixes?Z.split(" "):[];v._cssomPrefixes=b;var Y=v._config.usePrefixes?Z.toLowerCase().split(" "):[];v._domPrefixes=Y;var S={elem:i("modernizr")};Modernizr._q.push(function(){delete S.elem});var M={style:S.elem.style};Modernizr._q.unshift(function(){delete M.style}),v.testAllProps=w,v.testAllProps=y,Modernizr.addTest("cssfilters",function(){if(Modernizr.supports)return y("filter","blur(2px)");var A=i("a");return A.style.cssText=G.join("filter:blur(2px); "),!!A.style.length&&(e.documentMode===t||e.documentMode>9)}),o(),r(R),delete v.addTest,delete v.addAsyncTest;for(var V=0;V<Modernizr._q.length;V++)Modernizr._q[V]();A.Modernizr=Modernizr}(window,document);

/*! svg4everybody v2.1.0 | github.com/jonathantneal/svg4everybody */
(function(ua){
	if(ua.indexOf("trident/7.0") > -1 || ua.indexOf('msie') != -1){
!function(a,b){"function"==typeof define&&define.amd?define([],function(){return a.svg4everybody=b()}):"object"==typeof exports?module.exports=b():a.svg4everybody=b()}(this,function(){
;function c(d,f){if(f){var e=document.createDocumentFragment(),g=!d.getAttribute("viewBox")&&f.getAttribute("viewBox");g&&d.setAttribute("viewBox",g);for(var h=f.cloneNode(!0);h.childNodes.length;){e.appendChild(h.firstChild)}d.appendChild(e)}}function b(d){d.onreadystatechange=function(){if(4===d.readyState){var e=d._cachedDocument;e||(e=d._cachedDocument=document.implementation.createHTMLDocument(""),e.body.innerHTML=d.responseText,d._cachedTarget={}),d._embeds.splice(0).map(function(f){var g=d._cachedTarget[f.id];g||(g=d._cachedTarget[f.id]=e.getElementById(f.id)),c(f.svg,g)})}},d.onreadystatechange()}function a(g){function f(){for(var q=0;q<h.length;){var n=h[q],o=n.parentNode;if(o&&/svg/i.test(o.nodeName)){var t=n.getAttribute("xlink:href");if(m&&(!e.validate||e.validate(t,o,n))){o.removeChild(n);var r=t.split("#"),p=r.shift(),u=r.join("#");if(p.length){var s=d[p];s||(s=d[p]=new XMLHttpRequest(),s.open("GET",p),s.send(),s._embeds=[]),s._embeds.push({svg:o,id:u}),b(s)}else{c(o,document.getElementById(u))}}}else{++q}}l(f,67)}var m,e=Object(g),i=/\bTrident\/[567]\b|\bMSIE (?:9|10)\.0\b/,k=/\bAppleWebKit\/(\d+)\b/,j=/\bEdge\/12\.(\d+)\b/;m="polyfill" in e?e.polyfill:i.test(navigator.userAgent)||(navigator.userAgent.match(j)||[])[1]<10547||(navigator.userAgent.match(k)||[])[1]<537;var d={},l=window.requestAnimationFrame||setTimeout,h=document.getElementsByTagName("use");m&&f()}return a});svg4everybody();
	}
})(window.navigator.userAgent.toLowerCase());

var Common = {
	//PCとSP両方のdata-autoheightやautowidthを常時揃える変数
	setAlignElemLoop:null,

	//PCのdata-autoheightやautowidthを常時揃える変数
	setAlignElemLoopPC:null,

	//SPのdata-autoheightやautowidthを常時揃える変数
	setAlignElemLoopSP:null,

	//data-autowidthやautoheightの要素を揃える
	setAlignElem:function(){
		//PCとSP両方の高さや幅を揃える
		//一度高さや幅をリセットする
		$('[data-autowidth]').each(function(index, element) {
			element.style.width = '';
		});
		$('[data-autoheight]').each(function(index, element) {
			element.style.height = '';
		});

		//常時揃える処理を
		clearInterval(Common.setAlignElemLoop);
		Common.setAlignElemLoop = setInterval(function(){
			Common.sortElementStyle('data-autowidth', 'width');
			Common.sortElementStyle('data-autoheight', 'height');
			clearInterval(Common.setAlignElemLoop);
		}, 600);

		var w = window.innerWidth || document.documentElement.clientWidth || document.body.clientWidth;
		if(640 < w){//pc 420
			//高さ・幅を揃える
			$('[data-sp-autowidth]').each(function(index, element) {
				element.style.width = '';
			});
			$('[data-sp-autoheight]').each(function(index, element) {
				element.style.height = '';
			});
			clearInterval(Common.setAlignElemLoopPC);
			Common.setAlignElemLoopPC = setInterval(function(){
				Common.sortElementStyle('data-pc-autowidth', 'width');
				Common.sortElementStyle('data-pc-autoheight', 'height');
				clearInterval(Common.setAlignElemLoopPC);
			}, 600);
		}else{//smart
			//高さ・幅を揃える
			$('[data-pc-autowidth]').each(function(index, element) {
				element.style.width = '';
			});
			$('[data-pc-autoheight]').each(function(index, element) {
				element.style.height = '';
			});
			clearInterval(Common.setAlignElemLoopSP);
			Common.setAlignElemLoopSP = setInterval(function(){
				Common.sortElementStyle('data-sp-autowidth', 'width');
				Common.sortElementStyle('data-sp-autoheight', 'height');
				clearInterval(Common.setAlignElemLoopSP);
			}, 600);
		}
	},

	//高さを揃える
	sortElementStyle:function(targetVal, type, compareVals, cmp){
		compareVals = {};
		$('['+targetVal+']').each(function(index, element) {
			if(!compareVals[element.getAttribute(targetVal)]){
				compareVals[element.getAttribute(targetVal)] = [];
			}
			compareVals[element.getAttribute(targetVal)].push(element);
		});


		for(var key in compareVals){
			cmp = [];
			if(type == 'width'){
				TweenMax.set(compareVals[key], {width:'auto', onComplete:function(){
					for(var i = 0; i < compareVals[key].length; i++){
						cmp.push($(compareVals[key][i]).outerWidth());
					}
					$(compareVals[key]).outerWidth(Math.max.apply(null, cmp));
				}});
			}else if(type == 'height'){
				TweenMax.set(compareVals[key], {height:'auto', onComplete:function(){
					for(var i = 0; i < compareVals[key].length; i++){
						cmp.push($(compareVals[key][i]).outerHeight());
					}
					$(compareVals[key]).outerHeight(Math.max.apply(null, cmp));
				}});
			}
		}
		return false;
	},

	//ページのスクロールを有効／無効する変数
	autoScrollComplete:true,

	topScrollFunc:function(target, deleteHash){
		if(Common.autoScrollComplete) {
			if(target){
				if(target[0]){
					var val = target.offset().top;
					var w = $(window).width();
					/*switch(target.attr('id')){
						case 'Male':
							
							if(420 < w) val += 142;
							else val += 110;
							break;
						case 'Female':
							if(420 < w) val += 30;
							else val += 0;
							break;
					}*/

					if(640 < w) val -= 100;//220;

					if(deleteHash) location.hash = '';
					Common.autoScrollComplete = false;
					$('html,body').stop().animate({scrollTop:val},700, 'swing', function(){
						Common.autoScrollComplete = true;
					});
					return false;
				}
			}
		}
	},

	//任意のタイミングでスクロール
	scrollByLoaded:function(target){
		TweenMax.set('body', {delay:0.4, onComplete:function(){
			Common.topScrollFunc($('#'+location.hash.split('scroll-')[1]), true);
		}});
	},

	//読み込み
	loadfileChech:function(ele, result){
		var w = parseInt(window.innerWidth || document.documentElement.clientWidth || document.body.clientWidth, 10 );
        var h = parseInt( window.innerHeight || document.documentElement.clientHeight || document.body.clientHeight, 10 );

		if(ele.getAttribute('data-lf')){
			string = 'data-lf';
			if(ele.getAttribute('data-lf-portrait') || ele.getAttribute('data-lf-landscape')) {
				if(h > w) {//portrait
					if(ele.getAttribute('data-lf-portrait')) string = 'data-lf-portrait';
				}else{//landscape
					if(ele.getAttribute('data-lf-landscape')) string = 'data-lf-landscape';
				}
			}else{
				if(640 < w){//pc 420
					if(ele.getAttribute('data-lf-pc')) string = 'data-lf-pc';
				}else{//smart
					if(ele.getAttribute('data-lf-sp')) string = 'data-lf-sp';
				}
			}

			//すでに読み込まれているとき
			result = true;
			switch(ele.tagName){
				case 'IMG':
					if(ele.getAttribute('src') == ele.getAttribute(string)) result = false;
					break;
				default:
					//インラインスタイルにファイル名が含まれる場合
					if(ele.getAttribute('style')){
						if(ele.getAttribute('style').split(ele.getAttribute(string)).length > 1) result = false;
					}
					break;
			}

			//読み込まれていないとき
			if(result) Common.loadfile(ele, string);
		}else{
			if(ele.getAttribute('data-lf-portrait') || ele.getAttribute('data-lf-landscape')) {
				if(h > w) {//portrait
					Common.loadfile(ele, 'data-lf-portrait');
				}else{//landscape
					Common.loadfile(ele, 'data-lf-landscape');
				}
			}else{
				if(640 < w){//pc 420
					Common.loadfile(ele, 'data-lf-pc');
				}else{//smart
					Common.loadfile(ele, 'data-lf-sp');
				}
			}
		}
	},

	//読み込み
	loadfile:function(element, attr){
		if(!element.getAttribute(attr)){
			switch(element.tagName){
				case 'IMG':
					element.setAttribute('src', '');
					break;
				default:
					var withstyle = '';
					if(element.getAttribute('style')) withstyle = element.getAttribute('style');
					withstyle = withstyle.replace(/(background-image).*\);/g, '');
					element.setAttribute('style', withstyle);
					break;
			}
			return false;
		}

		var image = new Image();
		var ImageLoadFunc = function(){
			//表示
			switch(element.tagName){
				case 'IMG':
					element.setAttribute('src', image.src);
					break;
				default:
					var withstyle = '';
					if(element.getAttribute('style')) withstyle = element.getAttribute('style');
					withstyle = withstyle.replace(/(background-image).*\);/g, '');
					element.setAttribute('style', withstyle+'background-image:url('+image.src+');');
					break;
			}

			//イベント解除
			if(image.addEventListener){// イベントリスナーに対応している
				image.removeEventListener("load",ImageLoadFunc);
			}else if(image.attachEvent){// アタッチイベントに対応している
				image.detachEvent("onload",ImageLoadFunc);
			}else{// イベントハンドラを使用する
				//image.onload = ImageLoadFunc;
			}
		}

		//イベント登録
		if(image.addEventListener){// イベントリスナーに対応している
			image.addEventListener("load",ImageLoadFunc);
		}else if(image.attachEvent){// アタッチイベントに対応している
			image.attachEvent("onload",ImageLoadFunc);
		}else{// イベントハンドラを使用する
			image.onload = ImageLoadFunc;
		}

		//読み込み開始
		if(element.getAttribute(attr)) {
			image.src = element.getAttribute(attr);
		}
	},

	//画面表示のタイミングでスクロールを無効
	disableScrollInView:true,

	//スクロールの折返位置からの移動量
	sVecVal:0,

	//ページが表示された時／隠された時
	pageHideFlg:false,

	//URLパラメータを取得し配列に格納
	urlParams:(function(){
		var obj = {};
		var pair = location.search.substring(1).split('&');
		for(var i = 0; pair[i]; i++) {
			var kv = pair[i].split('=');
			obj[kv[0]]=kv[1];
		}
		return obj;
	})()
};

window.onload = function() {
	//画面表示時にスクロール
	if(Common.disableScrollInView) Common.scrollByLoaded();
};


//デバイスごとにclassを設定
(function(ua,ver,pla){
	if (pla.indexOf('win') != -1) {
		$('html').addClass('os-windows');
	}else{
		$('html').addClass('os-mac');
	}
	if (ua.indexOf('safari') !== -1 && ua.indexOf('chrome') === -1){
		$('html').addClass('safari');
	}else{
		$('html').addClass('not-safari');
	}
	if ((ua.indexOf('iphone') > 0 && ua.indexOf('ipad') == -1) || ua.indexOf('ipod') > 0) {
		$('html').addClass('os-iOS');
	}
	if (ua.indexOf('android') > 0) {
		$('html').addClass('os-android');
	}
	if (ua.indexOf('iphone') == -1 || ua.indexOf('ipad') == -1) {
		$('html').addClass('not-apple-device');
	}
	if(ua.indexOf('ipad') > 0) {
		$('html').addClass('ipad');
	}
	var isMSIE = (ua.indexOf('msie') > -1) && (ua.indexOf('opera') == -1);
	var isIE9 = isMSIE && (ver.indexOf('msie 9.') > -1);
	var isIE10 = isMSIE && (ver.indexOf('msie 10.') > -1);
	var isIE11 = (ua.indexOf('trident/7') > -1);
	var isEdge = (ua.indexOf('edge') > -1);
	if(ua.indexOf("trident/7.0") > -1 || ua.indexOf('msie') != -1 || ua.indexOf('edge') != -1){
		$('html').addClass('ie');
	}else{
		$('html').addClass('not-ie');
	}
	if(isIE9) $('html').addClass('ie9');
	if(isIE10) $('html').addClass('ie10');
	if(isIE11) $('html').addClass('ie11');
	if(isEdge) $('html').addClass('edge');
})(navigator.userAgent.toLowerCase(), navigator.appVersion.toLowerCase(), navigator.platform.toLowerCase());

//consoleの調整
if(!window.console){
	window.console = {log:function(msg){}};
}

$(function() {
	"use strict";
	
	var w,
		h,
		s;
	
	//タブのアクティブ
	$(window).on('focus', function(e){
		Common.pageHideFlg = false;
	});
	
	//タブの非アクティブ
	$(window).on('blur', function(e){
		Common.pageHideFlg = true;
	});
	
	// スムーズスクロール
	$(document).on('click','a[href*="#"]',function(){
		var str = this.getAttribute('href').split('#')[1];
		var str2 = this.getAttribute('href').split('#scroll-')[1];
		var target;
		if($('#'+str)[0]){
			target = $('#'+str);
			Common.topScrollFunc(target);
			return false;
		}else if($('#'+str2)[0]){
			target = $('#'+str2);
			Common.topScrollFunc(target);
			return false;
		}else{
		}
	});
	
	//電話
	$('[data-tel]').each(function(index, element) {
		if(Modernizr.touchevents){//タッチデバイスのとき
			element.setAttribute('href', 'tel:' + element.getAttribute('data-tel').replace((new RegExp('-', "g")) ,''));
		}
	});
	
	//画面サイズ変更イベント
	var switchResizeInit = true,
		switchResizeName = '',
		oldWidth;
	
	$(window).on('resize', function(){
		if($('html,body').scrollTop() !=0){
			s = $('html,body').scrollTop();
		}else{
			s = $(document).scrollTop();
		}
		w = window.innerWidth || document.documentElement.clientWidth || document.body.clientWidth;
		h = $(window).height();
		var ua = navigator.userAgent;

//行数制限：基本
/*
・pc,sp共通
<p data-lines="行数"><span>囲い方にルールがあるみたいですが完全に把握できていません。</span></p>
・pc,spで分ける
<p data-lines data-lines-pc="PCの行数" data-lines-sp="SPの行数"><span>囲い方にルールがあるみたいですが完全に把握できていません。</span></p>
*/
		$('[data-lines]').each(function(){
			if(!$('[data-lines-sp]').length && !$('[data-lines-pc]').length) {
				var d_l = $(this).get(0).getAttribute('data-lines');
				$(this).trunk8({
					lines: d_l
				});
			}
		});
		if(640 < w){//pc 420
			//PCかスマホか切り替わった判定
			if(switchResizeName != 'pc'){
				switchResizeName = 'pc';
				switchResizeInit = true;
			}
//行数制限：pc
			$('[data-lines]').each(function(){
				if($('[data-lines-pc]').length) {
					var d_l = $(this).get(0).getAttribute('data-lines-pc');
					$(this).trunk8({
						lines: d_l
					});
				}
			});


		}else{//smart
			//PCかスマホか切り替わった判定
			if(switchResizeName != 'sp'){
				switchResizeName = 'sp';
				switchResizeInit = true;
			}
//行数制限：sp
			$('[data-lines]').each(function(){
				if($('[data-lines-sp]').length) {
					var d_l = $(this).get(0).getAttribute('data-lines-sp');
					$(this).trunk8({
						lines: d_l
					});
				}
			});
		}

		//PCからスマホ、スマホからPCへ繊維したとき一度だけ実行
		if(switchResizeInit){
			switchResizeInit = false;
			$('[data-lf-area]').attr('data-lf-area', '0');

			//data-lf-area 要素を読み込む
			$('[data-lf-area]').off('inview.loadfile');
			$('[data-lf-area]').each(function(index, element) {
				$(this).on('inview.loadfile', function(event, isInView, visiblePartX, visiblePartY){
					if(isInView){
						if(element.getAttribute('data-lf-area') == '1') return true;
						$(element).attr('data-lf-area', '1');
						$(element).find('[data-lf]').each(function(index1, element1) {
							Common.loadfileChech(element1);
						});
						Common.loadfileChech(element);
					}
				});
			});
		}


		//幅のみリサイズされたとき
		if(oldWidth != w){
			oldWidth = w;

			//揃える
			Common.setAlignElem();
		}
		$(window).trigger('scroll');
	});
	$(window).trigger('resize');

	//スマホ、タブレットの縦横検知
	(function() {
		if(Modernizr.touchevents) {
			var defaultOrientation; // window.orientationが0または180の時に縦長であればtrue
			var w = window.innerWidth || document.documentElement.clientWidth || document.body.clientWidth;
			var firstFlg = false;

			if('orientation' in window) {
				var o1 = (window.innerWidth < window.innerHeight);
				var o2 = (window.orientation % 180 == 0);
				defaultOrientation = (o1 && o2) || !(o1 || o2);
			}

			var orientationEvent = 'orientationchange';
			$(window).on(orientationEvent, function(){
				if('orientation' in window) {
					// defaultOrientationがtrueの場合、window.orientationが0か180の時は縦長
					// defaultOrientationがfalseの場合、window.orientationが-90か90の時は縦長
					var o = (window.orientation % 180 == 0);
					if((o && defaultOrientation) || !(o || defaultOrientation)) {
						// ここに縦長画面への切り替え処理を記述
						//console.log('portrait');
					}else {
						// ここに横長画面への切り替え処理を記述
						//console.log('landscape');
					}
					//画面サイズが420以下のとき
						if(firstFlg) location.reload();
						firstFlg = true;
				}
			}).trigger(orientationEvent);
		}
	})();

	$('.touchevents a.touch_none').removeAttr('href');

	//画面スクロール処理
	$(window).on('scroll', function() {
		if($('html,body').scrollTop() !=0){
			s = $('html,body').scrollTop();
		}else{
			s = $(document).scrollTop();
		}

//グロナビ関連
		if(s < 150) {//120
			$('.mod_header').removeClass('fix').addClass('no_fix');
			$('.mod_spNav_btn').removeClass('fix');
		}else {
			$('.mod_header').addClass('fix').removeClass('no_fix');
			$('.mod_spNav_btn').addClass('fix');
		}
		if(s < 200) {
			$('.mod_toTop').removeClass('view');
			$('.mod_footer .inq_links').removeClass('view');
		}else {
			$('.mod_toTop').addClass('view');
			$('.mod_footer .inq_links').addClass('view');
		}
	});
	$(window).trigger('resize');
//スマホのメニュー
	$('.mod_spNav_btn').click(function(){
		if($(this).hasClass('open')){
			$('.mod_header').removeClass('open');
			$(this).removeClass('open');
			$('body').removeClass('nav_open');
		}else {
			$('.mod_header').addClass('open');
			$(this).addClass('open');
			$('body').addClass('nav_open');
		}
	});
});