$(function(){
/* data-slidearea02 */
	(function(){
					var slideDelay = 0.3;
					$('[data-slidearea02]').each(function(index, element) {
									var slidearea = $(element).find('.imgs'),
													slides = slidearea.find('.img'),
													length = slides.length-1,
													pager = $(element).find('.pager'),
													pagers,
													prev = $(element).find('.prev'),
													next = $(element).find('.next'),
													now = -1,
													loop,
													loopDur = 6000,
													flickFlg = false,
													flickArea = $(element),
													flickPosXBefore = 0,
													flickPosXNow = 0,
													flickPosXFirst = 0,
													flickPosYBefore = 0,
													flickPosYNow = 0,
													flickPosYFirst = 0,
													flickArrow,
													flickLoop,
													flickAnimTimeFlg = false;

									//番号を設定
									slides.each(function(i, e) {
													$(e).attr('data-slide-num', i).css({left:100*i+'%'});
													pager.append('<button data-slide-num="' + i + '"></button>');
									});
									pagers = pager.children();

									(function(){
													//端の要素を生成
													var closeElesPrev = [],
																	closeElesNext = [],
																	closeEle,
																	closeEleIndex = 1,
																	closeEleNow;

													//ダミー要素 前 作成
													closeEleNow = length+1;
													for(var i = 0; i < closeEleIndex; i++){
																	closeEleNow--;
																	if(closeEleNow < 0) closeEleNow = length-1;
																	var c = slides.eq(closeEleNow).clone();

																	$(c).css({left:(-100*(i+1))+'%'});
																	closeElesPrev.push($(c));
													}
													//ダミー要素 後 作成
													closeEleNow = -1;
													for(var i = 0; i < closeEleIndex; i++){
																	closeEleNow++;
																	if(length < closeEleNow) closeEleNow = 0;
																	var c = slides.eq(closeEleNow).clone();
																	$(c).css({left:100*(length+(i+1))+'%'});
																	closeElesNext.push($(c));
													}
													//ダミー要素をいれる
													for(var i = 0; i < closeElesPrev.length; i++){
																	closeElesPrev[i].addClass('dammy');
																	slidearea.prepend(closeElesPrev[i]);
													}
													for(var i = 0; i < closeElesNext.length; i++){
																	closeElesNext[i].addClass('dammy');
																	slidearea.append(closeElesNext[i]);
													}
									})();

									//pager
									pagers.on(((window.navigator.msPointerEnabled)?'pointerdown':('ontouchend' in document)?'touchstart':'click'), function(e){
													var val = $(this).attr('data-slide-num');
													changeFunc(val);
									});

									//prev処理
									function prevFunc(){
													var val = -1+now;
													if(val < 0) val = length;
													changeFunc(val, 'prev');
									}
									prev.on(((window.navigator.msPointerEnabled)?'pointerdown':('ontouchend' in document)?'touchstart':'click'), prevFunc);

									//next
									function nextFunc(){
													var val = +1+now;
													if(length < val) val = 0;
													changeFunc(val, 'next');
									}
									next.on(((window.navigator.msPointerEnabled)?'pointerdown':('ontouchend' in document)?'touchstart':'click'), nextFunc);

									//フリックエリアが見えたときに自動処理を有効にする
									$(element).on('inview', function(e, isInView, visiblePartX, visiblePartY) {
													if(isInView){
																	clearInterval(flickLoop);
																	flickLoop = setInterval(function(){
																					if(flickFlg) flickLoopFunc();
																	}, 33);
													}else{
																	clearInterval(flickLoop);
													}
									});

									//flick touchstart
									flickArea.on('touchstart', function(e){
													flickPosXNow = flickPosXBefore = flickPosXFirst = e.originalEvent.changedTouches[0].pageX;
													flickPosYNow = flickPosYBefore = flickPosYFirst = e.originalEvent.changedTouches[0].pageY;
													clearInterval(loop);
													flickFlg = false;
									});

									//flick touchmove
									flickArea.on('touchmove', function(e){
													flickPosXNow = e.originalEvent.changedTouches[0].pageX;
													flickPosYNow = e.originalEvent.changedTouches[0].pageY;
													flickPosSetFunc();
													flickFlg = true;
									});

									//flick touchend
									flickArea.on('touchend', function(e){
													if(flickFlg){
																	switch(flickArrow){
																					case 'next':
																									flickFlg = false;
																									flickAnimTimeFlg = true;
																									nextFunc();
																									break;
																					case 'prev':
																									flickFlg = false;
																									flickAnimTimeFlg = true;
																									prevFunc();
																									break;
																					default:
																									clearInterval(loop);
																									loop = setInterval(loopFunc, loopDur);

																									flickPosResetFunc();
																									break;
																	}
																	flickArrow = 'none';
													}else{
																	clearInterval(loop);
																	loop = setInterval(loopFunc, loopDur);
													}
													flickFlg = false;
									});

									//flick 常時処理
									function flickLoopFunc(){
													if(Math.abs(flickPosXNow-flickPosXBefore) > 10){//タッチ開始位置から一定の距離の場合
																	if(Math.abs(flickPosYNow-flickPosYBefore) < Math.abs(flickPosXNow-flickPosXBefore)){//Y軸よりX軸のほうが値が大きい場合
																					if(flickPosXNow-flickPosXBefore < 0){//マイナスのとき
																									flickArrow = 'next';
																					}else{
																									flickArrow = 'prev';
																					}
																	}
													}else{
																	flickArrow = 'none';
													}
													flickPosXBefore = flickPosXNow;
									}
									function flickPosSetFunc(per){
													per = ((flickPosXFirst-flickPosXNow)/flickArea.width())*100;
													TweenMax.set(slidearea, {x:(-100*now)-per+'%'});
									}
									function flickPosResetFunc(){
													TweenMax.to(slidearea, 0.4, {x:-100*now+'%'});
									}

									//ループ処理
									function loopFunc(){
													//ページが非表示のときは中断
													if(Common.pageHideFlg)return false;

													nextFunc();
									}

									//スライドの処理
									function changeFunc(num, arrow, target, targetP){

													//slide
													var moveNum = num;
													if(2 <= length){
																	if(length == now && num == 0) moveNum = length+1;
																	if(0 ==  now && num == length) moveNum = -1;
													}else{
																	if(num == 0 && arrow == 'next') moveNum = length+1;
																	if(num == length && arrow == 'prev') moveNum = -1;
													}

													var time = 0.4;
													if(flickAnimTimeFlg){
																	time = 0.2;
																	flickAnimTimeFlg = false;
													}
													TweenMax.to(slidearea, time, {x:-100*moveNum+'%', onComplete:function(){
																	TweenMax.set(slidearea, {x:-100*num+'%'});

																	//slide active
																	var activeTarget = slidearea.find('[data-slide-num="'+now+'"]');
																	activeTarget.addClass('active');
													}});

													target = slides.eq(num);
													targetP = slides.not(target);
													TweenMax.set(target, {alpha:1});
													TweenMax.set(targetP, {alpha:0});

													//pager
													target = pagers.eq(num);
													pagers.removeClass('active');
													target.addClass('active');

													//loop
													clearInterval(loop);
													loop = setInterval(loopFunc, loopDur);

													//
													now = num;
									}
									TweenMax.set(this, {delay:slideDelay*index, onComplete:function(){
													changeFunc(0);
									}});
					});
	})();


/* data-slidemulti */
	(function slidemulti(){
		$('[data-slidemulti]').each(function(index, element) {

//ひとつのスライドで表示されるコンテンツの個数の指定
				var multi =  $(element).get(0).getAttribute('data-slidemulti');
//必要要素の生成
				$(element).append('<div class="btns"><button class="prev"></button><div class="pager"></div><button class="next"></button></div>');
				do {
					$(element).children('.link:lt('+multi+')').wrapAll('<div class="c"></div>')
				}while($(element).children('.link').length);

				$(element).children('.c').wrapAll('<div class="c_slide"></div>');
				$(element).children('.c_slide').wrapAll('<div class="c_wrap"></div>');

			var slidearea = $(element).find('.c_slide'),
				slides = slidearea.find('.c'),
				slidesWithDammy,
				length = slides.length-1,
				pager = $(element).find('.pager'),
				pagers,
				now = -1,
				prev = $(element).find('.prev'),
				next = $(element).find('.next'),
				clickEnabled = true,
				loop,
				loopDur = 6000,
				flickFlg = false,
				flickArea = $(element).find('.c_wrap'),
				flickPosXBefore = 0,
				flickPosXNow = 0,
				flickPosXFirst = 0,
				flickPosYBefore = 0,
				flickPosYNow = 0,
				flickPosYFirst = 0,
				flickArrow,
				flickLoop,
				flickAnimTimeFlg = false;


//各コンテンツの幅を.c_wrapに合わせる


				// slides.css('width', flickArea.width());
			$(window).on('resize', function(){
				slides.css('width', flickArea.width());
				slidesWithDammy.css('width', flickArea.width());
			});

			(function(){
				slides.each(function(i,el){
					$(this).attr('data-slide-num', i);
				});
			})();

// pager
			slides.each(function(i,el) {
				var nume;
				if(i < 9) {
					nume = '0' + parseInt(i+1);
				}else {
					nume = parseInt(i+1);
				}
				pager.append('<p class="nume" data-slide-num="' + i + '">' + nume + '</p>')
			});

			var deno;
			if(slides.length < 10) {
				deno = '0' + slides.length;
			}else {
				deno = slides.length;
			}
			pager.append('<p class="deno">' + deno + '</p>')
			pagers = pager.find('.nume');

			if(1 < slides.length) {
//スライドが2以上のとき
				(function(){
//端の要素を生成
					var closeElesPrev = [],
						closeElesNext = [],
						closeEle,
						closeEleIndex = 1,
						closeEleNow;

//ダミー要素 前 作成
					closeEleNow = length+1;
					for(var i = 0; i < closeEleIndex; i++){
						closeEleNow--;
						if(closeEleNow < 0) closeEleNow = length-1;
						var c = slides.eq(closeEleNow).clone().attr('data-slide-num', -1-i);
						closeElesPrev.push($(c));
					}
//ダミー要素 後 作成
					closeEleNow = -1;
					for(var i = 0; i < closeEleIndex; i++){
						closeEleNow++;
						if(length < closeEleNow) closeEleNow = 0;
						var c = slides.eq(closeEleNow).clone().attr('data-slide-num', +1+length+i);
						closeElesNext.push($(c));
					}
// ダミー要素をいれる
					for(var i = 0; i < closeElesPrev.length; i++){
						closeElesPrev[i].addClass('dammy');
						slidearea.prepend(closeElesPrev[i]);
					}
					for(var i = 0; i < closeElesNext.length; i++){
						closeElesNext[i].addClass('dammy');
						slidearea.append(closeElesNext[i]);
					}

					slidesWithDammy = slidearea.children();

					slidearea.css('width', slidesWithDammy.length*100+'%');

					slidesWithDammy.each(function(i,el){
						TweenMax.set(this, {left:-((100 / slidesWithDammy.length)*i) + '%'});
					});
				})();



// prev処理
				function prevFunc(){
					if(clickEnabled) {
						var val = -1+now;
						if(val < 0) val = length;
						changeFunc(val, 'prev');

// 自動切り替え処理
						// clearInterval(loop);
						// loop = setInterval(loopFunc, loopDur);
					}
				}
				prev.on('click', prevFunc);

// next
				function nextFunc(){
					if(clickEnabled) {
						var val = +1+now;
						if(length < val) val = 0;
						changeFunc(val, 'next');

// 自動切り替え処理
						// clearInterval(loop);
						// loop = setInterval(loopFunc, loopDur);
					}
				}
				next.on('click', nextFunc);

// フリックエリアが見えたときに自動処理を有効にする
				$(element).on('inview', function(e, isInView, visiblePartX, visiblePartY) {
					if(isInView){
// 自動切り替え処理
						// clearInterval(loop);
						// loop = setInterval(loopFunc, loopDur);

// フリックの位置取得処理
						clearInterval(flickLoop);
						flickLoop = setInterval(function(){
							if(flickFlg) flickLoopFunc();
						}, 33);
					}else{
// 自動切り替え処理の削除
						clearInterval(loop);

// フリックの位置取得処理の削除
						clearInterval(flickLoop);
					}
				});

// flick touchstart
				flickArea.on('touchstart', function(e){
					flickPosXNow = flickPosXBefore = flickPosXFirst = e.originalEvent.changedTouches[0].pageX;
					flickPosYNow = flickPosYBefore = flickPosYFirst = e.originalEvent.changedTouches[0].pageY;
					clearInterval(loop);
					flickFlg = false;
				});

// flick touchmove
				flickArea.on('touchmove', function(e){
					flickPosXNow = e.originalEvent.changedTouches[0].pageX;
					flickPosYNow = e.originalEvent.changedTouches[0].pageY;
					flickPosSetFunc();
					flickFlg = true;
				});

// flick touchend
				flickArea.on('touchend', function(e){
					if(flickFlg){
						switch(flickArrow){
							case 'next':
								flickFlg = false;
								flickAnimTimeFlg = true;
								nextFunc();
								break;
							case 'prev':
								flickFlg = false;
								flickAnimTimeFlg = true;
								prevFunc();
								break;
							default:
								clearInterval(loop);
								loop = setInterval(loopFunc, loopDur);

								flickPosResetFunc();
								break;
						}
						flickArrow = 'none';
					}else{
						clearInterval(loop);
						loop = setInterval(loopFunc, loopDur);
					}
					flickFlg = false;
				});

// flick 常時処理
				function flickLoopFunc(){
					if(Math.abs(flickPosXNow-flickPosXBefore) > 10){//タッチ開始位置から一定の距離の場合
						if(Math.abs(flickPosYNow-flickPosYBefore) < Math.abs(flickPosXNow-flickPosXBefore)){//Y軸よりX軸のほうが値が大きい場合
							if(flickPosXNow-flickPosXBefore < 0){//マイナスのとき
								flickArrow = 'next';
							}else{
								flickArrow = 'prev';
							}
						}
					}else{
						flickArrow = 'none';
					}
					flickPosXBefore = flickPosXNow;
				}
				function flickPosSetFunc(per){
					per = ((flickPosXFirst-flickPosXNow)/flickArea.width())*100;
//					TweenMax.set(slidearea, {left:(-100*now)-per+'%'});
				}
				function flickPosResetFunc(){
//					TweenMax.to(slidearea, 0.4, {left:-100*now+'%'});
				}

// ループ処理
				function loopFunc(){
					nextFunc();
				}

// スライドの処理
				function changeFunc(num, arrow, target, other){
// slide active
					slidesWithDammy.removeClass('active');

					clickEnabled = false;

// slide
					var moveNum = num;
					if(2 <= length){
						if(length == now && num == 0) moveNum = length+1;
						if(0 ==  now && num == length) moveNum = -1;
					}else{
						if(num == 0 && arrow == 'next') moveNum = length+1;
						if(num == length && arrow == 'prev') moveNum = -1;
					}

					var time = .6;
					if(flickAnimTimeFlg){
						time = .4;
						flickAnimTimeFlg = false;
					}
					TweenMax.fromTo(slidesWithDammy.filter(function(i){return $(this).attr('data-slide-num') == moveNum;}), time, {x:(arrow == 'next')?50:-50, alpha:0, zIndex:3}, {x:0, alpha:1,ease: Sine.easeOut, onStart:function(){
						target = slidesWithDammy.filter(function(i){return $(this).attr('data-slide-num') == moveNum;});
						other = slidesWithDammy.not(target);
						TweenMax.set(other, {alpha:0, zIndex:1, pointerEvents:'none'});
					},onComplete:function(){
						TweenMax.set(target, {alpha:1, zIndex:2, pointerEvents:'auto'});
						$(element).addClass('view');
						clickEnabled = true;
					}});

// pager
					target = pagers.eq(num);
					pagers.removeClass('active');
					target.addClass('active');

					now = parseInt(num);
				}
				changeFunc(0);
				$(window).trigger('resize');
			}else {
//スライド数が1のとき
				$(element).addClass('view');
				pager.find('.nume').addClass('active');
				prev.remove();
				next.remove();
			}
		});
	})();
});