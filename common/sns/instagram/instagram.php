<?php

// json_encode()関数が存在しないなら
if (!function_exists('json_encode')) {
	// JSON.phpを読み込んで
	require_once(dirname( __FILE__ ).'/JSON.php');
	// json_encode()関数を定義する
	function json_encode($value) {
		$s = new Services_JSON();
		return $s->encodeUnsafe($value);
	}
	// json_decode()関数を定義する
	function json_decode($json, $assoc = false) {
		$s = new Services_JSON($assoc ? SERVICES_JSON_LOOSE_TYPE : 0);
		return $s->decode($json);
	}
}
// 以降、json_encode(), json_decode() が使用可能


class Instagram{
	public $images = array();
	public $weeks = array();
	public $links = array();
	
	function __construct($accessTokenValue, $countValue){
		//リクエストURL
		$request_url = 'https://api.instagram.com/v1/users/self/media/recent/?access_token='.$accessTokenValue.'&count='.$countValue;
		//JSONデータを取得し、オブジェクト形式に変換
		$obj = json_decode(@file_get_contents($request_url));
		//個々のメディア情報
		foreach((array) $obj->data as $item){
			//ID・リンク・投稿時間・コメント数・ライク数・フィルター・タグ・埋め込み用HTMLコード
			$id = $item->id;
			$link = $item->link;
			//$created = date('Y/m/d H:i',$item->created_time);
			//$created = date('W', $item->created_time);
			$created = date('Y.m.d',$item->created_time);
			$comments = (isset($item->comments->count)) ? $item->comments->count : 0;
			$likes = (isset($item->likes->count)) ? $item->likes->count : 0;
			$filter = $item->filter;
			$tags = (isset($item->tags) && $item->tags) ? implode('、',(array)$item->tags) : 'なし…';
			$embed = "&lt;iframe src=\"{$link}embed/\" width=\"612\" height=\"710\" frameborder=\"0\" scrolling=\"no\" allowtransparency=\"true\"&gt;&lt;/iframe&gt;";
			//メディアファイルのURL(画像・動画)
			$image_file = $item->images->standard_resolution->url;
			$image_width = $item->images->standard_resolution->width;
			$image_height = $item->images->standard_resolution->height;
			$movie_file = (isset($item->videos->standard_resolution->url)) ? "<br/>動画ファイルURL：".$item->videos->standard_resolution->url : '';
			//写真にタグ付けされたユーザー達の情報
			$users = $users_embed = '';
			
			foreach($item->users_in_photo as $userdata){
				//ユーザーID・ユーザーネーム・名前・アイコン・X座標・Y座標
				$tag_user = $userdata->user->id;
				$tag_username = $userdata->user->username;
				$tag_name = $userdata->user->full_name;
				$tag_icon = $userdata->user->profile_picture;
				$tag_icon_width = 75;
				$tag_icon_height = 75;
				$x_position = ($image_width * $userdata->position->x) - ($tag_icon_width / 2);
				$y_position = ($image_height * $userdata->position->y) - ($tag_icon_height / 2);
				$users .= "<br/>{$tag_name}(@{$tag_username})さんが、[X:{$x_position}][Y:{$y_position}]の位置に映ってます！";
				$users_embed .= "<img src=\"{$tag_icon}\" width=\"{$tag_icon_width}\" height=\"{$tag_icon_height}\" style=\"position:relative;top:{$y_position}px;left:{$x_position}px;\">";
			}
			
			//場所情報
			$geo = '';
			if(isset($item->location) && $item->location){
				//緯度・経度・名前
				$lat = $item->location->latitude;
				$lon = $item->location->longitude;
				$name = $item->location->name;
				$geo = "<br/>場所：<a href=\"https://www.google.co.jp/maps/@{$lat},{$lon},17z\" target=\"_blank\">{$name}</a>";
			}
			
			//画像[ユーザーがタグ付けされてる場合はアイコンを重ねる
			$this->images[] = $image_file;
			$this->links[] = $link;
			$this->tags[] = $tags;
			
			//出力
			//echo "<br/>メディアID：{$id}<br/>投稿日時：{$created}<br/>タグ：{$tags}<br/>ライク数：{$likes}人、コメント数：{$comments}<br/>リンクURL：<a href=\"{$link}\" target=\"_blank\">{$link}</a>{$geo}{$users}<br/>画像ファイルURL：{$image_file}{$movie_file}<br/>埋め込み用HTML：{$embed}<hr/>";
			$this->weeks[] = $created;
		}
	}
}
?>