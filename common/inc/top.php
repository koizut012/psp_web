<!DOCTYPE HTML>
<html lang="ja">
<head>
<meta charset="UTF-8">
<?php
$basetemplate_title = ' | 婚活パーティー・1店舗型街コン・ビジネスエリートとの出会いはPREMIUM STATUS PARTY';
$pagetitle = $page_include_title.$basetemplate_title;
$basetemplate_canonical = (empty($_SERVER["HTTPS"]) ? "http://" : "https://") . $_SERVER["HTTP_HOST"];
if($ua->set() === "tablet" || $ua->set() === "mobile"):?>
<script>
document.documentElement.classList.add('touchdevice');
</script>
<?php endif;?>
<title><?php echo $page_include_title; ?></title>
<link href="<?php echo ROOT; ?>/common/css/common.css<?php echo '?update='.date('Y-m-d_H-i-s',filemtime($_SERVER['DOCUMENT_ROOT'].'/common/css/common.css')); ?>" rel="stylesheet">
<?php if(is_array($localCSS)):foreach($localCSS as $val):?>
<link href="<?php echo $val.'?update='.date('Y-m-d_H-i-s',filemtime($_SERVER['DOCUMENT_ROOT'].$val));?>" rel="stylesheet">
<?php endforeach;endif;?>
<?php if(isset($page_include_canonical) && $page_include_canonical !== ''):?>
<link rel="canonical" href="<?php echo $basetemplate_canonical.ROOT.$page_include_canonical;?>">
<?php endif;?>
<meta name="description" content="<?php echo (isset($page_include_description) && $page_include_description !== '')?$page_include_description:'';?>">
<?php if(isset($page_include_keywords) && $page_include_keywords !== '') :?>
<meta name="keywords" content="<?php echo $page_include_keywords;?>">
<?php endif ?>
<?php if(isset($page_include_robots) && $page_include_robots !== '') :?>
<meta name="robots" content="<?php echo $page_include_robots;?>">
<?php endif ?>
<meta name="viewport" content="width=device-width">
<meta name="format-detection" content="telephone=no, email=no, address=no">
<meta name="skype_toolbar" content="skype_toolbar_parser_compatible">
<meta property="og:title" content="<?php echo $pagetitle; ?>">
<meta property="og:description" content="<?php echo (isset($page_include_description) && $page_include_description !== '')?$page_include_description:'';?>">
<?php if(isset($page_include_canonical) && $page_include_canonical !== ''):?>
<meta property="og:url" content="<?php echo $basetemplate_canonical.ROOT.$page_include_canonical;?>">
<?php endif;?>
<meta property="og:image" content="<?php echo $basetemplate_canonical.ROOT.'/common/img/ogimage.jpg'; ?>">
<?php if(isset($inlineCSS) && $inlineCSS !== ''):?>
<style type="text/css">
<?php echo $inlineCSS;?>
</style>
<?php endif;?>
</head>
<body id="page_top">
<?php if(isset($page_cat) && $page_cat == 'top_page'):?>
<div id="fb-root"></div>
<script>
(function(d, s, id) {
	var js, fjs = d.getElementsByTagName(s)[0];
	if (d.getElementById(id)) return;
	js = d.createElement(s); js.id = id;
	js.src = 'https://connect.facebook.net/ja_JP/sdk.js#xfbml=1&version=v2.11';
	fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>
<?php endif;?>

<header class="mod_header">
	<div class="wrap">
		<div class="subnav_wrap">
		<div class="mod_wrap01">
<?php if(isset($page_include_h1) && $page_include_h1 !== '') :?>
			<h1 class="txt" data-sc-pc><span><?php echo $page_include_h1;?></span></h1>
<?php endif ?>
			<ul class="sns_links">
				<li><a href="<?php echo LINK_INSTA;?>" target="_blank">
					<svg width="18" height="18">
						<desc>Instagram</desc>
						<use xlink:href="<?php echo ROOT;?>/common/svg/ico_sprite.svg#ico_insta"></use>
					</svg>
				</a></li>
				<li><a href="<?php echo LINK_FB;?>" target="_blank">
					<svg width="18" height="18">
						<desc>Facebook</desc>
						<use xlink:href="<?php echo ROOT;?>/common/svg/ico_sprite.svg#ico_fb"></use>
					</svg>
				</a></li>
				<li><a href="<?php echo LINK_TW;?>" target="_blank">
					<svg width="18" height="18">
						<desc>Twitter</desc>
						<use xlink:href="<?php echo ROOT;?>/common/svg/ico_sprite.svg#ico_tw"></use>
					</svg>
				</a></li>
			</ul>
			<ul class="inq_links" data-sc-pc>
				<li><a href="<?php echo ROOT;?>/inquiry/" class="mod_btn03 bgc01"><span>お問い合わせ</span></a></li>
			</ul>
		</div>
	</div>
		<div class="in">
		<div class="mod_wrap01">
		<div class="headerLogo"><a href="/"><img src="<?php echo ROOT;?>/common/img/header_logo.png" width="370" height="40" alt="PREMIUM STATUS PARTY"></a></div>
			<div class="nav_wrap">
			<nav>
				<ul class="mainNav">
					<li class="about"><a href="<?php echo ROOT;?>/about/"><span>プレミアムステイタスとは？</span></a></li>
					<li class="party"><a href="<?php echo ROOT;?>/schedule/"><span>パーティー検索</span></a></li>
					<li class="voice"><a href="<?php echo ROOT;?>/report/"><span>パーティー報告＆参加者の声</span></a></li>
					<li class="faq"><a href="<?php echo ROOT;?>/faq/"><span>よくあるご質問</span></a></li>
				</ul>
				<ul class="subNav" data-sc-sp>
					<li><a href="<?php echo ROOT; ?>/company/" class="mod_btn03 gst"><span>会社概要</span></a></li>
					<li><a href="<?php echo ROOT; ?>/offer/" class="mod_btn03 gst"><span>スタッフ・社員募集</span></a></li>
					<li><a href="<?php echo ROOT; ?>/rule/" class="mod_btn03 gst"><span>参加規約</span></a></li>
					<li><a href="<?php echo ROOT; ?>/notation/" class="mod_btn03 gst"><span>特定商取引に関する法律表記</span></a></li>
					<li><a href="<?php echo ROOT; ?>/privacy/" class="mod_btn03 gst"><span>プライバシーポリシー</span></a></li>
				</ul>
			</nav>
			<dl class="sideBnr" data-sc-sp>
				<dt>
					<picture>
						<source srcset="<?php echo ROOT;?>/common/img/regist_tit.png" <?php echo SC_SP;?>>
						<img src="<?php echo ROOT;?>/common/img/side_bnr.png" width="55" height="253" alt="公式アカウント無料登録でご優待情報ゲット！">
					</picture>
				</dt>
				<dd>
					<p class="cp">公式アカウント無料登録でご優待情報 GET!</p>
					<a href="<?php echo LINK_AS; ?>" target="_blank" class="store"><span><img src="<?php echo ROOT;?>/common/img/regist_app_store.png" width="166" height="50" alt="AppStore"></span></a>
					<a href="<?php echo LINK_GP; ?>" target="_blank" class="store"><span><img src="<?php echo ROOT;?>/common/img/regist_google_play.png" width="166" height="50" alt="GooglePlay"></span></a>
					<a href="<?php echo LINK_LINE; ?>" target="_blank" class="line"><span><img src="<?php echo ROOT;?>/common/img/regist_icon_line_sp.png" width="182" height="47" alt="" data-sc-sp></span></a>
					<span class="qr"><img src="<?php echo ROOT;?>/common/img/regist_qr_line.png" width="86" height="86" alt=""></span>
				</dd>
			</dl>
			<div class="inq_wrap" data-sc-sp>
				<ul class="inq_links">
					<li><a href="<?php echo ROOT;?>/inquiry/" class="mod_btn01 bgc01"><span>お問い合わせフォーム<span class="en">INQUIRY</span></span></a></li>
				</ul>
				<p>お返事には最大２営業日頂戴しております。</p>
			</div>
			<dl class="tel_wrap" data-sc-sp>
				<dt class="tel"><a data-tel="0352068288"><span class="en">TEL.03-5206-8288</span></a></dt>
				<dd class="times">【受付時間】11：00-20：00 月火定休（祝日は営業）</dd>
			</dl>
			<ul class="sns_links" data-sc-sp>
				<li><a href="<?php echo LINK_INSTA;?>" target="_blank">
					<svg width="30" height="30">
						<desc>Instagram</desc>
						<use xlink:href="<?php echo ROOT;?>/common/svg/footer_sprite.svg#footer_insta"></use>
					</svg>
				</a></li>
				<li><a href="<?php echo LINK_FB;?>" target="_blank">
					<svg width="30" height="30">
						<desc>Facebook</desc>
						<use xlink:href="<?php echo ROOT;?>/common/svg/footer_sprite.svg#footer_fb"></use>
					</svg>
				</a></li>
				<li><a href="<?php echo LINK_TW;?>" target="_blank">
					<svg width="30" height="30">
						<desc>Twitter</desc>
						<use xlink:href="<?php echo ROOT;?>/common/svg/footer_sprite.svg#footer_tw"></use>
					</svg>
				</a></li>
			</ul>
			</div>
		</div>
	</div>
	</div>
<?php if(isset($page_cat) && $page_cat == 'top_page'):?>
<dl class="sideBnr" data-sc-pc>
	<dt>
		<picture>
			<source srcset="<?php echo ROOT;?>/common/img/regist_tit.png" <?php echo SC_SP;?>>
			<img src="<?php echo ROOT;?>/common/img/side_bnr.png" width="55" height="253" alt="公式アカウント無料登録でご優待情報ゲット！">
		</picture>
	</dt>
	<dd>
		<a href="<?php echo LINK_AS; ?>" target="_blank" class="store"><span><img src="<?php echo ROOT;?>/common/img/regist_app_store.png" width="166" height="50" alt="AppStore"></span></a>
		<a href="<?php echo LINK_GP; ?>" target="_blank" class="store"><span><img src="<?php echo ROOT;?>/common/img/regist_google_play.png" width="166" height="50" alt="GooglePlay"></span></a>
		<span class="qr"><img src="<?php echo ROOT;?>/common/img/regist_qr_line.png" width="86" height="86" alt=""><img src="<?php echo ROOT;?>/common/img/regist_txt_line.png" width="71" height="20" alt="" class="txt"></span>
	</dd>
</dl>
<?php endif; ?>
</header>
<button class="mod_spNav_btn" data-sc-sp><span></span></button>
<?php if(isset($page_cat) && $page_cat !== 'top_page'):?>
<!-- パンくずユニット -->
<?php if(isset($topicpath) && is_array($topicpath) && count($topicpath)) : ?>
	<div class="mod_topicpath">
		<ul itemscope itemtype="http://schema.org/BreadcrumbList">
			<li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem"><a itemprop="item" href="/"><span itemprop="name">東京などで出会い・婚活パーティーならPREMIUM STATUSPARTY</span></a><meta itemprop="position" content="1" /></li>
<?php foreach($topicpath as $tp) : ?>
			<li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem"><?php if(isset($tp['href']) && $tp['href']) : ?><a itemprop="item" href="<?php if(preg_match("|^/|i", $tp['href'])) echo ROOT . ltrim($tp['href']); else echo $tp['href']; ?>"><?php endif; ?><span itemprop="name"><?php echo $tp['name']; ?></span><?php if(isset($tp['href']) && $tp['href']) : ?></a><?php endif; ?><meta itemprop="position" content="<?php echo $tp['count']; ?>" /></li>
<?php endforeach; ?>
		</ul>
	<!-- /.topicpath --></div>
<?php endif; ?>
<?php endif; ?>
<main class="mod_main" role="main">