'use strict'
var gulp = require('gulp'),//基本
	plumber = require('gulp-plumber'),//エラーによる強制停止を防止（gulp watchをかけているときに有効）
	notify = require('gulp-notify'),//通知のデスクトップ表示
	sass = require('gulp-sass'),//sassのコンパイル
	autoprefixer = require('gulp-autoprefixer'),//プレフィックス付与
	sourcemaps = require('gulp-sourcemaps'),//.css.map作成
	clean_css = require('gulp-clean-css'),//css圧縮
	imagemin = require('gulp-imagemin'),//画像圧縮
	pngquant = require('imagemin-pngquant'),//画像圧縮：png圧縮プラグイン
	mozjpeg = require('imagemin-mozjpeg'),//画像圧縮：png圧縮プラグイン
	concat = require('gulp-concat'),
	uglify = require('gulp-uglify')

// sassのコンパイル
gulp.task('sass', function() {
	gulp.src('../common/css/sass/*.scss')
	.pipe(sourcemaps.init())//.css.mapの作成
	.pipe(plumber({//watch中にエラーで停止させない
		errorHandler: notify.onError("Error: <%= error.message %>")//エラー表示
	}))
	.pipe(sourcemaps.write({includeContent: false}))//sassの時点で一旦インラインで書き出し
	.pipe(sourcemaps.init({loadMaps: true,identityMap: true}))//sass上記を読み込み再稼働
	.pipe(sass({
		outputStyle : 'expanded'//普通のcss記法
//    	outputStyle : 'compressed'//圧縮
	}))//cssの圧縮方法
	//.pipe(clean_css())//圧縮プラグイン使用※ソースマップズレを防ぐ
	.pipe(autoprefixer({
    	browsers: ['last 2 versions', 'ie >= 9', 'and_chr >= 2.3']
	}))//プレフィックス自動付与
	.pipe(sourcemaps.write('./maps/'))//.css.mapのの吐き出し先
	.pipe(gulp.dest('../common/css/'));//cssの吐き出し先
});//sass

// sassのコンパイル
gulp.task('sass_local', function() {
	gulp.src('../s_css/sass/*.scss')
	.pipe(sourcemaps.init())//.css.mapの作成
	.pipe(plumber({//watch中にエラーで停止させない
		errorHandler: notify.onError("Error: <%= error.message %>")//エラー表示
	}))
	.pipe(sourcemaps.write({includeContent: false}))
	.pipe(sourcemaps.init({loadMaps: true}))
	.pipe(sass({
		outputStyle : 'expanded'//普通のcss記法
//    	outputStyle : 'compressed'//圧縮
	}))//cssの圧縮方法
	//.pipe(clean_css())//圧縮プラグイン使用※ソースマップズレを防ぐ
	.pipe(autoprefixer({
    	browsers: ['last 2 versions', 'ie >= 9', 'and_chr >= 2.3']
	}))//プレフィックス自動付与
	.pipe(sourcemaps.write('./maps/'))//.css.mapのの吐き出し先
	.pipe(gulp.dest('../s_css/'));//cssの吐き出し先
});//sass_local

//js圧縮
gulp.task('jsmin', function() {
	var files = [
		'../common/js/dev/trunk8.js',
		'../common/js/dev/common.js',
		'../common/js/dev/slidemodule.js'
	];
	gulp.src(files)
	.pipe(concat('common.min.js'))
	.pipe(uglify())
	.pipe(gulp.dest('../common/js/'))
});


// 画像を圧縮
gulp.task('imagemin',function(){
	return gulp.src('./img_origin/**/*')//src_imgディレクトリ以下に画像を配置※ディレクトリの配置をサイトに合わせる※基本的にjpg,pngのみを入れる事
	.pipe(plumber())
	.pipe(imagemin([
		pngquant({ quality: '65-80', speed: 1 }),
		mozjpeg({ quality: 80,progressive: true }),
		imagemin.svgo(),
		imagemin.gifsicle()
	]))
	.pipe(gulp.dest('./img_min/'));//自動的にサイト使用ディレクトリに圧縮画像を配置
});//imagemin


// 変更ファイルをを監視して実行
gulp.task('watch', function () {
	gulp.watch('../common/css/sass/*.scss', ['sass']);//スタイルガイドも更新※保存しないと反映されない
	gulp.watch('../common/js/dev/*.js', ['jsmin']);//スタイルガイドも更新※保存しないと反映されない
	gulp.watch('../s_css/sass/*.scss', ['sass_local']);//スタイルガイドも更新※保存しないと反映されない
});

gulp.task('default', ['watch','sass','jsmin']);