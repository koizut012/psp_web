
<?php
get_header("page");
?>

<main class="mod_main" role="main">	<article id="Report" class="report">
        <header class="mv" data-lf-area data-lf-pc="/img/report/mv.jpg" data-lf-sp="/img/report/mv_sp.jpg">
            <h2 class="page_tit">
                    <img src="/img/report/title.png" width="457" height="93" alt="パーティー報告＆参加者の声">
            </h2>
        </header>

        <div class="contentsWrap">
            <div class="mod_wrap01">
                <p class="catch">婚活パーティー「プレミアムステイタス」の<br data-sc-pc>パーティー報告、参加者の声、お勧めパーティーファッションをご覧頂けます。</p>
                
                <section class="contents main index">
                <div class="detail">
                    <div class="voice_box interview" data-pc-autoheight="vc">
                        <a href="./detail.php" class="wrapLink"></a>
                        <figure data-lf-area data-lf="/img/report/ph01.jpg" class="ph"></figure>
                        <div class="detail" data-pc-autoheight="vc_wrap">
                            <h2 class="tit">【名古屋】土曜日の夜は栄えの隠れ家で大人のパーティ♪♪<br>好評トヨタ系企業企画！</h2>
                        </div>
                        <p class="link"><a href="./detail.php" class="more_btn"><span>続きを読む</span></a></p>
                    </div>
                    <div class="voice_box fashion" data-pc-autoheight="vc">
                        <a href="./detail.php" class="wrapLink"></a>
                        <figure data-lf-area data-lf="/img/report/ph02.jpg" class="ph"></figure>
                        <div class="detail" data-pc-autoheight="vc_wrap">
                            <h2 class="tit">パーティーお勧めファッション<br>タイトル入ります。</h2>
                        </div>
                        <p class="link"><a href="./detail.php" class="more_btn"><span>続きを読む</span></a></p>
                    </div>
                    <div class="voice_box report" data-pc-autoheight="vc">
                        <a href="./detail.php" class="wrapLink"></a>
                        <figure data-lf-area data-lf="/img/report/ph03.jpg" class="ph"></figure>
                        <div class="detail" data-pc-autoheight="vc_wrap">
                            <h2 class="tit">【名古屋】土曜日の夜は栄えの隠れ家で大人のパーティ♪♪<br>好評トヨタ系企業企画！</h2>
                        </div>
                        <p class="link"><a href="./detail.php" class="more_btn"><span>続きを読む</span></a></p>
                    </div>
                    <div class="voice_box interview" data-pc-autoheight="vc">
                        <a href="./detail.php" class="wrapLink"></a>
                        <figure data-lf-area data-lf="/img/report/ph01.jpg" class="ph"></figure>
                        <div class="detail" data-pc-autoheight="vc_wrap">
                            <h2 class="tit">【名古屋】土曜日の夜は栄えの隠れ家で大人のパーティ♪♪<br>好評トヨタ系企業企画！</h2>
                        </div>
                        <p class="link"><a href="./detail.php" class="more_btn"><span>続きを読む</span></a></p>
                    </div>
                    <div class="voice_box fashion" data-pc-autoheight="vc">
                        <a href="./detail.php" class="wrapLink"></a>
                        <figure data-lf-area data-lf="/img/report/ph02.jpg" class="ph"></figure>
                        <div class="detail" data-pc-autoheight="vc_wrap">
                            <h2 class="tit">パーティーお勧めファッション<br>タイトル入ります。</h2>
                        </div>
                        <p class="link"><a href="./detail.php" class="more_btn"><span>続きを読む</span></a></p>
                    </div>
                    <div class="voice_box report" data-pc-autoheight="vc">
                        <a href="./detail.php" class="wrapLink"></a>
                        <figure data-lf-area data-lf="/img/report/ph03.jpg" class="ph"></figure>
                        <div class="detail" data-pc-autoheight="vc_wrap">
                            <h2 class="tit">【名古屋】土曜日の夜は栄えの隠れ家で大人のパーティ♪♪<br>好評トヨタ系企業企画！</h2>
                        </div>
                        <p class="link"><a href="./detail.php" class="more_btn"><span>続きを読む</span></a></p>
                    </div>
                    <div class="voice_box interview" data-pc-autoheight="vc">
                        <a href="./detail.php" class="wrapLink"></a>
                        <figure data-lf-area data-lf="/img/report/ph01.jpg" class="ph"></figure>
                        <div class="detail" data-pc-autoheight="vc_wrap">
                            <h2 class="tit">【名古屋】土曜日の夜は栄えの隠れ家で大人のパーティ♪♪<br>好評トヨタ系企業企画！</h2>
                        </div>
                        <p class="link"><a href="./detail.php" class="more_btn"><span>続きを読む</span></a></p>
                    </div>
                    <div class="voice_box fashion" data-pc-autoheight="vc">
                        <a href="./detail.php" class="wrapLink"></a>
                        <figure data-lf-area data-lf="/img/report/ph02.jpg" class="ph"></figure>
                        <div class="detail" data-pc-autoheight="vc_wrap">
                            <h2 class="tit">パーティーお勧めファッション<br>タイトル入ります。</h2>
                        </div>
                        <p class="link"><a href="./detail.php" class="more_btn"><span>続きを読む</span></a></p>
                    </div>
                    <div class="voice_box report" data-pc-autoheight="vc">
                        <a href="./detail.php" class="wrapLink"></a>
                        <figure data-lf-area data-lf="/img/report/ph03.jpg" class="ph"></figure>
                        <div class="detail" data-pc-autoheight="vc_wrap">
                            <h2 class="tit">【名古屋】土曜日の夜は栄えの隠れ家で大人のパーティ♪♪<br>好評トヨタ系企業企画！</h2>
                        </div>
                        <p class="link"><a href="./detail.php" class="more_btn"><span>続きを読む</span></a></p>
                    </div>
                    <div class="voice_box interview" data-pc-autoheight="vc">
                        <a href="./detail.php" class="wrapLink"></a>
                        <figure data-lf-area data-lf="/img/report/ph01.jpg" class="ph"></figure>
                        <div class="detail" data-pc-autoheight="vc_wrap">
                            <h2 class="tit">【名古屋】土曜日の夜は栄えの隠れ家で大人のパーティ♪♪<br>好評トヨタ系企業企画！</h2>
                        </div>
                        <p class="link"><a href="./detail.php" class="more_btn"><span>続きを読む</span></a></p>
                    </div>
                    <div class="voice_box fashion" data-pc-autoheight="vc">
                        <a href="./detail.php" class="wrapLink"></a>
                        <figure data-lf-area data-lf="/img/report/ph02.jpg" class="ph"></figure>
                        <div class="detail" data-pc-autoheight="vc_wrap">
                            <h2 class="tit">パーティーお勧めファッション<br>タイトル入ります。</h2>
                        </div>
                        <p class="link"><a href="./detail.php" class="more_btn"><span>続きを読む</span></a></p>
                    </div>
                    <div class="voice_box report" data-pc-autoheight="vc">
                        <a href="./detail.php" class="wrapLink"></a>
                        <figure data-lf-area data-lf="/img/report/ph03.jpg" class="ph"></figure>
                        <div class="detail" data-pc-autoheight="vc_wrap">
                            <h2 class="tit">【名古屋】土曜日の夜は栄えの隠れ家で大人のパーティ♪♪<br>好評トヨタ系企業企画！</h2>
                        </div>
                        <p class="link"><a href="./detail.php" class="more_btn"><span>続きを読む</span></a></p>
                    </div>
                </div>

                <ul class="pager tp01">
                                                            <li class="next"><a href=""><span>次の12件へ</span></a></li>
                </ul>
                </section>
                
                <?php get_sidebar();?>
                
            </div>
        </div>
    </article>
</main>

<?php
get_footer();
?>

