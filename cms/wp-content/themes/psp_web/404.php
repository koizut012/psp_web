<?php get_header(); ?>

<main class="mod_main" role="main">
    <article id="404" class="inquiry">

        <header class="base">
            <h2 class="page_tit">
                お探しのページはみつかりませんでした
            </h2>
        </header>

        <div class="contentsWrap">
            <div class="mod_wrap01">
                <section class="contents main">
                    <div class="detail">
                        <div class="contentsWrap">
                            <div class="mod_wrap01">
                                <div class="mod_wrap04">
                                    <ul class="pager tp02">
                                        <li><a href="<?php echo home_url() . '/'; ?>"><span>トップページへ</span></a></li>
                                    </ul>

                                </div>
                            </div>
                        </div>
                    </div>
                </section>

            </div>
        </div>
    </article>
</main>

<?php get_footer();
