
<?php
get_header("page");
?>
    
<main class="mod_main" role="main">
    <article id="Report" class="report">



<div class="breadcrumbs">
    <?php if(function_exists('bcn_display'))
    {
        //bcn_display();
    }?>
</div>



        <header class="base">
            <h2 class="page_tit">
                    <img src="/img/report/detail_title.png" width="457" height="93" alt="パーティー報告＆参加者の声">
            </h2>
        </header>

        <div class="contentsWrap">
            <div class="mod_wrap01">
            
<?php
if(have_posts()): while(have_posts()): the_post();
$cat = get_the_category();
$cat_name = $cat[0]->name;
$report_flg = false;
if( $cat_name == "レポート" ){
    $detail_class_name = "report";
    $report_flg = true;
}else if( $cat_name == "インタビュー" ){
    $detail_class_name = "interview";
}else if( $cat_name == "ファッション" ){
    $detail_class_name = "fashion";
}
?>
            
                <section class="contents main">
                    <div class="detail <?php echo $detail_class_name;?>">
                        <p class="category"><?php echo $cat_name;?></p>
                        <header>
                            <h2 class="title">
                            <?php the_title(); ?>
                            </h2>
                            
                            <?php if( $report_flg == true ): ?>
                            <p class="date">開催日：<span><?php echo get_post_meta($post->ID , '開催日' ,true); ?></span></p>
                            <p class="place">会　場：<span><?php echo get_post_meta($post->ID , '会場' ,true); ?></span></p>
                            <?php endif; ?>
                            
                        </header>

                        <div class="item_box">
                        
                            <?php the_content(); ?>

                            <?php if( $report_flg == true ): ?>
                            <div class="ph_box">
                                <figure><?php echo wp_get_attachment_image(get_post_meta($post->ID,"サブ写真1",true),array(244,143)); ?></figure>
                                <figure><?php echo wp_get_attachment_image(get_post_meta($post->ID,"サブ写真1",true),array(244,143)); ?></figure>
                                <figure><?php echo wp_get_attachment_image(get_post_meta($post->ID,"サブ写真1",true),array(244,143)); ?></figure>
                            </div>
                            <?php endif; ?>
                            
                        </div>
                    </div>
                    <ul class="pager tp02">
                        <li class="back"><a href="<?php echo home_url().'/report/';?>"><span>パーティー報告＆参加者の声　一覧へ戻る</span></a></li>
                                            </ul>
                </section>
                
<?php
endwhile; endif;
?>
                
                <?php get_sidebar();?>
                
            </div>
        </div>
    </article>
</main>

<?php
get_footer();
?>

