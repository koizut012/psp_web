<?php

add_action('init', 'create_post_type');
function create_post_type()
{
    $labels = [
        'name' => _x('ブログ', 'post type general name'),
        'singular_name' => _x('ブログ', 'post type singular name'),
    ];
    $args = [
        'labels' => $labels,
        'public' => true,
        'publicly_queryable' => true,
        'show_ui' => true,
        'query_var' => true,
        'rewrite' => true,
        'capability_type' => 'post',
        'hierarchical' => false,
        'menu_position' => 4,
        'supports' => ['title', 'editor', 'thumbnail', 'custom-fields', 'excerpt', 'revisions', 'page-attributes'],
        'has_archive' => true,
        'description' => 'ブログ',
    ];
    register_post_type('blog', $args);

    register_taxonomy(
        'blog-cat',
        'blog',
        [
            'hierarchical' => true,
            'update_count_callback' => '_update_post_term_count',
            'label' => 'ブログのカテゴリー',
            'singular_label' => 'ブログのカテゴリー',
            'public' => true,
            'show_ui' => true,
        ]
    );
    add_theme_support('post-thumbnails');
    set_post_thumbnail_size(624, 9999); // Unlimited height, soft crop

}

// カテゴリー指定なしのアーカイブページを、reports/として表示
function custom_rewrite_basic() {
    add_rewrite_rule('reports/?$', 'index.php?orderby=title&order=DESC', 'top');
}
add_action('init', 'custom_rewrite_basic');


function get_terms_slug_by_post_id($id)
{

    global $wpdb;

    $query = sprintf("
SELECT term_taxonomy_id FROM %s where object_id='%s'",
        $wpdb->term_relationships, $id);
    $rows = $wpdb->get_results($query, ARRAY_A);
    $term_taxonomy_id = $rows[0]["term_taxonomy_id"];

    if ($term_taxonomy_id == "") {
        return false;
    }

    $query = sprintf("
SELECT term_id FROM %s where term_taxonomy_id='%s'",
        $wpdb->term_taxonomy, $term_taxonomy_id);
    $rows = $wpdb->get_results($query, ARRAY_A);
    $term_id = $rows[0]["term_id"];

    if ($term_id == "") {
        return false;
    }

    $query = sprintf("
SELECT slug FROM %s where term_id='%s'",
        $wpdb->terms, $term_id);
    $rows = $wpdb->get_results($query, ARRAY_A);
    $slug = $rows[0]["slug"];

    if ($slug == "") {
        return false;
    }

    return $slug;

}

function get_posts_all_data_num($post_type, $category_type)
{
    global $wpdb;

    if ($category_type == "all") {

        $query = sprintf("
SELECT ID FROM %s where post_status='publish' and post_type='%s'",
            $wpdb->posts, $post_type);

    } else {

        $query = sprintf("
SELECT wp_posts.ID FROM wp_posts 
left join wp_term_relationships on wp_term_relationships.object_id=wp_posts.ID 
left join wp_term_taxonomy on wp_term_taxonomy.term_taxonomy_id=wp_term_relationships.term_taxonomy_id 
left join wp_terms on wp_terms.term_id=wp_term_taxonomy.term_id 
where wp_posts.post_status='publish' and wp_posts.post_type='%s' and wp_terms.slug='%s'",
            $post_type, $category_type);

    }
    $data = $wpdb->get_results($query, ARRAY_A);
    $data_num = count($data);
    return $data_num;
}

function get_archive_paging_data($disp_num, $page_num, $category_type)
{

    $all_num = get_posts_all_data_num("post", $category_type);

    if ($page_num == "1") {
        $num_start = "1";
        $num_end = $disp_num;
    } else {
        $num_start = (($page_num - 1) * $disp_num) + 1;
        $num_end = ($num_start + $disp_num) - 1;
    }

    if ($num_end >= $all_num) {
        $next_exist = false;
        $next_num = "0";
    } else {
        $next_exist = true;
        $sa = $all_num - $num_end;
        if ($sa >= $disp_num) {
            $next_num = $disp_num;
        } else {
            $next_num = $sa;
        }
    }

    $back_start_num = $num_start - $disp_num;

    if ($back_start_num > 0) {
        $back_exist = true;
        $back_num = $disp_num;
    } else {
        $back_exist = false;
        $back_num = "0";
    }

    $data["back"]["exist"] = $back_exist;
    $data["back"]["blog_num"] = $back_num;
    $data["back"]["page_num"] = $page_num - 1;
    $data["next"]["exist"] = $next_exist;
    $data["next"]["blog_num"] = $next_num;
    $data["next"]["page_num"] = $page_num + 1;

    return $data;

}

function get_archive_posts_data($page_num, $disp_num, $category_type)
{

    global $wpdb;

    $limit_start = ($page_num - 1) * $disp_num;

    if ($category_type == "all") {

        $query = sprintf("
SELECT ID, post_title,post_name FROM %s
where post_status='publish' and post_type='post'
ORDER BY post_date desc LIMIT %s,%s",
            $wpdb->posts, $limit_start, $disp_num);

    } else {

        $query = sprintf("
SELECT wp_posts.ID, wp_posts.post_title,wp_posts.post_name FROM wp_posts 
left join wp_term_relationships on wp_term_relationships.object_id=wp_posts.ID 
left join wp_term_taxonomy on wp_term_taxonomy.term_taxonomy_id=wp_term_relationships.term_taxonomy_id 
left join wp_terms on wp_terms.term_id=wp_term_taxonomy.term_id 
where wp_posts.post_status='publish' and wp_posts.post_type='post' and wp_terms.slug='%s' 
ORDER BY wp_posts.post_date desc LIMIT %s,%s",
            $category_type, $limit_start, $disp_num);

    }

    $posts_data = $wpdb->get_results($query, ARRAY_A);

    return $posts_data;

}

function get_category_type_by_url()
{

    $cate_all_flg = get_cate_all_flg();

    if ($cate_all_flg == true) {
        return "all";
    }

    $data = $_SERVER["REQUEST_URI"];

    $data = explode("?", $data);
    $data = $data[0];

    $data = explode("/", $data);

    $data_num = count($data);

    for ($i = 0; $i < $data_num; $i++) {

        $tmp = $data[$i];

        if ($tmp == "interview") {

            return $tmp;

        } else if ($tmp == "fashion") {

            return $tmp;

        }

    }

    for ($i = 0; $i < $data_num; $i++) {

        $tmp = $data[$i];

        if ($tmp == "report") {

            return $tmp;

        }

    }

    return "all";

}

function get_date_page_year_month()
{

    $request_uri = $_SERVER["REQUEST_URI"];
    
    $data = str_replace("reports/date/", "", $request_uri);
    
    $pieces = explode("/", $data);
    
    $tmp["year"] = intval($pieces["1"]);
    $tmp["month"] = intval($pieces["2"]);
    
    return $tmp;

}

function get_posts_sql_where_year_month($year, $month)
{

    $year = intval($year);
    $month = intval($month);

    if ($month == "0") {

        $time_1 = sprintf("%s-01-01 00:00:00", $year);
        $time_2 = sprintf("%s-12-31 23:59:59", $year);


    } else {

        $month = if_under_10_add_zero($month);
        $year_month = sprintf('%s-%s', $year, $month);
        $day_last = date('d', strtotime('last day of ' . $year_month));

        $time_1 = sprintf("%s-%s-01 00:00:00", $year, $month);
        $time_2 = sprintf("%s-%s-%s 23:59:59", $year, $month, $day_last);

    }

    $sql_where = sprintf("((wp_posts.post_date>='%s') and (wp_posts.post_date<='%s'))", $time_1, $time_2);

    return $sql_where;

}

function if_under_10_add_zero($data)
{

    $data = intval($data);

    if ($data < 10) {
        $data = "0" . $data;
    }

    return $data;

}

function get_archive_posts_data_for_date($page_num, $disp_num, $sql_where)
{

    global $wpdb;

    $limit_start = ($page_num - 1) * $disp_num;

    $query = sprintf("
SELECT ID, post_title,post_name FROM wp_posts 
where %s and post_status='publish' and post_type='post'
ORDER BY post_date desc LIMIT %s,%s",
        $sql_where, $limit_start, $disp_num);

    $posts_data = $wpdb->get_results($query, ARRAY_A);

    return $posts_data;

}

function get_archive_paging_data_for_date($disp_num, $page_num, $sql_where)
{

    $all_num = get_posts_all_data_num_for_date("post", $sql_where);

    if ($page_num == "1") {
        $num_start = "1";
        $num_end = $disp_num;
    } else {
        $num_start = (($page_num - 1) * $disp_num) + 1;
        $num_end = ($num_start + $disp_num) - 1;
    }

    if ($num_end >= $all_num) {
        $next_exist = false;
        $next_num = "0";
    } else {
        $next_exist = true;
        $sa = $all_num - $num_end;
        if ($sa >= $disp_num) {
            $next_num = $disp_num;
        } else {
            $next_num = $sa;
        }
    }

    $back_start_num = $num_start - $disp_num;

    if ($back_start_num > 0) {
        $back_exist = true;
        $back_num = $disp_num;
    } else {
        $back_exist = false;
        $back_num = "0";
    }

    $data["back"]["exist"] = $back_exist;
    $data["back"]["blog_num"] = $back_num;
    $data["back"]["page_num"] = $page_num - 1;
    $data["next"]["exist"] = $next_exist;
    $data["next"]["blog_num"] = $next_num;
    $data["next"]["page_num"] = $page_num + 1;

    return $data;

}

function get_posts_all_data_num_for_date($post_type, $sql_where)
{

    global $wpdb;

    $query = sprintf("
SELECT ID FROM wp_posts where %s and post_status='publish' and post_type='%s'",
        $sql_where, $post_type);

    $data = $wpdb->get_results($query, ARRAY_A);
    $data_num = count($data);

    return $data_num;

}

function get_cate_all_flg()
{

    $root_url = (empty($_SERVER["HTTPS"]) ? "http://" : "https://") . $_SERVER["HTTP_HOST"];
    $home_url = home_url();
    $request_uri = $_SERVER["REQUEST_URI"];

    if (strpos($request_uri, 'reports/report') !== false) {
        return false;
    }

    $data = str_replace($root_url, "", $home_url);

    $data = $data . "/";

    $data = str_replace($data, "", $request_uri);

    $pieces = explode("?", $data);
    $data = $pieces[0];

    if ($data == "report/") {

        return true;

    } else {

        return false;

    }

}

function get_archive_posts_data_new_bk()
{

    global $wpdb;

    $disp_num = 5;

    $query = sprintf("
SELECT wp_posts.ID, wp_posts.post_title,wp_posts.post_name,wp_terms.slug FROM wp_posts
left join wp_term_relationships on wp_term_relationships.object_id=wp_posts.ID
left join wp_term_taxonomy on wp_term_taxonomy.term_taxonomy_id=wp_term_relationships.term_taxonomy_id
left join wp_terms on wp_terms.term_id=wp_term_taxonomy.term_id
where wp_posts.post_status='publish' and wp_posts.post_type='post' 
ORDER BY wp_posts.post_date desc LIMIT 0,%s",
        $disp_num);

    $posts_data = $wpdb->get_results($query, ARRAY_A);

    return $posts_data;

}

function get_archive_posts_data_new()
{

    global $wpdb;

    $disp_num = 5;

    $query = sprintf("
SELECT wp_posts.ID, wp_posts.post_title,wp_posts.post_name,post_content FROM wp_posts
where wp_posts.post_status='publish' and wp_posts.post_type='post'
ORDER BY wp_posts.post_date desc LIMIT 0,%s",
        $disp_num);

    $posts_data = $wpdb->get_results($query, ARRAY_A);

    $posts_data_num = count($posts_data);

    for ($i = 0; $i < $posts_data_num; $i++) {

        $post_id = $posts_data[$i]["ID"];
        $posts_data[$i]["slug"] = get_slug_by_post_id($post_id);

        $thumbnail_id = get_post_meta($post_id, '_thumbnail_id', true);
        $eye_img = wp_get_attachment_image_src($thumbnail_id, 'full');
        $posts_data[$i]["eye_img"] = $eye_img[0];

        $post_content = wp_strip_all_tags($posts_data[$i]["post_content"]);

        $posts_data[$i]["excerpt_disp"] = mb_strimwidth($post_content, 0, 100, "...", 'UTF-8');

    }

    return $posts_data;

}

function get_slug_by_post_id($post_id)
{

    global $wpdb;

    $query = sprintf("
SELECT wp_terms.slug FROM wp_term_relationships
left join wp_term_taxonomy on wp_term_taxonomy.term_taxonomy_id=wp_term_relationships.term_taxonomy_id
left join wp_terms on wp_terms.term_id=wp_term_taxonomy.term_id
where wp_term_relationships.object_id='%s'",
        $post_id);

    $posts_data = $wpdb->get_results($query, ARRAY_A);

    return $posts_data[0]["slug"];

}

//スラッシュを追加
function add_slash_uri_end($uri, $type)
{
    if ($type != 'single') {
        $uri = trailingslashit($uri);
    }
    return $uri;
}

add_filter('user_trailingslashit', 'add_slash_uri_end', 10, 2);

//remove_filter('the_content', 'wpautop');// 記事の自動整形を無効にする
//remove_filter('the_excerpt', 'wpautop');// 抜粋の自動整形を無効にする

function get_archive_page_type()
{

    $cate_all_flg = get_cate_all_flg();

    if ($cate_all_flg == true) {

        return "all";

    } else {

        $data = get_category_type_by_url();

        if (($data == "interview") || ($data == "fashion") || ($data == "report")) {

            return "category";

        } else {

            $data = get_date_page_year_month();

            if ($data["year"] > 2000) {

                return "date";

            }

        }

    }

    return false;

}

function get_archive_h1_value()
{

    $page_type = get_archive_page_type();

    if ($page_type == "category") {

        $tmp = get_category_type_by_url();
        $category_name = get_category_name_ja($tmp);

        $h1 = $category_name . "｜パーティー報告＆参加者の声｜" . get_bloginfo('name');

    } else if ($page_type == "date") {

        $data = get_date_page_year_month();

        if ($data["month"] == "0") {

            $disp_val = $data["year"] . "年";

        } else {

            $disp_val = $data["year"] . "年" . $data["month"] . "月";

        }

        $h1 = $disp_val . "｜パーティー報告＆参加者の声｜" . get_bloginfo('name');

    } else {

        //all
        $h1 = get_the_title() . "｜" . get_bloginfo('name');

    }

    return $h1;

}

function get_category_name_ja($tmp)
{

    if ($tmp == "interview") {

        return "インタビュー";

    } else if ($tmp == "fashion") {

        return "ファッション";

    } else if ($tmp == "report") {

        return "レポート";

    }

    return "不明";

}

function get_front_page_posts_data()
{

    $data = get_archive_posts_data_new();
    $return_data = [];

    for ($i = 0; $i < 4; $i++) {
        $id = $data[$i]["ID"];
        $return_data[$i] = $data[$i];
        $return_data[$i]["event_date"] = get_report_event_date_by_id($id);
        $return_data[$i]["meeting_place"] = get_report_meeting_place_by_id($id);
    }

    return $return_data;

}

function get_report_event_date_by_id($id)
{

    global $wpdb;

    $query = sprintf("
SELECT meta_value FROM wp_postmeta where meta_key='開催日' and post_id='%s'",
        $id);
    $rows = $wpdb->get_results($query, ARRAY_A);
    $meta_value = $rows[0]["meta_value"];

    if ($meta_value == "") {
        return false;
    }

    return $meta_value;

}

function get_report_meeting_place_by_id($id)
{

    global $wpdb;

    $query = sprintf("
SELECT meta_value FROM wp_postmeta where meta_key='会場' and post_id='%s'",
        $id);
    $rows = $wpdb->get_results($query, ARRAY_A);
    $meta_value = $rows[0]["meta_value"];

    if ($meta_value == "") {
        return false;
    }

    return $meta_value;

}

function get_this_page_url_for_archive($data)
{

    if ($data == "interview") {

        $url = home_url() . "/reports/interview/";

    } else if ($data == "fashion") {

        $url = home_url() . "/reports/fashion/";

    } else if ($data == "report") {

        $url = home_url() . "/reports/report/";

    } else {

        $url = home_url() . "/reports/";

    }

    return $url;

}

//パーマリンクカテゴリ削除(categoryをreportに変更)(minamikawa)
//「パーマリング設定」の「カテゴリーベース」をreportに変更する方法だと、
//レポートの記事が表示されなくなってしまうので、以下の記述の方法を選択

// koizumi comment out
add_filter('user_trailingslashit', 'remcat_function');
function remcat_function($link)
{
    return str_replace("/category/", "/", $link);
}

add_action('init', 'remcat_flush_rules');
function remcat_flush_rules()
{
    global $wp_rewrite;
    $wp_rewrite->flush_rules();
}

add_filter('generate_rewrite_rules', 'remcat_rewrite');
function remcat_rewrite($wp_rewrite)
{
    $new_rules = ['(.+)/page/(.+)/?' => 'index.php?category_name=' . $wp_rewrite->preg_index(1) . '&paged=' . $wp_rewrite->preg_index(2)];
    $wp_rewrite->rules = $new_rules + $wp_rewrite->rules;
}

function get_information_data_new()
{

    global $wpdb;

    $disp_num = 5;

    $query = sprintf("
SELECT ID, post_title,post_date FROM wp_posts 
where post_status='publish' and post_type='information'
ORDER BY post_date desc LIMIT 0,%s",
        $disp_num);

    $data = $wpdb->get_results($query, ARRAY_A);

    $data_num = count($data);

    $now_time = date_i18n("Y/m/d H:i:s");
    $ts_now = strtotime($now_time);
    $ts_past_3day = $ts_now - (60 * 60 * 24 * 3);

    for ($i = 0; $i < $data_num; $i++) {

        $ts_tmp = strtotime($data[$i]["post_date"]);

        if ($ts_tmp > $ts_past_3day) {
            $data[$i]["new_flg"] = true;
        } else {
            $data[$i]["new_flg"] = false;
        }

        $pieces = explode(" ", $data[$i]["post_date"]);
        $data[$i]["post_date_disp"] = $pieces[0];

    }

    return $data;

}

function get_rss_lightup()
{

    include_once(ABSPATH . WPINC . '/feed.php');

    $rss = fetch_feed('https://www.statusparty.jp/lightup/feed/');

    // すべてのフィードから最新５件を出力します。
    $maxitems = $rss->get_item_quantity(3);

    // 0件から始めて指定した件数までの配列を生成します。
    $rss_items = $rss->get_items(0, $maxitems);

    $return_data = [];
    $i = 0;

    foreach ($rss_items as $item) {

        $url = $item->get_permalink();

        $title = $item->get_title();

        $pieces = explode("jpg", $item->get_description());
        $img_url = $pieces[0] . "jpg";

        $category = $item->data["child"][""]["category"][0]["data"];

        $tmp = $item->data["child"][""]["pubDate"][0]["data"];

        $ts = strtotime($tmp);

        //一日プラス
        $ts = $ts + (24 * 60 * 60);

        $date = date("Y.m.d", $ts);

        $return_data[$i]["url"] = $url;
        $return_data[$i]["title"] = $title;
        $return_data[$i]["img_url"] = $img_url;
        $return_data[$i]["date"] = $date;
        $return_data[$i]["category"] = $category;
        $i++;

    }

    return $return_data;

}

