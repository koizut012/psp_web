
                <aside class="side">
                    <div class="side_box">
                        <h3 class="cat_title"><span>カテゴリ</span></h3>
                        <ul class="cat_link">
                            <li class="interview"><a href="<?php echo home_url()."/reports/interview/";?>"><span>インタビュー</span></a></li>
                            <li class="fashion"><a href="<?php echo home_url()."/reports/fashion/";?>"><span>ファッション</span></a></li>
                            <li class="report"><a href="<?php echo home_url()."/reports/report/";?>"><span>レポート</span></a></li>
                        </ul>
                    </div>

                    <div class="side_box">
                        <h3 class="cat_title"><span>最新記事</span></h3>
                        <ul class="item_link">
                        <?php
                        //最新5件
                        $posts_data_new = get_archive_posts_data_new();
                        
                        foreach($posts_data_new as $key => $value):
                            if( $value["slug"] == "interview" ){
                                $class_name = "interview";
                                $cate_name = "インタビュー";
                            }else if( $value["slug"]== "fashion" ){
                                $class_name = "fashion";
                                $cate_name = "ファッション";
                            }else if( $value["slug"]== "report" ){
                                $class_name = "report";
                                $cate_name = "レポート";
                            }else{
                                $class_name = "interview";
                                $cate_name = "インタビュー";
                            }
                            $link_url = home_url()."/reports/".$value["slug"]."/".$value["ID"];
                        ?>
                            <li class="<?php echo $class_name;?>">
                                <a href="<?php echo $link_url;?>">
                                    <p class="category"><span><?php echo $cate_name;?></span></p>
                                    <h3 class="title"><?php echo $value["post_title"];?></h3>
                                </a>
                            </li>
                        <?php
                        endforeach;
                        ?>
                        </ul>
                    </div>
                    
<div class="side_box">
<h3 class="cat_title"><span>過去の記事</span></h3>
<form action="./" class="formArea">
<div class="itemWrap selectWrap selectMonthWrap">
<span class="select">
<select id="selectMonth2" name="selectMonth" onChange='document.location.href=this.options[this.selectedIndex].value;'>
<option value="">月を検索</option>
<?php
$defaults = array(
    'type' => 'monthly', 
    'limit' => '',
    'format' => 'option', 
    'before' => '',
    'after' => '', 
    'show_post_count' => false,
    'echo' => 1, 
    'order' => 'DESC',
    'post_type' => 'post'
);
wp_get_archives($defaults);
?>
</select>
</span>
</div>
</form>
</div>

<br><br>

                </aside>
                