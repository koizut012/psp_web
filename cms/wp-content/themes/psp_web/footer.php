<!-- /.mod_main --></main>

<footer class="mod_footer">
    <ul class="inq_links" data-sc-sp>
        <li><a href="/schedule/"><span><img src="/common/img/footer_btn_txt01_sp.png" width="129" height="26" alt="Party検索"></span></a></li>
        <li><a href="/report/"><span><img src="/common/img/footer_btn_txt02_sp.png" width="124" height="26" alt="参加者の声"></span></a></li>
    </ul>
            
    <div class="upper_wrap">
        <div class="mod_wrap01">
        <dl class="recommendBnr">
        <dt><span class="en">RECOMMENDED SERVICE</span>エリート対象の立食形式婚活パーティー＆1店舗型街コン「プレミアムステイタスパーティー」の姉妹サービスです。</dt>
        <dd>
            <ul class="service_links">
            <li><a href="https://www.w-premium.jp/lp/ps/" target="blank"><img src="/common/img/service_bnr01.png" width="220" height="107" alt=""></a></li>
            <li><a href="https://mrmiss.asia/lp/ps/" target="blank"><img src="/common/img/service_bnr02.png" width="220" height="107" alt=""></a></li>
            <li><a href="https://www.statusclub.jp/" target="blank"><img src="/common/img/service_bnr03.png" width="220" height="107" alt=""></a></li>
            <li><a href="https://white-p.jp/" target="blank"><img src="/common/img/service_bnr04.png" width="220" height="107" alt=""></a></li>
            <li><a href="https://www.jieitaiclub.jp/" target="blank"><img src="/common/img/service_bnr05.png" width="220" height="107" alt=""></a></li>
            <li><a href="https://jieitai-bridal.jp/" target="blank"><img src="/common/img/service_bnr06.png" width="220" height="107" alt=""></a></li>
            <li><a href="https://1000.style/" target="blank"><img src="/common/img/service_bnr07.png" width="220" height="107" alt=""></a></li>
            <li><a href="https://www.breeze-club.jp/" target="blank"><img src="/common/img/service_bnr08.png" width="220" height="107" alt=""></a></li>
            <li><a href="http://privateclub.jp/contents/fusion_bridal/" target="blank"><img src="/common/img/service_bnr09.png" width="220" height="107" alt=""></a></li>
            <li><a href="https://premium-days.com" target="blank"><img src="/common/img/service_bnr10.png" width="220" height="107" alt=""></a></li>
        </ul>
        </dd>
    </dl>
    </div>
    </div>
    <div class="lower_wrap">
        <h1 class="footerLogo"><a href="/"><img src="/common/img/footer_logo.png" width="342" height="22" alt="PREMIUM STATUS PARTY"></a></h1>

        <div class="contactBox">
            <div class="inq_wrap">
                <p class="link"><a href="<?php echo home_url();?>/inquiry/" class="mod_btn01 bgc01"><span>お問い合わせフォーム<span class="en">INQUIRY</span></span></a></p>
                <p class="cap">お返事には最大２営業日頂戴しております</p>
            </div>
            
            <dl class="tel_wrap">
                <dt class="tel"><a data-tel="0352068288"><span class="en">TEL.03-5206-8288</span></a></dt>
                <dd class="times">【受付時間】11：00-20：00 月火定休（祝日は営業）</dd>
            </dl>
            
            <div class="mod_wrap01">
                <ul class="sns_links">
                    <li><a href="https://www.instagram.com/premiumstatus/" target="_blank">
                        <svg width="30" height="30">
                            <desc>Instagram</desc>
                            <use xlink:href="/common/svg/footer_sprite.svg#footer_insta"></use>
                        </svg>
                    </a></li>
                    <li><a href="https://www.facebook.com/premiumstatusparty/" target="_blank">
                        <svg width="30" height="30">
                            <desc>Facebook</desc>
                            <use xlink:href="/common/svg/footer_sprite.svg#footer_fb"></use>
                        </svg>
                    </a></li>
                    <li><a href="https://twitter.com/premiumstatus" target="_blank">
                        <svg width="30" height="30">
                            <desc>Twitter</desc>
                            <use xlink:href="/common/svg/footer_sprite.svg#footer_tw"></use>
                        </svg>
                    </a></li>
                </ul>
            </div>
        </div>
        <div class="nav_wrap">
        <nav data-sc-pc>
            <ul class="mainNav">
                <li><a href="/about/"><span>プレミアムステイタスとは？</span></a></li>
                <li><a href="/schedule/"><span>パーティー検索</span></a></li>
                <li><a href="/report/"><span>パーティー報告＆参加者の声</span></a></li>
                <li><a href="/faq/"><span>よくあるご質問</span></a></li>
            </ul>
            <ul class="subNav">
                <li><a href="/company/"><span>会社概要</span></a></li>
                <li><a href="/offer/"><span>サポーター＆スタッフ募集</span></a></li>
                <li><a href="/rule/"><span>参加規約</span></a></li>
                <li><a href="/notation/"><span>特定商取引に関する法律表記</span></a></li>
                <li><a href="/privacy/"><span>プライバシーポリシー</span></a></li>
            </ul>
            
            <ul class="areaNav">
                <li><a href="/schedule/area.php?"><span>銀座</span></a></li>
                <li><a href="/schedule/area.php?"><span>新宿</span></a></li>
                <li><a href="/schedule/area.php?"><span>恵比寿</span></a></li>
                <li><a href="/schedule/area.php?"><span>東京</span></a></li>
                <li><a href="/schedule/area.php?"><span>目黒</span></a></li>
                <li><a href="/schedule/area.php?"><span>六本木</span></a></li>
                <li><a href="/schedule/area.php?"><span>横浜</span></a></li>
                <li><a href="/schedule/area.php?"><span>名古屋</span></a></li>
                <li><a href="/schedule/area.php?"><span>大阪</span></a></li>
                <li><a href="/schedule/area.php?"><span>京都</span></a></li>
                <li><a href="/schedule/area.php?"><span>神戸</span></a></li>
                <li><a href="/schedule/area.php?"><span>福岡</span></a></li>
                <li><a href="/schedule/area.php?"><span>札幌</span></a></li>
            </ul>
        </nav>
        <p class="copyright">&copy;2017 PREMIUM STATUS PARTY. All Rights Reserved.</p>
        </div>
        </div>
</footer>
<div class="mod_toTop_area">
    <a href="#page_top" class="mod_toTop"></a>
</div>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>
<script src="/common/js/jquery/jquery.inview.min.js"></script>
<script src="/common/js/jquery/perfect-scrollbar.jquery.min.js"></script>
<script src="/common/js/gsap/TweenMax.min.js"></script>
<script src="/common/js/common.min.js?update=2018-02-21_12-57-09"></script>
<script src="/common/js/jquery/datepicker/jquery-ui.min.js?update=2018-02-21_12-57-09"></script>
<script src="/common/js/jquery/datepicker/jquery.ui.datepicker-ja.min.js?update=2018-02-21_12-57-09"></script>
<script src="/js/index.js?update=2018-02-21_12-57-09"></script>
<script src="<?php echo home_url();?>/js/script.js?<?php echo time();?>"></script>

<?php wp_footer(); ?>

</body>
</html>
