<?php
/**
 * Template Name: お問い合わせテンプレート
 */
?>

<?php
get_header("inquiry");
?>

<main class="mod_main" role="main">
<article id="Inquiry" class="inquiry">
<header class="base">
<h2 class="page_tit">
<img src="/img/inquiry/title.png" width="218" height="93" alt="お問い合わせ">
</h2>
</header>



<div class="contentsWrap">
<div class="mod_wrap04 formArea">
<?php
while ( have_posts() ) : the_post();
the_content();
endwhile;
?>
</div>
</div>



</article>
</main>

<?php
get_footer();
?>
