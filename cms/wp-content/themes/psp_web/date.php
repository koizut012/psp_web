<?php

$page_num = intval($_GET["page_num"]);

$disp_num = "12";

if( ($page_num=="") || ($page_num=="0") ){
    $page_num = "1";
}

$data = get_date_page_year_month();

$year = $data["year"];
$month = $data["month"];

// 2018-02-21 13:42:03
$sql_where = get_posts_sql_where_year_month($year,$month);

$posts_data = get_archive_posts_data_for_date($page_num,$disp_num,$sql_where);

$paging_data = get_archive_paging_data_for_date($disp_num,$page_num,$sql_where);

/*
echo "<pre>";
print_r($posts_data);
echo "</pre>";
exit();
*/

?>

<?php
get_header("archive");
?>

<main class="mod_main" role="main">	<article id="Report" class="report">
        <header class="mv" data-lf-area data-lf-pc="/img/report/mv.jpg" data-lf-sp="/img/report/mv_sp.jpg">
            <h2 class="page_tit">
                    <img src="/img/report/title.png" width="457" height="93" alt="パーティー報告＆参加者の声">
            </h2>
        </header>

        <div class="contentsWrap">
            <div class="mod_wrap01">
                <p class="catch">婚活パーティー「プレミアムステイタス」の<br data-sc-pc>パーティー報告、参加者の声、お勧めパーティーファッションをご覧頂けます。</p>
                
                <section class="contents main index">
                <div class="detail">
                
                
                    <?php
                    $home_url = home_url();
                    foreach($posts_data as $key => $value):
                        $id = $value["ID"];
                        $post_name = $value["post_name"];
                        $slug = get_terms_slug_by_post_id($id);
                        if( $slug == "interview" ){
                            $class_name = "interview";
                            $img_url = "/img/report/ph01.jpg";
                        }else if( $slug == "fashion" ){
                            $class_name = "fashion";
                            $img_url = "/img/report/ph02.jpg";
                        }else if( $slug == "report2" ){
                            $class_name = "report";
                            $img_url = "/img/report/ph03.jpg";
                        }else{
                            $class_name = "interview";
                            $img_url = "/img/report/ph01.jpg";
                        }
                        $link_url = $home_url."/".$slug."/".$post_name;
                    ?>
                    <div class="voice_box <?php echo $class_name;?>" data-pc-autoheight="vc">
                        <a href="<?php echo $link_url;?>" class="wrapLink"></a>
                        <figure data-lf-area data-lf="<?php echo $img_url;?>" class="ph"></figure>
                        <div class="detail" data-pc-autoheight="vc_wrap">
                            <h2 class="tit"><?php echo $value["post_title"];?></h2>
                        </div>
                        <p class="link"><a href="<?php echo $link_url;?>" class="more_btn"><span>続きを読む</span></a></p>
                    </div>
                    <?php
                    endforeach;
                    ?>
                
                </div>

                <ul class="pager tp01">
                    <?php if( $paging_data["back"]["exist"] == true ):?>
                    <li class="back"><a href="<?php echo $this_page_url."?page_num=".$paging_data["back"]["page_num"];?>"><span>前の<?php echo $paging_data["back"]["blog_num"];?>件へ</span></a></li>
                    <?php endif;?>
                                                                
                    <?php if( $paging_data["next"]["exist"] == true ):?>
                    <li class="next"><a href="<?php echo $this_page_url."?page_num=".$paging_data["next"]["page_num"];?>"><span>次の<?php echo $paging_data["next"]["blog_num"];?>件へ</span></a></li>
                    <?php endif;?>
                </ul>
                </section>
                
                <?php get_sidebar();?>
                
            </div>
        </div>
    </article>
</main>

<?php
get_footer();
?>
​