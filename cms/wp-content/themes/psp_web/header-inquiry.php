
<!DOCTYPE HTML>
<html lang="ja">
<head>
<meta charset="UTF-8">
<title>お問い合わせ｜婚活パーティー・恋活パーティーならPREMIUM STATUS PARTY</title>
<link href="/common/css/common.css?update=2018-02-21_12-57-08" rel="stylesheet">
<link href="/s_css/inquiry.css?update=2018-02-21_12-57-10" rel="stylesheet">
<link href="<?php echo home_url();?>/css/style.css?<?php echo time();?>" rel="stylesheet">
<link rel="canonical" href="http://psp-web.localhost/inquiry/">
<meta name="description" content="お問い合わせの確認画面です。「プレミアムステイタス」の婚活パーティー、恋活パーティーにご興味がある方はお気軽にお問い合わせください。プライベートライフの向上を目的としたハイステイタス対象の出会いパーティーを開催いたしております。">
<meta name="keywords" content="婚活パーティー,恋活パーティー,出会い,東京,大阪">
<meta name="viewport" content="width=device-width">
<meta name="format-detection" content="telephone=no, email=no, address=no">
<meta name="skype_toolbar" content="skype_toolbar_parser_compatible">
<meta property="og:title" content="お問い合わせ｜婚活パーティー・恋活パーティーならPREMIUM STATUS PARTY | 婚活パーティー・1店舗型街コン・ビジネスエリートとの出会いはPREMIUM STATUS PARTY">
<meta property="og:description" content="お問い合わせの確認画面です。「プレミアムステイタス」の婚活パーティー、恋活パーティーにご興味がある方はお気軽にお問い合わせください。プライベートライフの向上を目的としたハイステイタス対象の出会いパーティーを開催いたしております。">
<meta property="og:url" content="http://psp-web.localhost/inquiry/">
<meta property="og:image" content="http://psp-web.localhost/common/img/ogimage.jpg">

<?php //wp_head(); ?>
</head>
<body id="page_top">

<header class="mod_header">
<div class="wrap">
<div class="subnav_wrap">
<div class="mod_wrap01">
<h1 class="txt" data-sc-pc><span>お問い合わせ | 東京、大阪、名古屋での婚活パーティー・恋活パーティーなら、プレミアムステイタスパーティー</span></h1>
<ul class="sns_links">
<li><a href="https://www.instagram.com/premiumstatus/" target="_blank">
<svg width="18" height="18">
<desc>Instagram</desc>
<use xlink:href="/common/svg/ico_sprite.svg#ico_insta"></use>
</svg>
</a></li>
<li><a href="https://www.facebook.com/premiumstatusparty/" target="_blank">
<svg width="18" height="18">
<desc>Facebook</desc>
<use xlink:href="/common/svg/ico_sprite.svg#ico_fb"></use>
</svg>
</a></li>
<li><a href="https://twitter.com/premiumstatus" target="_blank">
<svg width="18" height="18">
<desc>Twitter</desc>
<use xlink:href="/common/svg/ico_sprite.svg#ico_tw"></use>
</svg>
</a></li>
</ul>
<ul class="inq_links" data-sc-pc>
<li><a href="<?php echo home_url();?>/inquiry/" class="mod_btn03 bgc01"><span>お問い合わせ</span></a></li>
</ul>
</div>
</div>
<div class="in">
<div class="mod_wrap01">
<div class="headerLogo"><a href="<?php echo home_url();?>/"><img src="/common/img/header_logo.png" width="370" height="40" alt="PREMIUM STATUS PARTY"></a></div>
<div class="nav_wrap">
<nav>
<ul class="mainNav">
<li class="about"><a href="/about/"><span>プレミアムステイタスとは？</span></a></li>
<li class="party"><a href="/schedule/"><span>パーティー検索</span></a></li>
<li class="voice"><a href="/report/"><span>パーティー報告＆参加者の声</span></a></li>
<li class="faq"><a href="/faq/"><span>よくあるご質問</span></a></li>
</ul>
<ul class="subNav" data-sc-sp>
<li><a href="/company/" class="mod_btn03 gst"><span>会社概要</span></a></li>
<li><a href="/offer/" class="mod_btn03 gst"><span>スタッフ・社員募集</span></a></li>
<li><a href="/rule/" class="mod_btn03 gst"><span>参加規約</span></a></li>
<li><a href="/notation/" class="mod_btn03 gst"><span>特定商取引に関する法律表記</span></a></li>
<li><a href="/privacy/" class="mod_btn03 gst"><span>プライバシーポリシー</span></a></li>
</ul>
</nav>
<dl class="sideBnr" data-sc-sp>
<dt>
<picture>
<source srcset="/common/img/regist_tit.png"  media="(max-width:640px)">
<img src="/common/img/side_bnr.png" width="55" height="253" alt="公式アカウント無料登録でご優待情報ゲット！">
</picture>
</dt>
<dd>
<p class="cp">公式アカウント無料登録でご優待情報 GET!</p>
<a href="https://itunes.apple.com/jp/app/id1205369404" target="_blank" class="store"><span><img src="/common/img/regist_app_store.png" width="166" height="50" alt="AppStore"></span></a>
<a href="https://play.google.com/store/apps/details?id=jp.statusparty.spapp" target="_blank" class="store"><span><img src="/common/img/regist_google_play.png" width="166" height="50" alt="GooglePlay"></span></a>
<a href="https://line.me/R/ti/p/%40wsx7805b" target="_blank" class="line"><span><img src="/common/img/regist_icon_line_sp.png" width="182" height="47" alt="" data-sc-sp></span></a>
<span class="qr"><img src="/common/img/regist_qr_line.png" width="86" height="86" alt=""></span>
</dd>
</dl>
<div class="inq_wrap" data-sc-sp>
<ul class="inq_links">
<li><a href="/inquiry/" class="mod_btn01 bgc01"><span>お問い合わせフォーム<span class="en">INQUIRY</span></span></a></li>
</ul>
<p>お返事には最大２営業日頂戴しております。</p>
</div>
<dl class="tel_wrap" data-sc-sp>
<dt class="tel"><a data-tel="0352068288"><span class="en">TEL.03-5206-8288</span></a></dt>
<dd class="times">【受付時間】11：00-20：00 月火定休（祝日は営業）</dd>
</dl>
<ul class="sns_links" data-sc-sp>
<li><a href="https://www.instagram.com/premiumstatus/" target="_blank">
<svg width="30" height="30">
<desc>Instagram</desc>
<use xlink:href="/common/svg/footer_sprite.svg#footer_insta"></use>
</svg>
</a></li>
<li><a href="https://www.facebook.com/premiumstatusparty/" target="_blank">
<svg width="30" height="30">
<desc>Facebook</desc>
<use xlink:href="/common/svg/footer_sprite.svg#footer_fb"></use>
</svg>
</a></li>
<li><a href="https://twitter.com/premiumstatus" target="_blank">
<svg width="30" height="30">
<desc>Twitter</desc>
<use xlink:href="/common/svg/footer_sprite.svg#footer_tw"></use>
</svg>
</a></li>
</ul>
</div>
</div>
</div>
</div>
</header>
<button class="mod_spNav_btn" data-sc-sp><span></span></button>
<!-- パンくずユニット -->
<div class="mod_topicpath">
<ul itemscope itemtype="http://schema.org/BreadcrumbList">
<li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem"><a itemprop="item" href="/"><span itemprop="name">東京などで出会い・婚活パーティーならPREMIUM STATUSPARTY</span></a><meta itemprop="position" content="1" /></li>
<li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem"><a itemprop="item" href="/inquiry/"><span itemprop="name">お問い合わせ</span></a><meta itemprop="position" content="2" /></li>
</ul>
<!-- /.topicpath --></div>