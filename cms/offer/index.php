<?php
$thisurl=dirname(__FILE__);require_once($_SERVER['DOCUMENT_ROOT'].'/common/inc/config.php');
$page_include_title = 'サポーター＆スタッフ募集｜婚活パーティー・恋活パーティーならPREMIUM STATUS PARTY';
$page_include_description = '「PREMIUM STATUS PARTY」ではパーティーの集客や運営をお手伝いいただける方を募集しています。上場企業・経営者・医師・公務員などビジネスエリートを対象。婚活や恋活を希望の方は「PREMIUM STATUS PARTY」まで。';
$page_include_keywords = '婚活パーティー,恋活パーティー,出会い,東京,大阪';
$page_include_robots = '';
$page_include_canonical = '/offer/';
$page_include_h1 = 'サポート&スタッフ募集 | 東京、大阪、名古屋での婚活パーティー・恋活パーティーなら、プレミアムステイタスパーティー';
$localCSS = array(
	ROOT.'/s_css/offer.css',
);
$localJS = array(
	//ROOT.'/js/index.js',
);

//ページカテゴリ
$page_cat = 'offer';

// パンくず
 $topicpath = array(
 	array(
 		'name' => 'サポーター＆スタッフ募集',//名前
 		'href' => '/offer/',//パス
 		'count' => '2',//階層※2階層目から
 	),
 );
?>

	<?php require_once('common/inc/top.php');?>
	<article id="Offer" class="offer">
		<header class="base">
			<h2 class="page_tit">
				<img src="<?php echo ROOT;?>/img/offer/title.png" width="404" height="92" alt="サポーター＆スタッフ募集">
			</h2>
		</header>

		<div class="contentsWrap">
			<div class="mod_wrap04">
				<section class="contents">
					<h3 class="sec_tit02 regular"><span>正社員募集</span></h3>
					<dl>
						<dt>イベントの企画・運営責任者 / 事務スタッフ</dt>
						<dd>年間開催数1200本の恋活・婚活パーティーの企画立案から、各会場の運営を切り盛りする現場統括スタッフ、またパーティー全般をサポートする事務スタッフなど、あなたの活躍の場は様々。学歴不問!あなたのやる気次第でお給料がUPしていくので魅力的。</dd>
					</dl>
					<div class="link">
						<a href="#Regular" class="mod_btn05"><span>詳細を見る</span></a>
					</div>

					<h3 class="sec_tit02 part"><span>アルバイトスタッフの募集</span></h3>
					<dl>
						<dt>運営スタッフ / 事務スタッフ / SE / ウェブデザイナー / カメラマン / ライター / サポータースタッフ</dt>
						<dd>毎回オシャレなレストランや夜景が見えるLoungeなどで開催する、恋活・婚活パーティーの運営スタッフ募集!司会や受付またドリンクスタッフなど華やかな会場で楽しみながら働きませんか!また働き方は自由自在で在宅勤務可能なSE・ウェブデザイナーや、パーティー会場で参加者の笑顔や風景を撮影するカメラマン、お客さんにインタビューして参加者の生の声を記事にするライター、パーティー集客をお手伝い頂くサポータースタッフを募集しています。</dd>
					</dl>
					<div class="link">
						<a href="#Part" class="mod_btn05"><span>詳細を見る</span></a>
					</div>

					<section>
						<div class="table_box" id="Regular">
							<div class="title_box">
								<h3 class="tit">正社員募集</h3>
								<p>イベント企画・運営責任者 / 事務スタッフ</p>
							</div>
							<dl>
								<dt>仕事内容</dt>
								<dd>
									<p><b class="c02">【イベント企画・運営責任者】</b><br> 恋活・婚活パーティーの企画立案から運営まで、現場責任者としてのお仕事をお任せいたします。
										<br> また新規会場の開拓や企画を作る楽しさを実感でき、やる気次第でお給料UPが見込めます。
									</p>
									<p><b class="c02">【事務スタッフ】</b><br> 人気の婚活パーティー事業部の事務Staffは、お客様からのメール対応、顧客データの入力、お客様からの問い合わせの対応など会社をサポートする役割を担います。
										<br>また営業の電話を掛けることなどは一切ないので安心です。</p>
								</dd>
							</dl>
							<dl>
								<dt>給与</dt>
								<dd>
									<p>20万円～<br>※基本給はスキルに応じて変動</p>
								</dd>
							</dl>
							<dl>
								<dt>賞与</dt>
								<dd>
									<p>毎年1回</p>
								</dd>
							</dl>
							<dl>
								<dt>休み</dt>
								<dd>
									<p>月曜日・火曜日</p>
								</dd>
							</dl>
							<dl>
								<dt>福利厚生</dt>
								<dd>
									<p>・毎年1回人間ドッグ<br>・毎月3000円のカフェ代支給</p>
								</dd>
							</dl>
							<dl>
								<dt>社員旅行</dt>
								<dd>
									<p>過去：ハワイ、グアム、沖縄、宮古島、石垣島など</p>
								</dd>
							</dl>
							<dl>
								<dt>夏季休暇</dt>
								<dd>
									<p>有給とは別に3日</p>
								</dd>
							</dl>
							<dl>
								<dt>代休制度</dt>
								<dd>
									<p>定休日の月曜日または火曜日が祝日で出勤の場合は、代休制度有り</p>
								</dd>
							</dl>
							<dl>
								<dt>社会保険</dt>
								<dd>
									<p>有</p>
								</dd>
							</dl>
							<dl>
								<dt>所在地</dt>
								<dd>
									<p>東京都 新宿区 津久戸町3-19 えひらビル2階(本社)</p>
								</dd>
							</dl>
							<dl>
								<dt>勤務体制</dt>
								<dd>
									<p>水曜日～日曜日<br> 11:00～20:00
										<br> ※パーティー運営時は13:00～22:00（企画により時間変動）
										<br> ※月火定休
										<br>※月火が祝日の場合は別日で代休</p>
								</dd>
							</dl>
							<dl>
								<dt>応募資格</dt>
								<dd>
									<p>学歴不問・未経験者歓迎！<br> 企画・運営責任者：20代～30代前半
										<br> 事務スタッフ：20代～40代
									</p>
								</dd>
							</dl>
						</div>
						<div class="box_wrap">
							<h3 class="title"><span>興味のある方は<br data-sc-sp>以下担当までご連絡下さい。</span></h3>
							<p>担当：島岡 学（しまおか まなぶ）</p>
							<ul class="info_box">
								<li><a data-tel="080-4141-9401">080-4141-9401</a><a data-tel="03-5206-8288">03-5206-8288</a></li>
								<li>11:00～20:00<br data-sc-sp>（月曜火曜休み　※祝日の場合は営業）</li>
								<li><a href="mailto:pscima@softbank.ne.jp">pscima@softbank.ne.jp</a><a href="mailto:cima@fandr.jp">cima@fandr.jp</a></li>
							</ul>
						</div>
					</section>

					<section>
						<div class="table_box" id="Part">
							<div class="title_box">
								<h3 class="tit">アルバイトスタッフ募集</h3>
								<p>運営スタッフ / 事務スタッフ / システムエンジニア / ウェブデザイナー / ライター / カメラマン / サポータースタッフ</p>
							</div>
							<dl>
								<dt>仕事内容</dt>
								<dd>
									<p><b class="c02">【運営スタッフ】</b><br> 会場設営やドリンクサーブや受付などの業務を行ってもらいます。また司会者につきましては別途司会手当があります。
										<br> 現在大学生から社会人まで幅広く在籍しており、アットホームな環境で楽しく働けます。
									</p>
									<p><b class="c02">【事務スタッフ】</b><br> お客様からのメール対応、顧客データの入力、お客様からの問い合わせなどベテランスタッフがひとつずつ丁寧に指導いたしますので、未経験の方でも安心です。
										<br> また営業の電話を掛けることなどは一切ございません。
									</p>
									<p><b class="c02">【システムエンジニア】</b><br> 社内WEBシステムの改修、保守、開発
										<br> PHPによるシステムの開発経験やHTML/CSSの基本的な知識
										<br> 経験者はもちろん歓迎ですが、プログラミングに関する業務経験が少ない、これからスキルアップしたい方も10年以上経験がある現職のエンジニアがサポートいたしますので、安心して業務にあたりスキルアップができるチャンスです。また自分のライフスタイルに合わせて自由に働けるのも魅力です。
									</p>
									<p><b class="c02">【ライター】</b><br> 既にライターのお仕事をしている方や、とにかく書くのが好きな方なら未経験者も歓迎!パーティー参加者のインタビューをもとに生の言葉を書き起こし、あなたの言葉で恋活パーティーを魅力的なものにしてください。
									</p>
									<p><b class="c02">【カメラマン】</b><br> パーティー風景や参加者などの笑顔を撮影するカメラマン
										<br> 現在カメラを源共通の方や、プロで活躍するカメラマンなどを募集しています。
									</p>
									<p><b class="c02">【ウェブデザイナー】</b><br> 各サイトのデザインや画像編集作業などを行ってもらいますが、あなたのライフスタイルに合わせて在宅やカフェまたは当社にて自由な時間に働くことができます。
									</p>
									<p><b class="c02">【サポータースタッフ】</b><br> 本業や学業をかけ持ちしながら、パーティー参加者の集客をお手伝い頂くお仕事になります。
										<br> 集客人数によって報酬がUPするため、やりがいのあるお仕事です。
									</p>
								</dd>
							</dl>
							<dl>
								<dt>時給</dt>
								<dd>
									<dl class="inline tp02">
										<dt class="c02">【運営スタッフ】</dt>
										<dd>1100円～</dd>
									</dl>
									<dl class="inline tp02">
										<dt class="c02">【事務スタッフ】</dt>
										<dd>1100円～</dd>
									</dl>
									<dl class="inline tp02">
										<dt class="c02">【システムエンジニア】</dt>
										<dd>1200～3000円</dd>
									</dl>
									<dl class="inline tp02">
										<dt class="c02">【ウェブデザイナー】</dt>
										<dd>1200～3000円</dd>
									</dl>
									<dl class="inline tp02">
										<dt class="c02">【ライター】</dt>
										<dd>7000円～（1Party取材＆ライター）</dd>
									</dl>
									<dl class="inline tp02">
										<dt class="c02">【カメラマン】</dt>
										<dd>7000円～（1Party:2h）</dd>
									</dl>
									<dl class="inline tp02">
										<dt class="c02">【サポータースタッフ】</dt>
										<dd>要相談</dd>
									</dl>
								</dd>
							</dl>
							<dl>
								<dt>交通費</dt>
								<dd>
									<p>往復2000円まで</p>
								</dd>
							</dl>
							<dl>
								<dt>勤務体系</dt>
								<dd>
									<p>火、水、木、金、土、日<br>※火曜日は隔週開催</p>
								</dd>
							</dl>
							<dl>
								<dt>勤務時間</dt>
								<dd>
									<p><b class="c02">【運営スタッフ】</b><br>平日：19:00～23:00<br>土曜：13:00～17:00 or 18:00～22:00<br>日曜：13:00～17:00 or 17:00～21:00<br>※土日は会場によって時間変動有り</p>
									<p><b class="c02">【事務スタッフ】</b><br>平日・土曜：11:00～20:30<br>日曜：11:00～20:00<br>実働4h以上～</p>
								</dd>
							</dl>
							<dl>
								<dt>所在地</dt>
								<dd>
									<p>東京都 新宿区 津久戸町3-19 えひらビル2階(本社)</p>
								</dd>
							</dl>
							<dl>
								<dt>応募資格</dt>
								<dd>
									学歴不問・未経験者歓迎!
									<dl class="inline tp02">
										<dt class="c02">【運営スタッフ】</dt>
										<dd>20代30代</dd>
									</dl>
									<dl class="inline tp02">
										<dt class="c02">【事務スタッフ】</dt>
										<dd>20代～50代</dd>
									</dl>
									<dl class="inline tp02">
										<dt class="c02">【システムエンジニア】</dt>
										<dd>年齢問わず</dd>
									</dl>
									<dl class="inline tp02">
										<dt class="c02">【ウェブデザイナー】</dt>
										<dd>年齢問わず</dd>
									</dl>
									<dl class="inline tp02">
										<dt class="c02">【ライター】</dt>
										<dd>20代～40代</dd>
									</dl>
									<dl class="inline tp02">
										<dt class="c02">【カメラマン】</dt>
										<dd>20代～40代</dd>
									</dl>
									<dl class="inline tp02">
										<dt class="c02">【サポータースタッフ】</dt>
										<dd>年齢問わず</dd>
									</dl>
								</dd>
							</dl>
						</div>
						<div class="box_wrap">
							<h3 class="title"><span>興味のある方は<br data-sc-sp>以下担当までご連絡下さい。</span></h3>
							<p>担当：島岡 学（しまおか まなぶ）</p>
							<ul class="info_box">
								<li><a data-tel="080-4141-9401">080-4141-9401</a><a data-tel="03-5206-8288">03-5206-8288</a></li>
								<li>11:00～20:00<br data-sc-sp>（月曜火曜休み　※祝日の場合は営業）</li>
								<li><a href="mailto:pscima@softbank.ne.jp">pscima@softbank.ne.jp</a><a href="mailto:cima@fandr.jp">cima@fandr.jp</a></li>
							</ul>
						</div>
					</section>
				</section>
			</div>
		</div>
	</article>
	<?php require_once('common/inc/bottom.php');?>