<?php
$thisurl=dirname(__FILE__);require_once($_SERVER['DOCUMENT_ROOT'].'/common/inc/config.php');
$page_include_title = 'パーティー予約｜出会い・婚活パーティーならPREMIUM STATUSPARTY';
$page_include_description = '';
$page_include_keywords = '婚活パーティー・恋活パーティー,東京,大阪,名古屋';
$page_include_robots = '';
$page_include_canonical = '/reservation/';
$page_include_h1 = '●●●●● | 東京、大阪、名古屋での出会い・婚活パーティーなら PREMIUM STATUSPARTY';
$localCSS = array(
	ROOT.'/s_css/reservation.css',
);
$localJS = array(
	ROOT.'/js/reservation.js',
);

//ページカテゴリ
$page_cat = 'reservation';

// パンくず
 $topicpath = array(
 	array(
 		'name' => 'パーティー予約',//名前
 		'href' => '/reservation/',//パス
 		'count' => '2',//階層※2階層目から
 	),
 );
?>

	<?php require_once('common/inc/top.php');?>
	<article id="Reservation" class="reservation step04">
		<header class="base">
			<h2 class="page_tit">
				<img src="<?php echo ROOT;?>/img/reservation/title.png" width="240" height="92" alt="パーティー予約">
			</h2>
		</header>

		<div class="contentsWrap">
			<div class="mod_wrap01">

				<div class="flow_box">
					<p class="flow03"><img src="<?php echo ROOT;?>/img/reservation/flow03.png" width="543" height="56" alt=""></p>
				</div>
				<p class="step_txt">参加費のお支払い</p>

				<div class="mod_wrap04">
					<form id="Form" autocomplete="on" enctype="multipart/form-data" name="form" method="post" action="./" class="formArea">
						<div class="price_box">
							<p class="price">参加費：0,000円</p>
							<p class="memo">1. 上記金額は割引前の金額合計です。　2. 割引等により差額が発生する場合は、会場にて返金致します。</p>
						</div>

						<div class="rowWrap">
							<dl class="element" data-need="" data-form-unique="radio_pay">
								<dt><span>お支払い方法</span></dt>
								<dd>
									<div class="itemWrap radioWrap radio_payBox">
										<span class="radio">
											<input type="radio" name="radio_pay"  value="cash" id="radio_pay0">
											<label for="radio_pay0"><span>当日現金払い</span></label>
										</span>
										<span class="radio">
											<input type="radio" name="radio_pay"  value="credit" id="radio_pay1">
											<label for="radio_pay1"><span>クレジット決済</span></label>
										</span>
										<span class="radio">
											<input type="radio" name="radio_pay"  value="cvs" id="radio_pay2">
											<label for="radio_pay2"><span>コンビニ決済</span></label>
										</span>
									</div>
								</dd>
							</dl>
						</div>

						<!--当日現金払い-->
						<div id="Cash">
							<div class="link">
								<div class="wrap">
									<label for="ConfirmBtn" class="Confirm mod_btn07">
									<input id="ConfirmBtn" value="上記内容で予約する" type="submit">
								</label>

									<label for="BackBtn" class="back mod_btn07 tp02">
									<input id="BackBtn" type="button" value="内容を修正する" onClick="history.back()">
								</label>
								</div>
							</div>
						</div>
						<!--当日現金払い-->

						<!--カード払い-->
						<div id="Credit">
							<div class="rowWrap">
								<dl class="element">
									<dt><span>カード番号</span></dt>
									<dd>
										<div class="itemWrap"><img src="<?php echo ROOT;?>/img/reservation/cardface.png" width="271" height="47" alt=""></div>
									</dd>
								</dl>
							</div>
							<div class="rowWrap">
								<dl class="element" data-need="" data-form-unique="text_card">
									<dt><span>カード番号</span></dt>
									<dd>
										<div class="itemWrap textWrap text_cardBox w02 spW01"><input type="tel" name="text_card" id="text_card" value="" size="15" placeholder="1234123412341234"></div>
									</dd>
								</dl>
							</div>
							<div class="rowWrap">
								<dl class="element" data-need="">
									<dt><span>カード名義</span></dt>
									<dd>
										<div class="itemWrap textWrap name01_wrap w01" data-form-unique="name01">
											<label for="name01"><input name="name01" id="name01" value="" placeholder="TARO" type="text"></label></div>

										<div class="itemWrap textWrap name02_wrap w01" data-form-unique="name02">
											<label for="name02"><input name="name02" id="name02" value="" placeholder="YAMADA" type="text"></label></div>
									</dd>
								</dl>
							</div>
							<div class="rowWrap">
								<dl class="element" data-need="">
									<dt><span>有効期限（月 / 年）</span></dt>
									<dd>
										<div class="itemWrap textWrap date01_wrap w01" data-form-unique="date01">
											<label for="date01"><input name="date01" id="date01" value="" placeholder="MM" type="text"></label></div>

										<div class="itemWrap textWrap date02_wrap w01" data-form-unique="date02">
											<label for="date02"><input name="date02" id="date02" value="" placeholder="YY" type="text"></label></div>
										<p class="memo">※それぞれ2桁の半角数字で入力してください</p>
									</dd>
								</dl>
							</div>
							<div class="rowWrap">
								<dl class="element" data-need="" data-form-unique="text_secrity">
									<dt><span>セキュリティコード</span></dt>
									<dd>
										<div class="itemWrap textWrap text_secrityBox w01"><input type="tel" name="text_secrity" id="text_secrity" value="" size="15" placeholder="123"></div>
										<p class="memo">※ セキュリティコードについて<br>クレジットカードの裏面・表面に記載されている3桁もしくは4桁の数字を入力ください。</p>
										<p class="cardface"><img src="<?php echo ROOT;?>/img/reservation/cardface02.png" width="318" height="86" alt=""></p>
									</dd>
								</dl>
							</div>
							<dl class="memo">
								<dt>カード決済後のキャンセルは、以下の手数料が発生いたします。</dt>
								<dd>1万円未満…1,080円　　1〜3万円未満…2,160円　　3万円以上…3,240円</dd>
							</dl>
							<div class="link">
								<div class="wrap">
									<label for="ConfirmBtn1" class="Confirm mod_btn07">
									<input id="ConfirmBtn1" value="上記内容で決済して予約する" type="submit">
								</label>

									<label for="BackBtn1" class="back mod_btn07 tp02">
									<input id="BackBtn1" type="button" value="内容を修正する" onClick="history.back()">
								</label>
								</div>
							</div>
						</div>
						<!--//カード払い-->

						<!--コンビニ払い-->
						<div id="Cvs">
							<div class="rowWrap">
								<dl class="element" data-need="" data-form-unique="radio_pay">
									<dt><span>コンビニ<br data-sc-pc>決済種別を<br>選択してください。</span></dt>
									<dd>
										<div class="itemWrap radioWrap radio_paytypeBox">
											<span class="radio">
											<input type="radio" name="radio_paytype"  value="セブン決済" id="radio_paytype0">
											<label for="radio_paytype0"><span>セブン決済</span></label>
											</span>
											<span class="radio">
											<input type="radio" name="radio_paytype"  value="Loppi決済" id="radio_paytype1">
											<label for="radio_paytype1"><span>Loppi決済</span></label>
											</span>
											<span class="radio">
											<input type="radio" name="radio_paytype"  value="ファミマ決済" id="radio_paytype2">
											<label for="radio_paytype2"><span>ファミマ決済</span></label>
											</span>
										</div>
										<ul class="info">
											<li><img src="<?php echo ROOT;?>/img/reservation/cvs_icon01.png" width="86" height="20" alt="セブン決済"><span>セブンイレブン</span></li>
											<li><img src="<?php echo ROOT;?>/img/reservation/cvs_icon02.png" width="86" height="20" alt="Loppi決済"><span>ローソン・ミニストップ・サークルKサンクス・セイコーマート</span></li>
											<li><img src="<?php echo ROOT;?>/img/reservation/cvs_icon03.png" width="86" height="20" alt="ファミマ決済"><span>ファミリーマート</span></li>
										</ul>
									</dd>
								</dl>
							</div>

							<div class="rowWrap">
								<dl class="element" data-need="">
									<dt><span>氏名</span></dt>
									<dd>
										<div class="itemWrap textWrap name01_wrap w01" data-form-unique="name01b">
											<label for="name01b"><input name="name01b" id="name01b" value="" placeholder="姓" type="text"></label></div>

										<div class="itemWrap textWrap name02_wrap w01" data-form-unique="name02b">
											<label for="name02b"><input name="name02b" id="name02b" value="" placeholder="名" type="text"></label></div>
									</dd>
								</dl>
							</div>
							<div class="rowWrap">
								<dl class="element" data-need="">
									<dt><span>住所</span></dt>
									<dd>
										<div class="itemWrap textWrap address_wrap w02 spW01" data-form-unique="address">
											<label for="address"><input name="address" id="address" value="" placeholder="例：東京都新宿区××××1-2-3××ビル〇階" type="text"></label></div>
										<p class="memo">※コンビニ決済のためのみに利用いたします。</p>
									</dd>
								</dl>
							</div>
							<p class="memo c02"><b>※コンビニ決済の場合はお客様負担で手数料216円がかかります。<br>※予約完了後にお支払い頂くコンビニの変更はできませんのでご注意ください。</b></p>
							<div class="link">
								<div class="wrap">
									<label for="ConfirmBtn2" class="Confirm mod_btn07">
									<input id="ConfirmBtn2" value="コンビニ決済で予約する" type="submit">
								</label>

									<label for="BackBtn2" class="back mod_btn07 tp02">
									<input id="BackBtn2" type="button" value="内容を修正する" onClick="history.back()">
								</label>
								</div>
							</div>
						</div>
						<!--//コンビニ払い-->
					</form>
				</div>
			</div>
		</div>
	</article>
	<?php require_once('common/inc/bottom.php');?>