<?php
$thisurl=dirname(__FILE__);require_once($_SERVER['DOCUMENT_ROOT'].'/common/inc/config.php');
$page_include_title = 'パーティー予約｜出会い・婚活パーティーならPREMIUM STATUSPARTY';
$page_include_description = '';
$page_include_keywords = '婚活パーティー・恋活パーティー,東京,大阪,名古屋';
$page_include_robots = '';
$page_include_canonical = '/reservation/';
$page_include_h1 = '●●●●● | 東京、大阪、名古屋での出会い・婚活パーティーなら PREMIUM STATUSPARTY';
$localCSS = array(
	ROOT.'/s_css/reservation.css',
);
$localJS = array(
	//ROOT.'/js/reservation.js',
);

//ページカテゴリ
$page_cat = 'reservation';

// パンくず
 $topicpath = array(
 	array(
 		'name' => 'パーティー予約',//名前
 		'href' => '/reservation/',//パス
 		'count' => '2',//階層※2階層目から
 	),
 );
?>

	<?php require_once('common/inc/top.php');?>
	<article id="Reservation" class="reservation step05">
		<header class="base">
			<h2 class="page_tit">
				<img src="<?php echo ROOT;?>/img/reservation/title.png" width="240" height="92" alt="パーティー予約">
			</h2>
		</header>

		<div class="contentsWrap">
			<div class="mod_wrap01">
				<section class="contents">
					<div class="flow_box">
						<p class="flow04"><img src="<?php echo ROOT;?>/img/reservation/flow04.png" width="543" height="56" alt=""></p>
					</div>
					<h3 class="sec_tit"><span>ご予約が完了しました。</span></h3>
					<div class="mod_wrap04">


						<div class="info_box">
							<p class="price">お支払合計：0,000円</p>
							<div class="wrap">
								<p>1. 上記金額は割引前の金額合計です。<br> 2. 割引等により差額が発生する場合は、会場にて返金致します。</p>
							</div>


							<dl class="atn_box">
								<dt><span>下記ご確認ください。</span></dt>
								<dd>
									<dl>
										<dt class="deco01">ご予約された内容をメールにて送信しておりますのでご確認ください。</dt>
										<dd>また、数分経ってもメールが届かない場合は、弊社まで必ずお電話ください。（03-5206-8288）<br> ドメイン受信をされている方は、「info@statusparty.jp」を受信できるように設定してください。
										</dd>
									</dl>
								</dd>
								<dd>
									<dl>
										<dt class="deco01">フリーメール（Yahoo!メール、hotmailなど）をお使いの皆様へ</dt>
										<dd>予約確認メールが迷惑メールフォルダに自動的に振り分けられる場合がございます。<br>メールが受信されない場合は、迷惑メールフォルダもご確認ください。</dd>
									</dl>
								</dd>
							</dl>

						</div>

					</div>
				</section>
			</div>
		</div>
	</article>
	<?php require_once('common/inc/bottom.php');?>