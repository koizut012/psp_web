<?php
$thisurl=dirname(__FILE__);require_once($_SERVER['DOCUMENT_ROOT'].'/common/inc/config.php');
$page_include_title = 'よくあるご質問｜婚活パーティー・恋活パーティー・エリートとの出会いならPREMIUM STATUS PARTY';
$page_include_description = '婚活パーティー「プレミアムステイタス」寄せられるよくあるご質問です。プライベートライフの向上を目的とした恋活パーティー・出会いパーティを開催しておりますのでお気軽にお問い合わせください。';
$page_include_keywords = '婚活パーティー・恋活パーティー,東京,大阪,名古屋';
$page_include_robots = '';
$page_include_canonical = '/faq/';
$page_include_h1 = 'よくある質問 | 東京、大阪、名古屋での婚活パーティー・恋活パーティー・エリートとの出会いなら、プレミアムステイタスパーティー';
$localCSS = array(
	ROOT.'/s_css/faq.css',
);
$localJS = array(
	//ROOT.'/js/index.js',
);

//ページカテゴリ
$page_cat = 'faq';

// パンくず
 $topicpath = array(
	array(
		'name' => 'よくあるご質問',//名前
		'href' => '/faq/',//パス
		'count' => '2',//階層※2階層目から
	),
);
?>

	<?php require_once('common/inc/top.php');?>
	<article id="Faq" class="faq">
		<header class="mv" data-lf-area data-lf-pc="<?php echo ROOT;?>/img/faq/mv.jpg" data-lf-sp="<?php echo ROOT;?>/img/faq/mv_sp.jpg">
			<h2 class="page_tit">
				<img src="<?php echo ROOT;?>/img/faq/title.png" width="234" height="93" alt="よくあるご質問">
			</h2>
		</header>
		<div class="contentsWrap">
			<div class="mod_wrap05">
				<ul class="link_box">
					<li><a href="#Fashion"><span>服装・ドレスコード</span></a></li>
					<li><a href="#Certificate"><span>身分証明書</span></a></li>
					<li><a href="#Reservation"><span>予約・キャンセル規定</span></a></li>
					<li><a href="#Age"><span>参加資格・年齢</span></a></li>
					<li><a href="#Progress"><span>進行・形式</span></a></li>
					<li><a href="#Meal"><span>食事・ドリンク</span></a></li>
					<li><a href="#Discount"><span>割引・ポイントサービス</span></a></li>
					<li><a href="#Payment"><span>お支払い方法</span></a></li>
					<li><a href="#Other"><span>その他</span></a></li>
				</ul>
			</div>

			<div class="mod_wrap04">
				<section class="contents">
					<div class="detail">

						<h3 class="sec_tit02 fashion" id="Fashion"><span>服装・ドレスコード</span></h3>
						<dl class="faq_box">
							<dt>どんな服装で行けばいいですか？</dt>
							<dd>セミカジュアル程度のドレスコードでご来場をお願いしております。イメージとして、お洒落なレストランにそのまま入れる服装でお越しください。</dd>
						</dl>
                        
                        <dl class="faq_box">
							<dt>男性のファッションはどういうものが相応しいですか？</dt>
							<dd>ジャケットや襟付きのシャツなどのスマートカジュアルがお勧めです。</dd>
						</dl>
                        
						<dl class="faq_box">
							<dt>夏場は暑いのでTシャツでの参加は大丈夫ですか？</dt>
							<dd>Tシャツの場合はジャケット着用をお願いしています。</dd>
						</dl>

						<h3 class="sec_tit02 certificate" id="Certificate"><span>身分証明書</span></h3>
						<dl class="faq_box">
							<dt>資格証明書を忘れても参加出来ますか？</dt>
							<dd>大変申し訳ございません。例え参加資格に該当されていても、資格証明書のご提示が無い場合はご入場頂けません。更に、キャンセル料金も頂戴する形となります。</dd>
						</dl>
						<dl class="faq_box">
							<dt>年収のみ該当するのですが、名刺や社員証でも大丈夫でしょうか？</dt>
							<dd>大変申し訳ございません。年収が該当されている場合は、源泉徴収票や納税証明書など、公的な年収証明のみとなります。</dd>
						</dl>

						<h3 class="sec_tit02 reservation" id="Reservation"><span>予約・キャンセル規定</span></h3>
						<dl class="faq_box">
							<dt>キャンセル料は発生しますか？</dt>
							<dd>キャンセル料につきましては各パーティー企画ごとに規約が変わります。パーティースケジュールの『キャンセル料規定』記載しておりますので、各企画ごとにご確認ください。</dd>
						</dl>
                        
                        <dl class="faq_box">
							<dt>別のパーティーの予約に振替したいです。</dt>
							<dd>弊社では各企画ごとのご予約となっておりますので、お手数ですが一度予約済のパーティーをキャンセル頂き、改めて別の企画にお申し込みをお願いいたします。</dd>
						</dl>
                        
                        <dl class="faq_box">
							<dt>予約は電話でも可能ですか？</dt>
							<dd>可能ですが、通常参加費から1000円アップの料金となります。</dd>
						</dl>
                        
						<dl class="faq_box">
							<dt>前日20:00からキャンセル料金が発生する企画ですが、当日高熱を出したので、無料キャンセル可能ですか？</dt>
							<dd>大変申し訳ございません。体調が優れないということで状況はお察し致しますが、その場合でも規定のキャンセル料金が発生します。</dd>
						</dl>
                        
                        <dl class="faq_box">
							<dt>キャンセル待ちの連絡が来ましたが、既に予定を入れてしまいました。断ったらキャンセル料が発生しますか？</dt>
							<dd>正式予約以降にキャンセル規定が適応されます。キャンセル待ちの段階では特にキャンセル料は発生いたしませんのでご安心ください。</dd>
						</dl>
                        
                        
						
						<h3 class="sec_tit02 age" id="Age"><span>参加資格・年齢</span></h3>
						
                        <dl class="faq_box">
							<dt>大手企業・一流企業って具体的に何ですか？</dt>
							<dd>テキスト入ります。</dd>
						</dl>
                        
                         <dl class="faq_box">
							<dt>自分が何の資格に該当しているか分かりません。</dt>
							<dd>ご参加の該当資格につきましては、<a href="http://www.statusparty.jp/rule/" style="text-decoration: underline">【参加規約ページ】</a>より詳細をご確認ください。</dd>
						</dl>
                        
                        <dl class="faq_box">
							<dt>○○の資格はありますが、年収が足りません。</dt>
							<dd>ご参加資格につきましては、各企画ごとに記載しております資格のうち【いずれか1点】に該当されているようでしたら、ご参加は問題ございません。 必ず当日は該当の参加資格証明書と身分証明書の2点をお持ちください。確認できない場合は、ご参加をお断りしております。</dd>
						</dl>
                        
                        <dl class="faq_box">
							<dt>女性にも参加資格は必要ですか？</dt>
							<dd>一部のパーティーを除き、女性の方はご年齢が募集要項内であれば、ご職業を問わずご参加頂けます。ご職業限定の企画の際は、パーティースケジュールの『女性資格』欄に記載しておりますので、各企画ごとにご確認ください。</dd>
						</dl>
                        
                        <dl class="faq_box">
							<dt>証明書は何を持参したらいいか分かりません。</dt>
							<dd>男性・女性共に身分証明書は必ずお持ちいただいております。そちらに加え、男性は該当資格証明書をご持参いただいておりますが、該当資格によってお持ちいただくものが違いますので、<a href="http://www.statusparty.jp/rule/" style="text-decoration: underline">【参加規約ページ】</a>より詳細をご確認ください。</dd>
						</dl>
                        
                        <dl class="faq_box">
							<dt>学生や未成年でも参加出来ますか？</dt>
							<dd>高校生を除く、18歳以上の方であればご参加頂けます。ただし、アルコールドリンクはご提供いたしかねますので、何卒ご了承ください。</dd>
						</dl>
						
                         <dl class="faq_box">
							<dt>結婚暦があっても参加出来ますか？子供がいますが参加出来ますか？</dt>
							<dd>現在独身の方であれば、既婚歴やお子様の有無は問わずご参加頂けますのでご安心ください。</dd>
						</dl>
                        
						<h3 class="sec_tit02 progress" id="Progress"><span>進行・形式</span></h3>
						<dl class="faq_box">
							<dt>パーティーに遅刻してしまいます。</dt>
							<dd>随時受付を行っておりますので、遅れてのご参加でも安心して会場までお越しくださいませ。ですが、着席形式のパーティーの場合ですと、お時間によっては1対1でお話いただけない方がいらっしゃる可能性もございます、また途中退席につきましても、ご遠慮いただいておりますのでご了承ください。</dd>
						</dl>
						
						<h3 class="sec_tit02 meal" id="Meal"><span>食事・ドリンク</span></h3>
						<dl class="faq_box">
							<dt>テキスト入ります。</dt>
							<dd>テキスト入ります。</dd>
						</dl>
						
						<h3 class="sec_tit02 discount" id="Discount"><span>割引・ポイントサービス</span></h3>
						<dl class="faq_box">
							<dt>テキスト入ります。</dt>
							<dd>テキスト入ります。</dd>
						</dl>
						
						<h3 class="sec_tit02 payment" id="Payment"><span>お支払い方法</span></h3>
						
                        <dl class="faq_box">
							<dt>支払い方法が分かりません。</dt>
							<dd>WEB予約後の完了ページからそのままクレジットカード決済のお手続きをして頂くか、パーティー当日の受付時に現金でご清算をお願いしております。</dd>
						</dl>
                        
                       <dl class="faq_box">
							<dt>クレジットカード決済をしたら違う金額が出てきました。</dt>
							<dd>システムの都合上、一度定価でご清算頂いておりますが、人数割引等、差額が発生する場合は受付時にご返金をさせて頂いております。</dd>
						</dl>
						
						<h3 class="sec_tit02 other" id="Other"><span>その他</span></h3>
						<dl class="faq_box">
							<dt>初めての参加で不安です。</dt>
							<dd>テキスト入ります。</dd>
						</dl>
                        
						<dl class="faq_box">
							<dt>一人参加で不安です。</dt>
							<dd>パーティーのご参加につきましては、各企画ごとによって多少変動はございますが、およそ男性の半数近く、女性の1/3程の方がお一人参加となります。もし交流にお困りのことがございましたら、スタッフが会話のセッティング等もお手伝いしておりますので、どうぞお気軽にご参加くださいませ。</dd>
						</dl>
                        
                        <dl class="faq_box">
							<dt>荷物は預けられますか？</dt>
							<dd>有料500円にてクロークをご用意しております。その他、お手荷物は会場内には置けませんのでご注意ください。また、貴重品につきましてはご自身での管理をお願い致します。なお、企画によっては自己管理での荷物置き場（無料）をご用意する場合もございます。いずれの場合でも、貴重品含め、盗難等があっても当社及び会場側では一切責任は負いかねますのでご了承ください。</dd>
						</dl>
                        
                        <dl class="faq_box">
							<dt>領収書は出ますか？</dt>
							<dd>受付時に領収書をご希望の旨をスタッフにお伝え頂けましたら、お帰りまでに準備させて頂きます。</dd>
						</dl>
                        
					</div>
				</section>
			</div>
		</div>
	</article>
	<?php require_once('common/inc/bottom.php');?>