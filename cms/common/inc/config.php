<?php
mb_internal_encoding('UTF-8');
date_default_timezone_set('Asia/Tokyo');

define('ROOT', '');
set_include_path(get_include_path() . PATH_SEPARATOR . dirname(__FILE__) . '/../..');

define('LINK_FB', 'https://www.facebook.com/premiumstatusparty/');
define('LINK_TW', 'https://twitter.com/premiumstatus');
define('LINK_INSTA', 'https://www.instagram.com/premiumstatus/');
define('LINK_LINE', 'https://line.me/R/ti/p/%40wsx7805b');
define('LINK_AS', 'https://itunes.apple.com/jp/app/id1205369404');
define('LINK_GP', 'https://play.google.com/store/apps/details?id=jp.statusparty.spapp');

define('TEL', '#');
define('MALE', '/reservation/reservation_step1.php?id=male');//?id=male
define('FEMALE', '/reservation/reservation_step1.php?id=female');//?id=female



// define('FILE_CACHECLEAR', '?a='.rand());
define('FILE_CACHECLEAR', '');

//picture要素の一括設定用
define('SC_SP', ' media="(max-width:640px)"');//420

//ユーザーエージェント判定
class UserAgent{
	private $ua;
	private $device;
	public function set(){
		$this->ua = mb_strtolower($_SERVER['HTTP_USER_AGENT']);
		if(strpos($this->ua,'iphone') !== false){
			$this->device = 'mobile';
		}elseif(strpos($this->ua,'ipod') !== false){
			$this->device = 'mobile';
		}elseif((strpos($this->ua,'android') !== false) && (strpos($this->ua, 'mobile') !== false)){
			$this->device = 'mobile';
		}elseif((strpos($this->ua,'windows') !== false) && (strpos($this->ua, 'phone') !== false)){
			$this->device = 'mobile';
		}elseif((strpos($this->ua,'firefox') !== false) && (strpos($this->ua, 'mobile') !== false)){
			$this->device = 'mobile';
		}elseif(strpos($this->ua,'ipad') !== false){
			$this->device = 'tablet';
		}elseif((strpos($this->ua,'windows') !== false) && (strpos($this->ua, 'touch') !== false && (strpos($this->ua, 'tablet pc') == false))){
			$this->device = 'tablet';
		}elseif((strpos($this->ua,'android') !== false) && (strpos($this->ua, 'mobile') === false)){
			$this->device = 'tablet';
		}elseif((strpos($this->ua,'firefox') !== false) && (strpos($this->ua, 'tablet') !== false)){
			$this->device = 'tablet';
		}else{
			$this->device = 'others';
		}
		return $this->device;
	}
}
$ua = new UserAgent();