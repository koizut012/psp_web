<!-- /.mod_main --></main>

<footer class="mod_footer">
<?php if(isset($page_cat) && $page_cat == 'schedule_detail'):?>
    <ul class="inq_links" data-sc-sp>
        <li><a href="<?php echo ROOT; ?>/reservation/reservation_step1.php"><span><img src="/common/img/footer_btn_txt03_sp.png" width="102" height="26" alt="予約する"></span></a></li>
        <li><a href="<?php echo ROOT; ?>/schedule/"><span><img src="/common/img/footer_btn_txt01_sp.png" width="129" height="26" alt="Party検索"></span></a></li>
    </ul>
<?php elseif(isset($page_cat) && $page_cat == 'schedule'):?>
    <ul class="inq_links" data-sc-sp>
        <li><a href="<?php echo ROOT; ?>/schedule/"><span><img src="/common/img/footer_btn_txt04_sp.png" width="158" height="26" alt="Party絞り込み"></span></a></li>
        <li><a href="<?php echo ROOT; ?>/report/"><span><img src="/common/img/footer_btn_txt02_sp.png" width="124" height="26" alt="参加者の声"></span></a></li>
    </ul>
<?php else: ?>
    <ul class="inq_links" data-sc-sp>
        <li><a href="<?php echo ROOT; ?>/schedule/"><span><img src="/common/img/footer_btn_txt01_sp.png" width="129" height="26" alt="Party検索"></span></a></li>
        <li><a href="<?php echo ROOT; ?>/report/"><span><img src="/common/img/footer_btn_txt02_sp.png" width="124" height="26" alt="参加者の声"></span></a></li>
    </ul>
<?php endif; ?>
            
    <div class="upper_wrap">
        <div class="mod_wrap01">
        <dl class="recommendBnr">
        <dt><span class="en">RECOMMENDED SERVICE</span>エリート対象の立食形式婚活パーティー＆1店舗型街コン「プレミアムステイタスパーティー」の姉妹サービスです。</dt>
        <dd>
            <ul class="service_links">
            <li><a href="https://www.w-premium.jp/lp/ps/" target="blank"><img src="<?php echo ROOT;?>/common/img/service_bnr01.png" width="220" height="107" alt=""></a></li>
            <li><a href="https://mrmiss.asia/lp/ps/" target="blank"><img src="<?php echo ROOT;?>/common/img/service_bnr02.png" width="220" height="107" alt=""></a></li>
            <li><a href="https://www.statusclub.jp/" target="blank"><img src="<?php echo ROOT;?>/common/img/service_bnr03.png" width="220" height="107" alt=""></a></li>
            <li><a href="https://white-p.jp/" target="blank"><img src="<?php echo ROOT;?>/common/img/service_bnr04.png" width="220" height="107" alt=""></a></li>
            <li><a href="https://www.jieitaiclub.jp/" target="blank"><img src="<?php echo ROOT;?>/common/img/service_bnr05.png" width="220" height="107" alt=""></a></li>
            <li><a href="https://jieitai-bridal.jp/" target="blank"><img src="<?php echo ROOT;?>/common/img/service_bnr06.png" width="220" height="107" alt=""></a></li>
            <li><a href="https://1000.style/" target="blank"><img src="<?php echo ROOT;?>/common/img/service_bnr07.png" width="220" height="107" alt=""></a></li>
            <li><a href="https://www.breeze-club.jp/" target="blank"><img src="<?php echo ROOT;?>/common/img/service_bnr08.png" width="220" height="107" alt=""></a></li>
            <li><a href="http://privateclub.jp/contents/fusion_bridal/" target="blank"><img src="<?php echo ROOT;?>/common/img/service_bnr09.png" width="220" height="107" alt=""></a></li>
            <li><a href="https://premium-days.com" target="blank"><img src="<?php echo ROOT;?>/common/img/service_bnr10.png" width="220" height="107" alt=""></a></li>
        </ul>
        </dd>
    </dl>
    </div>
    </div>
    <div class="lower_wrap">
        <h1 class="footerLogo"><a href="/"><img src="<?php echo ROOT; ?>/common/img/footer_logo.png" width="342" height="22" alt="PREMIUM STATUS PARTY"></a></h1>

        <div class="contactBox">
            <div class="inq_wrap">
                <p class="link"><a href="<?php echo ROOT; ?>/inquiry/" class="mod_btn01 bgc01"><span>お問い合わせフォーム<span class="en">INQUIRY</span></span></a></p>
                <p class="cap">お返事には最大２営業日頂戴しております</p>
            </div>
            
            <dl class="tel_wrap">
                <dt class="tel"><a data-tel="0352068288"><span class="en">TEL.03-5206-8288</span></a></dt>
                <dd class="times">【受付時間】11：00-20：00 月火定休（祝日は営業）</dd>
            </dl>
            
            <div class="mod_wrap01">
                <ul class="sns_links">
                    <li><a href="<?php echo LINK_INSTA;?>" target="_blank">
                        <svg width="30" height="30">
                            <desc>Instagram</desc>
                            <use xlink:href="<?php echo ROOT;?>/common/svg/footer_sprite.svg#footer_insta"></use>
                        </svg>
                    </a></li>
                    <li><a href="<?php echo LINK_FB;?>" target="_blank">
                        <svg width="30" height="30">
                            <desc>Facebook</desc>
                            <use xlink:href="<?php echo ROOT;?>/common/svg/footer_sprite.svg#footer_fb"></use>
                        </svg>
                    </a></li>
                    <li><a href="<?php echo LINK_TW;?>" target="_blank">
                        <svg width="30" height="30">
                            <desc>Twitter</desc>
                            <use xlink:href="<?php echo ROOT;?>/common/svg/footer_sprite.svg#footer_tw"></use>
                        </svg>
                    </a></li>
                </ul>
            </div>
        </div>
        <div class="nav_wrap">
        <nav data-sc-pc>
            <ul class="mainNav">
                <li><a href="<?php echo ROOT;?>/about/"><span>プレミアムステイタスとは？</span></a></li>
                <li><a href="<?php echo ROOT;?>/schedule/"><span>パーティー検索</span></a></li>
                <li><a href="<?php echo ROOT;?>/report/"><span>パーティー報告＆参加者の声</span></a></li>
                <li><a href="<?php echo ROOT;?>/faq/"><span>よくあるご質問</span></a></li>
            </ul>
            <ul class="subNav">
                <li><a href="<?php echo ROOT; ?>/company/"><span>会社概要</span></a></li>
                <li><a href="<?php echo ROOT; ?>/offer/"><span>サポーター＆スタッフ募集</span></a></li>
                <li><a href="<?php echo ROOT; ?>/rule/"><span>参加規約</span></a></li>
                <li><a href="<?php echo ROOT; ?>/notation/"><span>特定商取引に関する法律表記</span></a></li>
                <li><a href="<?php echo ROOT; ?>/privacy/"><span>プライバシーポリシー</span></a></li>
            </ul>
            
            <ul class="areaNav">
                <li><a href="<?php echo ROOT; ?>/schedule/area.php?"><span>銀座</span></a></li>
                <li><a href="<?php echo ROOT; ?>/schedule/area.php?"><span>新宿</span></a></li>
                <li><a href="<?php echo ROOT; ?>/schedule/area.php?"><span>恵比寿</span></a></li>
                <li><a href="<?php echo ROOT; ?>/schedule/area.php?"><span>東京</span></a></li>
                <li><a href="<?php echo ROOT; ?>/schedule/area.php?"><span>目黒</span></a></li>
                <li><a href="<?php echo ROOT; ?>/schedule/area.php?"><span>六本木</span></a></li>
                <li><a href="<?php echo ROOT; ?>/schedule/area.php?"><span>横浜</span></a></li>
                <li><a href="<?php echo ROOT; ?>/schedule/area.php?"><span>名古屋</span></a></li>
                <li><a href="<?php echo ROOT; ?>/schedule/area.php?"><span>大阪</span></a></li>
                <li><a href="<?php echo ROOT; ?>/schedule/area.php?"><span>京都</span></a></li>
                <li><a href="<?php echo ROOT; ?>/schedule/area.php?"><span>神戸</span></a></li>
                <li><a href="<?php echo ROOT; ?>/schedule/area.php?"><span>福岡</span></a></li>
                <li><a href="<?php echo ROOT; ?>/schedule/area.php?"><span>札幌</span></a></li>
            </ul>
        </nav>
        <p class="copyright">&copy;2017 PREMIUM STATUS PARTY. All Rights Reserved.</p>
        </div>
        </div>
</footer>
<div class="mod_toTop_area">
    <a href="#page_top" class="mod_toTop"></a>
</div>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>
<script src="<?php echo ROOT; ?>/common/js/jquery/jquery.inview.min.js"></script>
<script src="<?php echo ROOT; ?>/common/js/jquery/perfect-scrollbar.jquery.min.js"></script>
<script src="<?php echo ROOT; ?>/common/js/gsap/TweenMax.min.js"></script>
<script src="<?php echo ROOT; ?>/common/js/common.min.js<?php echo '?update='.date('Y-m-d_H-i-s',filemtime($_SERVER['DOCUMENT_ROOT'].'/common/js/common.min.js')); ?>"></script>
<?php if(is_array($localJS)):foreach($localJS as $val):?>
<script src="<?php echo $val.'?update='.date('Y-m-d_H-i-s',filemtime($_SERVER['DOCUMENT_ROOT'].$val));?>"></script>
<?php endforeach;endif;?>
<?php if(isset($inlineJS) && $inlineJS !== ''):?>
<script>
<?php echo $inlineJS;?>
</script>
<?php endif;?>

</body>
</html>