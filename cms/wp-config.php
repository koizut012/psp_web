<?php
/**
 * WordPress の基本設定
 *
 * このファイルは、インストール時に wp-config.php 作成ウィザードが利用します。
 * ウィザードを介さずにこのファイルを "wp-config.php" という名前でコピーして
 * 直接編集して値を入力してもかまいません。
 *
 * このファイルは、以下の設定を含みます。
 *
 * * MySQL 設定
 * * 秘密鍵
 * * データベーステーブル接頭辞
 * * ABSPATH
 *
 * @link http://wpdocs.osdn.jp/wp-config.php_%E3%81%AE%E7%B7%A8%E9%9B%86
 *
 * @package WordPress
 */

// 注意:
// Windows の "メモ帳" でこのファイルを編集しないでください !
// 問題なく使えるテキストエディタ
// (http://wpdocs.osdn.jp/%E7%94%A8%E8%AA%9E%E9%9B%86#.E3.83.86.E3.82.AD.E3.82.B9.E3.83.88.E3.82.A8.E3.83.87.E3.82.A3.E3.82.BF 参照)
// を使用し、必ず UTF-8 の BOM なし (UTF-8N) で保存してください。

//echo $_SERVER['HTTP_HOST'];exit();

// ** MySQL 設定 - この情報はホスティング先から入手してください。 ** //

if( $_SERVER['HTTP_HOST'] == "test.statusparty.jp" ){
    
    define('DB_NAME', 'far1_wp4psptest');
    define('DB_USER', 'far1_wp4psptest');
    define('DB_PASSWORD', 'ZFUPcQV1H2u2');
    define('DB_HOST', 'mysql1210.xserver.jp');
    
}else{
    
    /** WordPress のためのデータベース名 */
    define('DB_NAME', 'psp_web');
    
    /** MySQL データベースのユーザー名 */
    define('DB_USER', 'psp_web');
    
    /** MySQL データベースのパスワード */
    define('DB_PASSWORD', 'psp_web');
    
    /** MySQL のホスト名 */
    define('DB_HOST', 'localhost');

}

/** データベースのテーブルを作成する際のデータベースの文字セット */
define('DB_CHARSET', 'utf8mb4');

/** データベースの照合順序 (ほとんどの場合変更する必要はありません) */
define('DB_COLLATE', '');

/**#@+
 * 認証用ユニークキー
 *
 * それぞれを異なるユニーク (一意) な文字列に変更してください。
 * {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org の秘密鍵サービス} で自動生成することもできます。
 * 後でいつでも変更して、既存のすべての cookie を無効にできます。これにより、すべてのユーザーを強制的に再ログインさせることになります。
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '+ #Nrb4p|pH% lTW)]_0@d_tqz%,UZeXw90i0xz|k~%4H[f&^AI+wB$S$2:z}PyH');
define('SECURE_AUTH_KEY',  'U4JNGZ.7(!0B%xN*W$+kw>s=q#1HyZ6GT6!6qjcv)d[WoFmb%K%kU-VFQecf0JZh');
define('LOGGED_IN_KEY',    'MZDPoa?6H[spa?0=9B;[.^T>81ND?;vKJg[1O[VJ[u81?ypfC(+vJz^-9*xma>L)');
define('NONCE_KEY',        '8*%brD7tRt0#c7mwm4:k?;aCeop/O+[^50int}nJL3o(c{+,l(nBa3.1|.+/*Z^%');
define('AUTH_SALT',        'YK]uN6:jWUZs1yG@daTH*~89EKCfB*4$Ryi%^JgOi]18pwWBw>.{{^s1]!@G6~~T');
define('SECURE_AUTH_SALT', '&#)K3pyoc!]: zr|,GS$en6-h0SJ6w.f>l0?<a_TICrpCWz[vA@C4WxY*}&Z|iby');
define('LOGGED_IN_SALT',   '/n{{plm[7=k.u1VWy:,E4^-;}P4yP!i7EklnY-UQyeIc3$0WnM%WY7QdPZ>Ku5Jr');
define('NONCE_SALT',       'NaPn7pi1WkwDod:KAcdmxn2%oEjxDa.i*XRKk1y!;u+C:><Sg7+%PFVt8`1N1vJf');

/**#@-*/

/**
 * WordPress データベーステーブルの接頭辞
 *
 * それぞれにユニーク (一意) な接頭辞を与えることで一つのデータベースに複数の WordPress を
 * インストールすることができます。半角英数字と下線のみを使用してください。
 */
$table_prefix  = 'wp_';

/**
 * 開発者へ: WordPress デバッグモード
 *
 * この値を true にすると、開発中に注意 (notice) を表示します。
 * テーマおよびプラグインの開発者には、その開発環境においてこの WP_DEBUG を使用することを強く推奨します。
 *
 * その他のデバッグに利用できる定数については Codex をご覧ください。
 *
 * @link http://wpdocs.osdn.jp/WordPress%E3%81%A7%E3%81%AE%E3%83%87%E3%83%90%E3%83%83%E3%82%B0
 */
define('WP_DEBUG', false);

/* 編集が必要なのはここまでです ! WordPress でブログをお楽しみください。 */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
    define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
