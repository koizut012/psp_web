$(function() {
	"use strict";

	var video = document.getElementById('video');
	var video_btn = document.getElementById('video-btn');
	var btn_status = 0;

	video_btn.addEventListener('click', function (){
		if(btn_status === 0){
			video.play();
			btn_status = 1;
			if($(this).hasClass('play')){$(this).removeClass('play');}
			$(this).addClass('pause');
			
			video.addEventListener('ended', function(){
				if($(video_btn).hasClass('pause')){
					$(video_btn).removeClass('pause');
				}
			});
		}else {
			video.pause();
			btn_status = 0;
			if($(this).hasClass('pause')){$(this).removeClass('pause');}
			$(this).addClass('play');
		}
	});
	
});

