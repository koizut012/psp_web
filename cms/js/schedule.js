$(function() {
	"use strict";

		//カレンダー
		(function(){
			$('[data-calender]').each(function(index, element) {
				$.datepicker.setDefaults($.datepicker.regional['ja']);
				$(element).datepicker({
					dateFormat: 'yy年mm月dd日',
					minDate: '+0d'
				}).attr({readonly:'readonly'});
			});
		})();

	//ドロワメニュー
	$('.drawer_btn').click(function(){
		if($(this).hasClass('open')){
			$('.drawer').removeClass('open');
			$(this).removeClass('open');
		}else {
			$(this).next('.drawer').addClass('open');
			$(this).addClass('open');
		}
	});
	$('.drawer_box .drawer a').click(function(){
		if($('.drawer').hasClass('open')){
			$('.drawer_btn').removeClass('open');
			$('.drawer').removeClass('open');
		}
	});


	//スペック詳細wrap
	$('.spec_box > .detail > button').click(function(){
		$(this).parent('[data-autoheight]').each(function(index, element) {
			element.style.height = '';
		});
		if($(this).parent().hasClass('open')){
			$(this).parent().removeClass('open');
		}else{
			$(this).parent().addClass('open');
		}
	});
	
	
	//SP絞り込み検索　切替
	$('.search_box > .search_btn').click(function(){
			$('.area_box .deco').addClass('open');
			$('.formArea').css('display','block');
			$(this).parent().css('display','none');
	});


	//画面サイズ変更イベント
	var switchResizeInit = true,
		switchResizeName = '',
		w,
		h,
		s;
	
	$(window).on('resize', function(){
		w = window.innerWidth || document.documentElement.clientWidth || document.body.clientWidth;
		var ua = navigator.userAgent;

		if(420 < w){//pc
			if(switchResizeName != 'pc'){
				switchResizeName = 'pc';
				switchResizeInit = true;


				//SPアコーディオン・線維持リセット
				if($('.accordion dt').hasClass('open')){
					$(this).removeClass('open');
				}
				$('.accordion dt').next('.drawer').removeAttr('style');

			}
		}else{//smart
			if(switchResizeName != 'sp'){
				switchResizeName = 'sp';
				switchResizeInit = true;
			}
		}
	});
	$(window).trigger('resize');

	//SPアコーディオン
	$('.accordion dt').on("click", function() {
		w = window.innerWidth || document.documentElement.clientWidth || document.body.clientWidth;
		if(420 < w){
		}else{
			if($(this).hasClass('open')){
				$(this).removeClass('open');
				$(this).next('.drawer').slideUp();
			}else{
				$(this).addClass('open');
				$(this).next('.drawer').slideDown();
			}
		}
	});
	
});

