$(function() {
	"use strict";

		//カレンダー
		(function(){
			$('[data-calender]').each(function(index, element) {
				$.datepicker.setDefaults($.datepicker.regional['ja']);
				$(element).datepicker({
					dateFormat: 'yy年mm月dd日',
					minDate: '+0d'
				}).attr({readonly:'readonly'});
			});
		})();
		
		//プルダウンを確認
		(function(){
			$('.select').each(function(index, element) {
				var needArea = $(element).closest('[data-need]');
				if(needArea[0]){
					function check(){
						//入力が間違っているかチェック
						if(needArea.find('.select option:selected').val() == ''){//入力されていないとき
							needArea.find('.select').removeAttr('data-need-ok');
						}else{//入力があるとき
							needArea.find('.select').attr('data-need-ok', 1);
						}
					}
					$(element).on('change', check);
					check();
				}
			});
		})();



	//インスタスライダ
	//$('.sec_insta').on('inview', function(event, isInView, visiblePartX, visiblePartY){
		//if(isInView){
			$('[data-instagram01]').each(function(index, element) {
				var instaArea = element,
					kazuhozon = 0,
					count = 6,
					instagramPHP = '/common/sns/instagram/insta_ajax.php',
					time = parseInt(80),
					w,
					h,
					s;
				$.ajax({
					type: "POST",
					url: instagramPHP,
					data: {from:kazuhozon,max:count},
					dataType: "html",
					success : function(data){
						var unique = Math.floor(Math.random()*1000000);
						var ary = [],
							now = 0,
							index = 0;
						do{
							ary.push($(data).eq(index).clone());
							now++;
							index++;
							if($(data).length <= index) index = 0;
						}while(count > now);

						for(var i = 0; i < (count/2); i++){
							var c = ary[i].clone();
							// c.attr('style', 'left:'+10*i+'%; transition-delay:'+.1*i+'s;');
							c.attr('style', 'left:'+10*i+'%;');
							$(instaArea).find('.row01').find('.content').append(c);
						}
						var c2 = $(instaArea).find('.row01').find('.content');
						/*$(instaArea).find('.row01').append(c2.clone());
						$(instaArea).find('.row01').append(c2.clone());*/

						ary.reverse();
						for(var i = 0; i < (count/2); i++){
							var c = ary[i].clone();
							// c.attr('style', 'left:'+10*i+'%; transition-delay:'+.1*i+'s;');
							c.attr('style', 'left:'+10*i+'%; transition-delay:.1s;');
							$(instaArea).find('.row02').find('.content').append(c);
						}
						var c2 = $(instaArea).find('.row02').find('.content');
						/*$(instaArea).find('.row02').append(c2.clone());
						$(instaArea).find('.row02').append(c2.clone());*/

						$(instaArea).attr('data-instagram01', unique);

						//画面サイズ変更イベント
						var oldWidth;
						$(window).on('resize.imgloop'+unique, function(){
							if($('html,body').scrollTop() !=0){
								s = $('html,body').scrollTop();
							}else{
								s = $(document).scrollTop();
							}
							w = window.innerWidth || document.documentElement.clientWidth || document.body.clientWidth;
							h = $(window).height();


							//幅のみリサイズされたとき
							if(oldWidth != w){
								oldWidth = w;

							}
						}).trigger('resize.imgloop'+unique);
						$(instaArea).addClass('view');
					}
				});
			});
			//$(this).off('inview');
		//}
	//});



//画面サイズ変更イベント
	var switchResizeInit = true,
		switchResizeName = '',
		oldWidth,
		s,
		w,
		h

	$(window).on('resize', function(){
		if($('html,body').scrollTop() !=0){
			s = $('html,body').scrollTop();
		}else{
			s = $(document).scrollTop();
		}

		w = window.innerWidth || document.documentElement.clientWidth || document.body.clientWidth;
		h = window.innerHeight;
		var mv = $('.sec_mv');
		if(420 < w){//pc
			if(switchResizeName != 'pc'){
				switchResizeName = 'pc';
				switchResizeInit = true;
				mv.removeAttr('style');//スマホのMV高さをトル
			}


		}else{//smart
//PCかスマホか切り替わった判定
			if(switchResizeName != 'sp'){
				switchResizeName = 'sp';
				switchResizeInit = true;
				TweenMax.set(mv,{height:h - 60});//MVの高さをグロナビ-画面高さに固定
			}
		}
	});
	$(window).trigger('resize');
	$(window).trigger('scroll');

//画面スクロール処理
	var stPos = 0,
		glNavUp = 0,
		saveScrollPos = 0,
		s;

	$(window).on('load scroll', function() {
	
		if($('html,body').scrollTop() !=0){
			s = $('html,body').scrollTop();
		}else{
			s = $(document).scrollTop();
		}
		
		//現在のスクロール位置
		var navL = $('.mod_header .gl_nav li a');
		var navOut = '';//$('.mod_header .gl_nav li.n01 a');//固有リンクを除外する
		
		var comInPagesArr = new Array();
		for (var i = 0; i < navL.length - navOut.length; i++) {
			var targetcomInPages = navL.eq(i).attr('href');
			if(targetcomInPages.charAt(0) == '#') {
				var targetcomInPagesTop = $(targetcomInPages).offset().top;
				var targetcomInPagesBottom = targetcomInPagesTop + $(targetcomInPages).outerHeight(true) - 30;
				comInPagesArr[i] = [targetcomInPagesTop, targetcomInPagesBottom]
			}
		};
		function nowCheck() {
			var windowScrolltop = $(window).scrollTop(); //+ 120;
			for (var i = 0; i < comInPagesArr.length; i++) {
			if (comInPagesArr[i] !== undefined) {//エラー解消までの暫定設置
				if(comInPagesArr[i][0] <= windowScrolltop && comInPagesArr[i][1] >= windowScrolltop) {
					navL.removeClass('current');
					navL.eq(i).addClass('current');
					i == comInPagesArr.length;
				}
			}
			};
		}
		nowCheck();

		var navH = 120;//$(window).height();
		var mvH = 620//navH - 120;
		var header = $('.mod_header');
		
		//console.log(navH);
		//console.log(mvH);
		
		//var side = $('.side_btn');
		if(s < mvH + 140 ) {//- 340
			header.removeClass('pc_fix');
			//side.removeClass('pc_fix');
		}else {
			header.addClass('pc_fix');
			//side.addClass('pc_fix');
		}
		if(s < navH + mvH - 40) {// - 460 
			header.addClass('pc_no_fix');
		}else {
			header.removeClass('pc_no_fix');
		}
		
	});
	$(window).trigger('scroll');

});

