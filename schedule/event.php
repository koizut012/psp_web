<?php
$thisurl=dirname(__FILE__);require_once($_SERVER['DOCUMENT_ROOT'].'/common/inc/config.php');
$page_include_title = 'パーティー検索｜出会い・婚活パーティーならPREMIUM STATUSPARTY';
$page_include_description = 'スケジュール＆予約のページです。上場企業・経営者・士業などエリート対象の婚活パーティー・恋活パーティーの検索と予約が可能です。';
$page_include_keywords = '婚活パーティー,恋活パーティー,東京,大阪,名古屋,出会い';
$page_include_robots = '';
$page_include_canonical = '/schedule/party.php?mode=party&action=search';
$page_include_h1 = '●●●●● | 東京、大阪、名古屋での出会い・婚活パーティーなら PREMIUM STATUSPARTY';
$localCSS = array(
	ROOT.'/common/js/jquery/datepicker/jquery-ui.min.css',
	ROOT.'/common/js/jquery/datepicker/jquery-ui.theme.min.css',
	ROOT.'/s_css/schedule.css',
);
$localJS = array(
	ROOT.'/common/js/jquery/datepicker/jquery-ui.min.js',
	ROOT.'/common/js/jquery/datepicker/jquery.ui.datepicker-ja.min.js',
	ROOT.'/js/schedule.js',
	ROOT.'/js/form.js',
);

//ページカテゴリ
$page_cat = 'schedule';

// パンくず
 $topicpath = array(
 	array(
 		'name' => '立食100名～300名',//名前
 		'href' => '/schedule/',//パス
 		'count' => '2',//階層※2階層目から
 	),
 );
?>

<?php require_once('common/inc/top.php');?>
<article id="Schedule" class="schedule event">
	<header class="mv" data-lf-area data-lf-pc="<?php echo ROOT;?>/img/schedule/event_mv.jpg" data-lf-sp="<?php echo ROOT;?>/img/schedule/event_mv_sp.jpg">
		<div class="title_box">
			<div>
			<div class="wrap">
				<div class="in">
					<span class="name">企画から探す</span>
					<h2 class="page_tit"><span>立食100名～300名</span></h2>
					<p class="read">当社がオススメする人気企画大人数でありながら、男性参加者はハイステイタスをクリアしている方に限定！多くの異性の中から素敵なお相手を見つけてください。</p>
				</div>
			</div>
			</div>
		</div>
	</header>
	
	<div class="contentsWrap">
		<section class="contents">
			<div class="mod_wrap03">
				<div class="area_box" data-sc-pc>
					<ul class="area_link">
						<li><a href=""><span>関東</span></a></li>
						<li class="current"><a href=""><span>東海</span></a></li>
						<li><a href=""><span>関西</span></a></li>
						<li><a href=""><span>信越・北陸</span></a></li>
						<li><a href=""><span>北海道・東北</span></a></li>
						<li><a href=""><span>九州・沖縄</span></a></li>
					</ul>
					<p class="deco"><img src="<?php echo ROOT;?>/img/schedule/detail/arealist_deco.png" width="254" height="92" alt=""></p>
				</div>

				<form id="Form" autocomplete="on" enctype="multipart/form-data" name="form" method="post" action="./" class="formArea">
				<h2 class="sec_tit"><span>絞り込み検索</span></h2>
					<div class="rowWrap">
						<dl class="element" data-need="" data-form-unique="selectArea">
							<dt class="inline"><span>エリア</span></dt>
							<dd>
								<div class="itemWrap selectWrap selectAreaWrap">
									<span class="select"><select id="selectArea" name="selectArea">
									<option value="">選択してください</option>
									<option value="プルダウン１">プルダウン１</option>
									<option value="プルダウン２">プルダウン２</option>
									<option value="プルダウン３">プルダウン３</option>
									<optgroup label=""></optgroup>
									</select>
									</span>
								</div>
							</dd>
						</dl>
					</div>
					<div class="rowWrap">
						<dl class="element">
							<dt><span>日付</span></dt>
							<dd>
									<div class="itemWrap textWrap calender01_wrap" data-form-unique="calender01">
									<label for="calender01"><input name="calender01" id="calender01" value="" data-calender placeholder="" type="text" class="cal"></label></div>

									<div class="itemWrap textWrap calender02_wrap" data-form-unique="calender02">
									<label for="calender02"><input name="calender02" id="calender02" value="" data-calender placeholder="" type="text" class="cal"></label></div>
							</dd>
						</dl>
						<dl class="element" data-need="" data-need-needcheckval="1" data-form-unique="checkbox_week">
							<dt><span>曜日</span></dt>
							<dd>
								<div class="itemWrap checkboxWrap checkbox_weekBox">
									<span class="checkbox">
										<input type="checkbox" name="checkbox_week[]" value="月" id="checkbox_week0">
										<label for="checkbox_week0"><span>月</span></label>
									</span>
									<span class="checkbox">
										<input type="checkbox" name="checkbox_week[]" value="火" id="checkbox_week1">
										<label for="checkbox_week1"><span>火</span></label>
									</span>
									<span class="checkbox">
										<input type="checkbox" name="checkbox_week[]" value="水" id="checkbox_week2">
										<label for="checkbox_week2"><span>水</span></label>
									</span>
									<span class="checkbox">
										<input type="checkbox" name="checkbox_week[]" value="木" id="checkbox_week3">
										<label for="checkbox_week3"><span>木</span></label>
									</span>
									<span class="checkbox">
										<input type="checkbox" name="checkbox_week[]" value="金" id="checkbox_week4">
										<label for="checkbox_week4"><span>金</span></label>
									</span>
									<span class="checkbox">
										<input type="checkbox" name="checkbox_week[]" value="土" id="checkbox_week5">
										<label for="checkbox_week5"><span>土</span></label>
									</span>
									<span class="checkbox">
										<input type="checkbox" name="checkbox_week[]" value="日" id="checkbox_week6">
										<label for="checkbox_week6"><span>日</span></label>
									</span>
								</div>
							</dd>
						</dl>
					</div>
					<div class="rowWrap col3">
						<dl class="element"  data-need="" data-form-unique="radio_sex">
							<dt class="inline"><span><small>ご自身の</small>性別</span></dt>
							<dd>
								<div class="itemWrap radioWrap radio_sexBox">
									<span class="radio">
										<input type="radio" name="radio_sex"  value="男性" id="radio_sex0">
										<label for="radio_sex0"><span>男性</span></label>
									</span>
									<span class="radio">
										<input type="radio" name="radio_sex"  value="女性" id="radio_sex1">
										<label for="radio_sex1"><span>女性</span></label>
									</span>
								</div>
							</dd>
						</dl>
						<div class="wrap">
						<dl class="element inline" data-need="" data-form-unique="selectOwnAge">
							<dt><span><small>ご自身の</small>年齢</span></dt>
							<dd>
								<div class="itemWrap selectWrap selectOwnAgeWrap">
									<span class="select"><select id="selectOwnAge" name="selectOwnAge"  >
									<option value="">選択してください</option>
									<option value="プルダウン１">プルダウン１</option>
									<option value="プルダウン２">プルダウン２</option>
									<option value="プルダウン３">プルダウン３</option>
									<optgroup label=""></optgroup>
									</select>
									</span>
								</div>
							</dd>
						</dl>
						<dl class="element inline" data-need="" data-form-unique="selectPartnerAge">
							<dt><span><small>お相手の</small>年代</span></dt>
							<dd>
								<div class="itemWrap selectWrap selectPartnerAgeWrap">
									<span class="select"><select id="selectPartnerAge" name="selectPartnerAge"  >
									<option value="">選択してください</option>
									<option value="プルダウン１">プルダウン１</option>
									<option value="プルダウン２">プルダウン２</option>
									<option value="プルダウン３">プルダウン３</option>
									<optgroup label=""></optgroup>
									</select>
									</span>
								</div>
							</dd>
						</dl>
						</div>
					</div>
					<div class="rowWrap">
						<dl class="element" data-need="" data-form-unique="selectType">
							<dt class="inline"><span>形式</span></dt>
							<dd>
								<div class="itemWrap selectWrap selectTypeWrap">
									<span class="select"><select id="selectType" name="selectType">
									<option value="">選択してください</option>
									<option value="プルダウン１">プルダウン１</option>
									<option value="プルダウン２">プルダウン２</option>
									<option value="プルダウン３">プルダウン３</option>
									<optgroup label=""></optgroup>
									</select></span>
								</div>
							</dd>
						</dl>
						<dl class="element" data-need="" data-form-unique="selectScale">
							<dt class="inline"><span>開催規模</span></dt>
							<dd>
								<div class="itemWrap selectWrap selectScaleWrap">
									<span class="select"><select id="selectScale" name="selectScale">
									<option value="">選択してください</option>
									<option value="プルダウン１">プルダウン１</option>
									<option value="プルダウン２">プルダウン２</option>
									<option value="プルダウン３">プルダウン３</option>
									<optgroup label=""></optgroup>
									</select></span>
								</div>
							</dd>
						</dl>
					</div>
					<div class="keywordWrap">
						<dl class="element"  data-need="" data-form-unique="text_Keyword">
							<dt><span>キーワード検索</span></dt>
							<dd>
								<div class="itemWrap textWrap text_KeywordBox"><input type="text" name="text_Keyword" id="text_Keyword"  value=""></div>
							</dd>
						</dl>
					</div>
					<label for="searchBtn" class="search mod_btn01 bgc04">
						<input id="searchBtn" value="この条件で検索" type="submit">
					</label>
				</form>
				<div class="search_box">
					<button class="search_btn"><span>絞り込み検索</span></button>
				</div>
				
				<div class="detail">
					<div class="result_box">
						<div class="wrap">
							<ul class="pager tp03">
								<li class="count"><span>1 <span>/ 3</span></span></li>
								<?php /*<li class="back"><a href=""><span>前のページへ</span></a></li>*/ ;?>
								<li class="next"><a href=""><span>次のページへ</span></a></li>
							</ul>
							<p class="result">検索結果<span>154件</span></p>
						</div>
						<div class="detail" data-sc-pc>
							<p>企画から探す：<span>立食100名～300名</span></p>
						</div>
					</div>

					<div class="event_box" data-lf-area>
						<div class="header">
							<p class="date">10/18(水)<span class="time">20:00～22:00</span></p>
							<p class="place"><span class="area">東京・銀座</span>Cafe Julliet</p>
						</div>

						<div class="detail">
							<div class="left">
								<p class="type_box">
									<span class="type01">立食形式</span>
									<span class="type02">着席形式</span>
									<span class="type03">自衛隊企画</span>
								</p>
								<h3 class="title"><a href="./detail.php"><span>50名着席全員会話☆スマートビジネスマン</span></a></h3>
								<p class="spec">男性35歳以下医師・上場・年収700万以上vs女性32歳以下パーティー</p>
								<figure><a href="./detail.php">
									<div class="ph" data-lf="<?php echo ROOT;?>/img/schedule/detail/ph01.jpg"></div>
								</a></figure>
							</div>
							<div class="right">
								<div class="wrap">
									<dl class="spec_box male">
									<dt><span>男性</span></dt>
									<dd class="status"><span class="type01">予約受付中</span></dd>
									<dd class="age" data-autoheight="age"><span>20代30代</span></dd>
									<dd class="price" data-autoheight="price">
										<p>5,500円<br data-sc-sp><span>アプリ登録者</span></p>
										<p>6,000円<br data-sc-sp><span>アプリ登録無</span></p>
									</dd>
									<dd class="detail" data-autoheight="detail">
										<p class="desc">【独身者限定】<br>医師・経営者・大手企業・公務員・会計士・上場…</p>
										<p>【独身者限定】<br>医師・経営者（資本金1000万円以上または売上1億円以上※資本金の場合は会社謄本orお貴社ホームページ提示、売上の場合は決算書提示）・弁護士・税理士・会計士・公務員・上場企業・大手企業・年収700万円以上</p>
										<button><span>詳細</span><span class="close">閉じる</span></button>
									</dd>
									<dd class="entry">
										<a href="<?php echo MALE;?>" class="mod_btn06"><span>男性予約</span></a>
									</dd>
								</dl>
									<dl class="spec_box female">
									<dt><span>女性</span></dt>
									<dd class="status"><span class="type03">まもなく定員</span></dd>
									<dd class="age" data-autoheight="age"><span>20代30代</span></dd>
									<dd class="price" data-autoheight="price">
										<p>2,500円<br data-sc-sp><span>2名以上予約</span></p>
										<p>6,000円<br data-sc-sp><span>1名予約</span></p>
									</dd>
									<dd class="detail" data-autoheight="detail">
										<p class="desc">【独身者限定】<br>OL・受付・秘書・看護師など（職業は限定…</p>
										<p>【独身者限定】<br>OL・受付・秘書・看護師など（職業は限定しておりません）</p>
										<button><span>詳細</span><span class="close">閉じる</span></button>
									</dd>
									<dd class="entry">
										<a href="<?php echo FEMALE;?>" class="mod_btn06"><span>女性予約</span></a>
									</dd>
								</dl>
								</div>
								<div class="drawer_box">
									<button class="drawer_btn"><span>友達に教える</span></button>
									<ul class="drawer">
										<li class="line"><a href=""><img src="<?php echo ROOT;?>/img/schedule/detail/drawer_btn_line.png" width="121" height="30" alt="LINEで送る"></a></li>
										<li class="mail"><a href=""><img src="<?php echo ROOT;?>/img/schedule/detail/drawer_btn_mail.png" width="121" height="30" alt="Mailで送る"></a></li>
										<li class="fb"><a href=""><img src="<?php echo ROOT;?>/img/schedule/detail/drawer_btn_messenger.png" width="73" height="30" alt="送信"></a></li>
									</ul>
								</div>
								<div class="link">
									<a href="./detail.php" class="mod_btn05"><span>詳細をみる</span></a>
								</div>
							</div>
						</div>
					</div><!--/.event_box-->

					<div class="event_box" data-lf-area>
						<div class="header">
							<p class="date">10/18(水)<span class="time">20:00～22:00</span></p>
							<p class="place"><span class="area">東京・銀座</span>Cafe Julliet</p>
						</div>
						<div class="detail">
							<div class="left">
								<p class="type_box">
									<span class="type01">立食形式</span>
									<span class="type02">着席形式</span>
									<span class="type03">自衛隊企画</span>
								</p>
								<h3 class="title"><a href="./detail.php"><span>50名着席全員会話☆スマートビジネスマン</span></a></h3>
								<p class="spec">男性35歳以下医師・上場・年収700万以上vs女性32歳以下パーティー</p>
								<figure><a href="./detail.php">
									<div class="ph" data-lf="<?php echo ROOT;?>/img/schedule/detail/ph01.jpg"></div>
								</a></figure>
							</div>
							<div class="right">
								<div class="wrap">
									<dl class="spec_box male">
									<dt><span>男性</span></dt>
									<dd class="status"><span class="type02">予約枠残り1名</span></dd>
									<dd class="age" data-autoheight="age"><span>20代30代</span></dd>
									<dd class="price" data-autoheight="price">
										<p>5,500円<br data-sc-sp><span>アプリ登録者</span></p>
										<p>6,000円<br data-sc-sp><span>アプリ登録無</span></p>
									</dd>
									<dd class="detail" data-autoheight="detail">
										<p class="desc">【独身者限定】<br>医師・経営者・大手企業・公務員・会計士・上場…</p>
										<p>【独身者限定】<br>医師・経営者（資本金1000万円以上または売上1億円以上※資本金の場合は会社謄本orお貴社ホームページ提示、売上の場合は決算書提示）・弁護士・税理士・会計士・公務員・上場企業・大手企業・年収700万円以上</p>
										<button><span>詳細</span><span class="close">閉じる</span></button>
									</dd>
									<dd class="entry">
										<a href="<?php echo MALE;?>" class="mod_btn06 cancel"><span><small>男性</small>キャンセル待ち申込</span></a>
									</dd>
								</dl>
									<dl class="spec_box female">
									<dt><span>女性</span></dt>
									<dd class="status"><span class="type03">まもなく定員</span></dd>
									<dd class="age" data-autoheight="age"><span>20代30代</span></dd>
									<dd class="price" data-autoheight="price">
										<p>2,500円<br data-sc-sp><span>2名以上予約</span></p>
										<p>6,000円<br data-sc-sp><span>1名予約</span></p>
									</dd>
									<dd class="detail" data-autoheight="detail">
										<p class="desc">【独身者限定】<br>OL・受付・秘書・看護師など（職業は限定…</p>
										<p>【独身者限定】<br>OL・受付・秘書・看護師など（職業は限定しておりません）</p>
										<button><span>詳細</span><span class="close">閉じる</span></button>
									</dd>
									<dd class="entry">
										<a href="<?php echo FEMALE;?>" class="mod_btn06 cancel"><span><small>女性</small>キャンセル待ち申込</span></a>
									</dd>
								</dl>
								</div>
								<div class="drawer_box">
									<button class="drawer_btn"><span>友達に教える</span></button>
									<ul class="drawer">
										<li class="line"><a href=""><img src="<?php echo ROOT;?>/img/schedule/detail/drawer_btn_line.png" width="121" height="30" alt="LINEで送る"></a></li>
										<li class="mail"><a href=""><img src="<?php echo ROOT;?>/img/schedule/detail/drawer_btn_mail.png" width="121" height="30" alt="Mailで送る"></a></li>
										<li class="fb"><a href=""><img src="<?php echo ROOT;?>/img/schedule/detail/drawer_btn_messenger.png" width="73" height="30" alt="送信"></a></li>
									</ul>
								</div>
								<div class="link">
									<a href="./detail.php" class="mod_btn05"><span>詳細をみる</span></a>
								</div>
							</div>
						</div>
					</div><!--/.event_box-->

					<div class="event_box" data-lf-area>
						<div class="header">
							<p class="date">10/18(水)<span class="time">20:00～22:00</span></p>
							<p class="place"><span class="area">東京・銀座</span>Cafe Julliet</p>
						</div>

						<div class="detail">
							<div class="left">
								<p class="type_box">
									<span class="type01">立食形式</span>
									<span class="type02">着席形式</span>
									<span class="type03">自衛隊企画</span>
								</p>
								<h3 class="title"><a href="./detail.php"><span>50名着席全員会話☆スマートビジネスマン</span></a></h3>
								<p class="spec">男性35歳以下医師・上場・年収700万以上vs女性32歳以下パーティー</p>
								<figure><a href="./detail.php">
									<div class="ph" data-lf="<?php echo ROOT;?>/img/schedule/detail/ph01.jpg"></div>
								</a></figure>
							</div>
							<div class="right">
								<div class="wrap">
									<dl class="spec_box male">
									<dt><span>男性</span></dt>
									<dd class="status"><span class="type01">予約受付中</span></dd>
									<dd class="age" data-autoheight="age"><span>20代30代</span></dd>
									<dd class="price" data-autoheight="price">
										<p>5,500円<br data-sc-sp><span>アプリ登録者</span></p>
										<p>6,000円<br data-sc-sp><span>アプリ登録無</span></p>
									</dd>
									<dd class="detail" data-autoheight="detail">
										<p class="desc">【独身者限定】<br>医師・経営者・大手企業・公務員・会計士・上場…</p>
										<p>【独身者限定】<br>医師・経営者（資本金1000万円以上または売上1億円以上※資本金の場合は会社謄本orお貴社ホームページ提示、売上の場合は決算書提示）・弁護士・税理士・会計士・公務員・上場企業・大手企業・年収700万円以上</p>
										<button><span>詳細</span><span class="close">閉じる</span></button>
									</dd>
									<dd class="entry">
										<a href="<?php echo MALE;?>" class="mod_btn06"><span>男性予約</span></a>
									</dd>
								</dl>
									<dl class="spec_box female">
									<dt><span>女性</span></dt>
									<dd class="status"><span class="type01">予約受付中</span></dd>
									<dd class="age" data-autoheight="age"><span>20代30代</span></dd>
									<dd class="price" data-autoheight="price">
										<p>2,500円<br data-sc-sp><span>2名以上予約</span></p>
										<p>6,000円<br data-sc-sp><span>1名予約</span></p>
									</dd>
									<dd class="detail" data-autoheight="detail">
										<p class="desc">【独身者限定】<br>OL・受付・秘書・看護師など（職業は限定…</p>
										<p>【独身者限定】<br>OL・受付・秘書・看護師など（職業は限定しておりません）</p>
										<button><span>詳細</span><span class="close">閉じる</span></button>
									</dd>
									<dd class="entry">
										<a href="<?php echo FEMALE;?>" class="mod_btn06"><span>女性予約</span></a>
									</dd>
								</dl>
								</div>
								<div class="drawer_box">
									<button class="drawer_btn"><span>友達に教える</span></button>
									<ul class="drawer">
										<li class="line"><a href=""><img src="<?php echo ROOT;?>/img/schedule/detail/drawer_btn_line.png" width="121" height="30" alt="LINEで送る"></a></li>
										<li class="mail"><a href=""><img src="<?php echo ROOT;?>/img/schedule/detail/drawer_btn_mail.png" width="121" height="30" alt="Mailで送る"></a></li>
										<li class="fb"><a href=""><img src="<?php echo ROOT;?>/img/schedule/detail/drawer_btn_messenger.png" width="73" height="30" alt="送信"></a></li>
									</ul>
								</div>
								<div class="link">
									<a href="./detail.php" class="mod_btn05"><span>詳細をみる</span></a>
								</div>
							</div>
						</div>
					</div><!--/.event_box-->

					<div class="result_box">
						<div class="wrap">
							<ul class="pager tp03">
								<li class="count"><span>1 <span>/ 3</span></span></li>
								<?php /*<li class="back"><a href=""><span>前のページへ</span></a></li>*/ ;?>
								<li class="next"><a href=""><span>次のページへ</span></a></li>
							</ul>
							<p class="result">検索結果<span>154件</span></p>
						</div>
					</div>
				</div>
			</div>
		</section>
	</div>
</article>
<?php require_once('common/inc/bottom.php');?>
