<?php
$thisurl=dirname(__FILE__);require_once($_SERVER['DOCUMENT_ROOT'].'/common/inc/config.php');
$page_include_title = 'イベントタイトルが入ります｜出会い・婚活パーティーならPREMIUM STATUSPARTY';
$page_include_description = '';
$page_include_keywords = '婚活パーティー,恋活パーティー,東京,大阪,名古屋,出会い';
$page_include_robots = '';
$page_include_canonical = '/schedule/party.php?mode=party&action=search';
$page_include_h1 = '●●●●● | 東京、大阪、名古屋での出会い・婚活パーティーなら PREMIUM STATUSPARTY';
$localCSS = array(
	ROOT.'/common/js/jquery/datepicker/jquery-ui.min.css',
	ROOT.'/common/js/jquery/datepicker/jquery-ui.theme.min.css',
	ROOT.'/s_css/schedule.css',
);
$localJS = array(
	ROOT.'/common/js/jquery/datepicker/jquery-ui.min.js',
	ROOT.'/common/js/jquery/datepicker/jquery.ui.datepicker-ja.min.js',
	ROOT.'/js/form.js',
	ROOT.'/js/schedule.js',
);

//ページカテゴリ
$page_cat = 'schedule_detail';

// パンくず
 $topicpath = array(
 	array(
 		'name' => 'パーティー検索',//名前
 		'href' => '/schedule/index.php',//パス
 		'count' => '2',//階層※2階層目から
 	),
 	array(
 		'name' => '関東',//名前
 		'href' => '/schedule/area.php',//パス
 		'count' => '3',
 	),
		array(
 		'name' => '新宿',//名前
 		'href' => '/schedule/area.php',//パス
 		'count' => '4',
 	),
		array(
 		'name' => 'パーティ日付とタイトル入ります',//名前
 		//'href' => '/schedule/',//パス
 		'count' => '5',
 	),
 );
?>

	<?php require_once('common/inc/top.php');?>

	<article id="Schedule" class="schedule detail">
		<header class="base">
			<h2 class="page_tit">
				<img src="<?php echo ROOT;?>/img/schedule/detail/detail_title.png" width="240" height="93" alt="パーティー詳細">
			</h2>
		</header>

		<div class="contentsWrap">
			<div class="mod_wrap01">
				<section class="contents main">
					<div class="detail">
						<div class="event_box" data-lf-area>
							<div class="header">
								<p class="date">10/18(水)<span class="time">20:00～22:00</span></p>
								<p class="place"><span class="area">東京・銀座</span>Cafe Julliet</p>
							</div>

							<div class="wrap">
								<header>
									<div class="left">
										<p class="type_box">
											<span class="type01">立食形式</span>
											<span class="type02">着席形式</span>
											<span class="type03">自衛隊企画</span>
										</p>
										<h2 class="title"><span>50名着席全員会話☆スマートビジネスマン</span></h2>
										<p class="spec">男性35歳以下医師・上場・年収700万以上vs女性32歳以下パーティー</p>
									</div>
									<div class="right" data-sc-pc>
										<p class="male"><a href="<?php echo MALE;?>" class="mod_btn06"><span>男性予約</span></a></p>
										<p class="female"><a href="<?php echo FEMALE;?>" class="mod_btn06 cancel"><span><small>女性</small>キャンセル待ち申込</span></a></p>
										<div class="drawer_box">
											<button class="drawer_btn"><span>友達に教える</span></button>
											<ul class="drawer">
												<li class="line"><a href=""><img src="<?php echo ROOT;?>/img/schedule/detail/drawer_btn_line.png" width="121" height="30" alt="LINEで送る"></a></li>
												<li class="mail"><a href=""><img src="<?php echo ROOT;?>/img/schedule/detail/drawer_btn_mail.png" width="121" height="30" alt="Mailで送る"></a></li>
												<li class="fb"><a href=""><img src="<?php echo ROOT;?>/img/schedule/detail/drawer_btn_messenger.png" width="73" height="30" alt="送信"></a></li>
											</ul>
										</div>
									</div>
								</header>
								<figure class="ph" data-lf="<?php echo ROOT;?>/img/schedule/detail/ph01.jpg"></figure>
								<div class="spec_box_wrap">
									<dl class="male">
										<dt><span>男性</span></dt>
										<dd>
											<div class="status" data-autoheight="status">
												<p class="sec_title spNon"><span>予約状況</span></p>
												<div class="wrap">
													<span class="type01">予約受付中</span>
												</div>
											</div>
											<div class="age" data-autoheight="age">
												<p class="sec_title"><span>年齢</span></p>
												<div class="wrap">
													<span>20代30代</span>
												</div>
											</div>
											<div class="price" data-autoheight="price">
												<p class="sec_title"><span>料金</span></p>
												<div class="wrap">
													<p>5,500円<br data-sc-sp><span>アプリ登録者</span></p>
													<p>6,000円<br data-sc-sp><span>アプリ登録無</span></p>
													<dl class="atn">
														<dt>注意</dt>
														<dd>電話予約又は予約無しの場合は最大料金から1,000円アップ</dd>
													</dl>
												</div>
											</div>

											<span class="offSet" id="Spec"></span>
											<div class="detail" data-autoheight="detail">
												<p class="sec_title"><span>資格</span></p>
												<div class="wrap offSet">
													<p>【独身者限定】<br>医師・経営者（資本金1000万円以上または売上1億円以上※資本金の場合は会社謄本orお貴社ホームページ提示、売上の場合は決算書提示）・弁護士・税理士・会計士・公務員・上場企業・大手企業・年収700万円以上</p>
													<dl class="atn">
														<dt>提示必須</dt>
														<dd>該当証明書＋身分証明書（例：社員証＋免許証）</dd>
													</dl>
												</div>
											</div>
											<div class="entry" data-autoheight="entry">
												<p class="sec_title spNon"><span>予約申し込み</span></p>
												<a href="<?php echo MALE;?>" class="mod_btn06"><span>男性予約</span></a>
											</div>
										</dd>
									</dl>
									<dl class="female">
										<dt><span>女性</span></dt>
										<dd>
											<div class="status" data-autoheight="status">
												<p class="sec_title spNon"><span>予約状況</span></p>
												<div class="wrap">
													<span class="type03">まもなく定員</span>
												</div>
											</div>
											<div class="age" data-autoheight="age">
												<p class="sec_title"><span>年齢</span></p>
												<div class="wrap">
													<span>20代30代</span>
												</div>
											</div>
											<div class="price" data-autoheight="price">
												<p class="sec_title"><span>料金</span></p>
												<div class="wrap">
													<p>2,500円<br data-sc-sp><span>2名以上予約</span></p>
													<p>6,000円<br data-sc-sp><span>1名予約</span></p>
													<dl class="atn">
														<dt>注意</dt>
														<dd>電話予約又は予約無しの場合は最大料金から1,000円アップ</dd>
													</dl>
												</div>
											</div>
											<div class="detail" data-autoheight="detail">
												<p class="sec_title"><span>資格</span></p>
												<div class="wrap">
													<p>【独身者限定】<br>OL・受付・秘書・看護師など（職業は限定しておりません）</p>
													<dl class="atn">
														<dt>提示必須</dt>
														<dd>身分証明書</dd>
													</dl>
												</div>
											</div>
											<div class="entry" data-autoheight="entry">
												<p class="sec_title spNon"><span>予約申し込み</span></p>
												<a href="<?php echo FEMALE;?>" class="mod_btn06"><span>女性予約</span></a>
											</div>
										</dd>
									</dl>
								</div>
								<div class="detail_box">
									<dl class="accordion">
										<dt><span>概要</span></dt>
										<dd class="drawer">
											<p>旅行の好きな人、スポーツ好きな人同士の交流パーティー。もちろん旅行やスポーツに興味のある方も歓迎！<br> 共通の趣味を話題にお楽しみください。
												<br> ☆会場は恵比寿駅より徒歩０分♪期待を胸に地下へと続く階段をおりると、地下とは思えないほどの空間が広がる。店内はNYのホテルラウンジの様なLoungeに木調ピアノにオレンジ色のライトが照らされ高級感を演出！そんな高級感溢れる会場にて、ハイステイタス交流Partyを開催いたします。☆男性は大手商社（売上100億円以上又は資本金5億円以上または売上1億円以上）・弁護士・税理士・会計士・公務員・外資企業・上場企業・年収700万円以上の方と、女性20代中心のステイタス交流パーティー！※当該資格がわかる証明書提示必須※大変恐縮ではありますが、資格がわかる証明書と年齢確認できる証明書の持参無い場合は、ご来場頂いてもご入場頂く事が出来ない場合がございますのでご了承願います。☆以下の職業の方はご参加をお断りしております（ネットワークビジネス・不動産投資・各種セミナーを主催または携わっている方・各種勧誘・販売目的の方・同業のパーティーオーガナイザー）のご参加はお断りしています。
											</p>
										</dd>
									</dl>
									<dl class="accordion">
										<dt><span>ドレスコード</span></dt>
										<dd class="drawer">
											<ul class="dress">
												<li class="male"><span>男性</span>ジャケット着用or襟付シャツ着用（ポロシャツ・ネルシャツ不可）</li>
												<li class="female"><span>女性</span>ワンピースなどお洒落な服装でご参加下さい。</li>
											</ul>
											<p>【以下の服装はご参加をお断りさせて頂きます】<br>ポロシャツ・ネルシャツ、ベースボールキャップ、ワークキャップ<br>※ハット系は参加可</p>
											<p>※上記ドレスコード以外に当パーティーに相応しくない服装のお客様は現場にてお断りさせて頂く場合がございますので、予めご了承ください。</p>
										</dd>
									</dl>
									<dl class="accordion">
										<dt><span>進行・形式</span></dt>
										<dd class="drawer">
											<p>フリースタイル・スタンディング（一部席有）</p>
										</dd>
									</dl>
									<dl class="accordion">
										<dt><span>飲食</span></dt>
										<dd class="drawer">
											<p>軽食＆フリードリンク</p>
										</dd>
									</dl>
									<dl class="accordion">
										<dt><span>予約（VIP）席</span></dt>
										<dd class="drawer">
											<p>無</p>
										</dd>
									</dl>
									<dl class="accordion">
										<dt><span>喫煙</span></dt>
										<dd class="drawer">
											<p>会場内禁煙（一部禁煙コーナー設置）</p>
										</dd>
									</dl>
									<dl class="accordion">
										<dt><span>荷物置き場</span></dt>
										<dd class="drawer">
											<p>有料（500円）</p>
										</dd>
									</dl>
									<dl class="accordion">
										<dt><span>キャンセル料<br data-sc-pc>規程</span></dt>
										<dd class="drawer">
											<p>前日18:00迄にメールにて連絡頂いた場合は無料キャンセル。<br> 以降は男性6000円・女性3000円のキャンセル料金が発生致します。
												<br> なお、キャンセル料のお支払いが確認出来るまで、今後のパーティーにはご予約する事ができなくなるとともに、次回のご予約がある場合は、一度全てお取り消しさせて頂きます。
											</p>
											<p><span class="icon">注</span><br> 男性は資格がわかる証明書と年齢確認ができる証明書の持参必須！ご参加頂けない場合や参加資格に該当していない場合、ご参加をご遠慮頂くと共に定価のキャンセル料が発生いたしますので、参加条件をご確認のうえご予約願います。（該当資格について不明な点はお問い合わせください）
											</p>
										</dd>
									</dl>
									<dl class="accordion">
										<dt><span>ポイントサービス<br data-sc-pc>・各種割引</span></dt>
										<dd class="drawer">
											<p>男性1000ポイント／女性600ポイント還元♪（ポイント＝円・男性5,000円／女性3,000円以上から利用可能※ポイント利用は前日の20時までにweb予約の方に限らせて頂きます）<br> 【ポイント利用についてはこちら】
											</p>
										</dd>
									</dl>
									<dl class="accordion">
										<dt><span>参加規約</span></dt>
										<dd class="drawer">
											<p>テキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキスト</p>
										</dd>
									</dl>
									<dl class="accordion">
										<dt><span>その他</span></dt>
										<dd class="drawer">
											<p>☆【ダブルプレミアム】会員割引をご希望の場合、ご予約時に【ご質問欄】へダブルプレミアム会員である旨記載願います。割引は前日20時までのWEB予約の場合に限ります。</p>
											<p>【下記ご参加をお断りしております】<br> 既婚者・ネットワークビジネス・不動産投資・各種セミナーを主催または携わっている方・各種勧誘・販売目的の方・同業のパーティーオーガナイザーのご参加はお断りしています。
												<br>また男女の婚活恋活を目的に開催している為、同性同士の連絡先交換をお断りしています。</p>
											<p>※上記禁止事項が発覚した際、一旦入場されている場合でも、即退場頂きます。その場合、参加費や交通費等は一切返金致しません。<br>また悪質な場合には、法的措置も含めて厳しく対処せざるをえませんので、ご注意ください。</p>
										</dd>
									</dl>
								</div>
								<div class="access_box">
									<h3 class="sec_tit02"><span>会場までのアクセス</span></h3>

									<div class="access_detail">
										<figure><img src="<?php echo ROOT;?>/img/schedule/detail/shop_ph.jpg" width="330" height="157" alt=""></figure>
										<dl class="place">
											<dt>会場</dt>
											<dd>デザイナーズレストラン『Cafe Serre』</dd>
										</dl>
										<dl class="address">
											<dt>住所</dt>
											<dd>東京都中央区築地1-13-1　松竹スクエア2F</dd>
										</dl>
										<dl class="tel">
											<dt>電話</dt>
											<dd><a data-tel="0335437272">0335437272</a></dd>
										</dl>
										<dl class="web">
											<dt>HP</dt>
											<dd><a href="http://r.gnavi.co.jp/g793100/" target="_blank">http://r.gnavi.co.jp/g793100/</a></dd>
										</dl>
										<dl class="traffic">
											<dt>交通</dt>
											<dd>東銀座（浅草線/日比谷線）</dd>
										</dl>
										<dl class="access">
											<dt>アクセス</dt>
											<dd>東銀座駅5版出口を出て、そのまま晴海通りを築地方面に直進。下に高速道路がある萬年橋を超え、すぐ左手にみえるビルの二階がカフェセレとなります。</dd>
										</dl>
									</div>
									<div class="map">
										<iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d1620.6849825630582!2d139.76904003284025!3d35.667891338796906!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x973e4c93f10939fe!2zQ2FmZSBTZXJyZSDvvJzjgqvjg5Xjgqfjg7vjgrvjg6zvvJ4!5e0!3m2!1sja!2sjp!4v1512087795036" width="100%" height="350" frameborder="0" style="border:0" allowfullscreen></iframe>
									</div>
								</div>
								<div class="link">
									<p class="male"><a href="<?php echo MALE;?>" class="mod_btn06"><span>男性予約</span></a></p>
									<p class="female"><a href="<?php echo FEMALE;?>" class="mod_btn06"><span>女性予約</span></a></p>
								</div>
								<div class="drawer_box bottom">
									<button class="drawer_btn"><span>友達に教える</span></button>
									<ul class="drawer">
										<li class="line"><a href=""><img src="<?php echo ROOT;?>/img/schedule/detail/drawer_btn_line.png" width="121" height="30" alt="LINEで送る"></a></li>
										<li class="mail"><a href=""><img src="<?php echo ROOT;?>/img/schedule/detail/drawer_btn_mail.png" width="121" height="30" alt="Mailで送る"></a></li>
										<li class="fb"><a href=""><img src="<?php echo ROOT;?>/img/schedule/detail/drawer_btn_messenger.png" width="73" height="30" alt="送信"></a></li>
									</ul>
								</div>
							</div>
						</div>
						<!--/.event_box-->
						<ul class="pager tp04">
							<li class="back"><a href="./index.php"><span>パーティー検索一覧へ戻る</span></a></li>
							<?php/*<li class="next"><a href=""><span>パーティー検索一覧へ戻る</span></a></li>*/?>
						</ul>
					</div>
				</section>

				<aside class="side" data-sc-pc>
					<div class="inq_box">
						<div class="inq_wrap">
							<p class="link"><a href="<?php echo ROOT; ?>/inquiry/" class="mod_btn01 bgc01"><span>お問い合わせフォーム</span></a></p>
							<p class="cap">お返事には最大２営業日<br>頂戴しております</p>

							<?php /*<dl class="tel_wrap">
							<dt class="tel"><a data-tel="0352068288"><span class="en">TEL.03-5206-8288</span></a></dt>
							<dd class="times">【受付時間】11：00-20：00<br>月火定休（祝日は営業）</dd>
						</dl>*/ ;?>
						</div>
					</div>

					<dl class="regist_box">
						<dt><img src="<?php echo ROOT;?>/img/schedule/detail/regist_tit.png" width="192" height="44" alt="PREMIUM STATUS OFFICIAL"></dt>
						<dd>
							<p class="cp">公式アカウント無料登録で<br>ご優待情報 GET!</p>
							<a href="<?php echo LINK_AS; ?>" target="_blank" class="store"><span><img src="<?php echo ROOT;?>/common/img/regist_app_store.png" width="166" height="50" alt="AppStore"></span></a>
							<a href="<?php echo LINK_GP; ?>" target="_blank" class="store"><span><img src="<?php echo ROOT;?>/common/img/regist_google_play.png" width="166" height="50" alt="GooglePlay"></span></a>

							<div class="wrap">
								<a href="<?php echo LINK_LINE; ?>" target="_blank" class="line"><span><img src="<?php echo ROOT;?>/common/img/regist_icon_line.png" width="50" height="50" alt="" data-sc-pc><img src="<?php echo ROOT;?>/common/img/regist_icon_line_sp.png" width="182" height="47" alt="" data-sc-sp></span></a>
								<span class="qr"><img src="<?php echo ROOT;?>/common/img/regist_qr_line.png" width="86" height="86" alt=""></span>
							</div>
						</dd>
					</dl>

					<div class="search_box">
						<h3 class="cat_title"><span>エリアから探す</span></h3>
						<ul>
							<li>
								<a href="/schedule/area.php" class="mod_btn04">
									<span>関東</span>
								</a>
							</li>
							<li>
								<a href="/schedule/area.php" class="mod_btn04">
									<span>東海</span>
								</a>
							</li>
							<li>
								<a href="/schedule/area.php" class="mod_btn04">
									<span>関西</span>
								</a>
							</li>
							<li>
								<a href="/schedule/area.php" class="mod_btn04">
									<span>信越・北陸</span>
								</a>
							</li>
							<li>
								<a href="/schedule/area.php" class="mod_btn04">
									<span>北海道・東北</span>
								</a>
							</li>
							<li>
								<a href="/schedule/area.php" class="mod_btn04">
									<span>九州・沖縄</span>
								</a>
							</li>
						</ul>
					</div>

					<div class="search_box">
						<h3 class="cat_title"><span>企画から探す</span></h3>
						<ul>
							<li>
								<a href="/schedule/event.php" class="mod_btn04 large bgc01">
									<span>立食<br>100〜300名</span>
								</a>
							</li>
							<li>
								<a href="/schedule/event.php" class="mod_btn04 large bgc01">
									<span>立食<br>100名以下</span>
								</a>
							</li>
							<li>
								<a href="/schedule/event.php" class="mod_btn04 large bgc01">
									<span>着席<br>全員会話</span>
								</a>
							</li>
							<li>
								<a href="/schedule/event.php" class="mod_btn04 large bgc01">
									<span>20代<br>中心</span>
								</a>
							</li>
							<li>
								<a href="/schedule/event.php" class="mod_btn04 large bgc01">
									<span>30代40代<br>以上</span>
								</a>
							</li>
							<li>
								<a href="/schedule/event.php" class="mod_btn04 large bgc01">
									<span>特別企画</span>
								</a>
							</li>
						</ul>
					</div>

					<div class="search_box">
						<h3 class="cat_title"><span>日程から探す</span></h3>
						<div class="form_box">
							<form class="formArea" id="Form" autocomplete="on" enctype="multipart/form-data" name="form" method="post" action="./">
								<dl class="element" data-need="" data-form-unique="selectArea">
									<dt><span>開催地</span></dt>
									<dd>
										<div class="itemWrap selectWrap selectAreaWrap">
											<span class="select"><select id="selectArea" name="selectArea"  >
										<option value="">--</option>
										<option value="プルダウン１">プルダウン１</option>
										<option value="プルダウン２">プルダウン２</option>
										<option value="プルダウン３">プルダウン３</option>
										<optgroup label=""></optgroup>
										</select>
										</span>
										</div>
									</dd>
								</dl>
								<dl class="element">
									<dt><span>日付</span></dt>
									<dd>
										<div class="itemWrap textWrap calender01_wrap" data-form-unique="calender01">
											<label for="calender01"><input name="calender01" id="calender01" value="" data-calender placeholder="" type="text"></label></div>

										<div class="itemWrap textWrap calender02_wrap" data-form-unique="calender02">
											<label for="calender02"><input name="calender02" id="calender02" value="" data-calender placeholder="" type="text"></label></div>
									</dd>
								</dl>
								<dl class="element" data-need="" data-need-needcheckval="1" data-form-unique="checkbox_week">
									<dt><span>曜日</span></dt>
									<dd>
										<div class="itemWrap checkboxWrap checkbox_weekBox">
											<span class="checkbox">
												<input type="checkbox" name="checkbox_week[]" value="月" id="checkbox_week0">
												<label for="checkbox_week0"><span>月</span></label>
											</span>
											<span class="checkbox">
												<input type="checkbox" name="checkbox_week[]" value="火" id="checkbox_week1">
												<label for="checkbox_week1"><span>火</span></label>
											</span>
											<span class="checkbox">
												<input type="checkbox" name="checkbox_week[]" value="水" id="checkbox_week2">
												<label for="checkbox_week2"><span>水</span></label>
											</span>
											<span class="checkbox">
												<input type="checkbox" name="checkbox_week[]" value="木" id="checkbox_week3">
												<label for="checkbox_week3"><span>木</span></label>
											</span>
											<span class="checkbox">
												<input type="checkbox" name="checkbox_week[]" value="金" id="checkbox_week4">
												<label for="checkbox_week4"><span>金</span></label>
											</span>
											<span class="checkbox">
												<input type="checkbox" name="checkbox_week[]" value="土" id="checkbox_week5">
												<label for="checkbox_week5"><span>土</span></label>
											</span>
											<span class="checkbox">
												<input type="checkbox" name="checkbox_week[]" value="日" id="checkbox_week6">
												<label for="checkbox_week6"><span>日</span></label>
											</span>
										</div>
									</dd>
								</dl>
								<label for="searchBtn" class="search mod_btn01 bgc04">
								<input id="searchBtn" value="検 索" type="submit">
							</label>
							</form>
						</div>
					</div>

					<div class="search_box">
						<h3 class="cat_title"><span>詳細から探す</span></h3>
						<div class="link">
							<a href="/schedule/" class="mod_btn04 bgc01"><span>検 索</span></a>
						</div>
					</div>
				</aside>
			</div>
		</div>
	</article>
	<?php require_once('common/inc/bottom.php');?>