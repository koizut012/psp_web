<?php
$thisurl=dirname(__FILE__);require_once($_SERVER['DOCUMENT_ROOT'].'/common/inc/config.php');
$page_include_title = 'パーティー報告＆参加者の声｜出会い・婚活パーティーならPREMIUM STATUSPARTY';
$page_include_description = '';
$page_include_keywords = '異業種交流会,東京,異業種,交流会,出会いパーティー';
$page_include_robots = '';
$page_include_canonical = '/report/detail.php';
$page_include_h1 = '●●●● | 東京、大阪、名古屋での異業種交流・出会い・婚活パーティーならPREMIUM STATUS PARTYへ';
$localCSS = array(
	ROOT.'/s_css/report.css',
);
$localJS = array(
	//ROOT.'/js/index.js',
);

//ページカテゴリ
$page_cat = 'report';

// パンくず
 $topicpath = array(
	array(
		'name' => 'パーティー報告＆参加者の声',//名前
		'href' => '/report/',//パス
		'count' => '2',//階層※2階層目から
	),
	array(
		'name' => 'タイトル入ります',//名前
		//'href' => '/report/',//パス
		'count' => '3',//階層※2階層目から
	),
);
?>

	<?php require_once('common/inc/top.php');?>
	<article id="Report" class="report">
		<header class="base">
			<h2 class="page_tit">
					<img src="<?php echo ROOT;?>/img/report/detail_title.png" width="457" height="93" alt="パーティー報告＆参加者の声">
			</h2>
		</header>

		<div class="contentsWrap">
			<div class="mod_wrap01">
				<section class="contents main">
					<div class="detail interview<?php /*other class .fashion .category*/ ;?>">
						<p class="category">インタビュー</p>
						<header>
							<h2 class="title">【名古屋】土曜の夜は栄の隠れ家で大人のパーティー♪♪<br>好評トヨタ系企画！</h2>
							<p class="date">開催日：<span>2017.09.23 SAT 19:00～21:30</span></p>
							<p class="place">会　場：<span>Party&amp;Dining『FAMILLA』</span></p>
						</header>

						<div class="item_box">
							<figure class="ph main" data-lf-area data-lf="<?php echo ROOT;?>/img/report/detail/ph01.jpg"></figure>
							<p>みなさんこんにちは！ 9月も終盤に差し掛かり、本格的に秋を感じられるようになって来ましたが、いかがお過ごしですか？真夏の暑さが過ぎ去り、過ごしやすくなってきましたが、案外日中と朝晩で気温差があったりするので、着る服や体調管理には気を付けたい時期ですね。 そんな中、秋分の日の9月23日(土・祝)に開催致しましたパーティーのご報告をさせて頂きたいと思います^ ^ 会場は栄の中心エリアにある、party&amp;dining『FAMILLA』にて。 男女比も抜群♪男性はトヨタ系企業の方中心で、女性は20代の方中心の沢山のお客様がご来場下さいました^ ^ 受付をお済ませ頂き皆さんお揃いの頃、それぞれ異性の方と乾杯をしてパーティースタート！お酒やお料理を楽しみつつ、ご歓談に入っていきます！ 今回の会場では、色々とおしゃれなアルコール類が揃っているのも嬉しいポイントですね♪ そして、最初こそ少し緊張感も見受けられたものの、途中のゲームタイムで盛り上がったりするうち、気付けばとても和やかなムードに包まれていました^ ^ パーティーは未経験で中々手が出しづらいという方も、いざ参加してみると意外と楽しめるかも！？ ゲームなど場の盛り上がりももちろんですが、私共スタッフも出来る限りのお手伝いをさせて頂ければと思っておりますので、迷っている方はぜひ一度、会場に足を運んで頂ければと思います☆ 今週もたくさんのお客様のご参加、誠にありがとうございました！ またのお越しをスタッフの一同心からお待ちしております♪</p>

							<div class="ph_box">
								<figure><img src="<?php echo ROOT;?>/img/report/detail/ph02.jpg" width="244" height="143" alt=""></figure>
								<figure><img src="<?php echo ROOT;?>/img/report/detail/ph03.jpg" width="244" height="143" alt=""></figure>
								<figure><img src="<?php echo ROOT;?>/img/report/detail/ph04.jpg" width="244" height="143" alt=""></figure>
							</div>
						</div>
					</div>
					<ul class="pager tp02">
						<li class="back"><a href="./index.php"><span>パーティー報告＆参加者の声　一覧へ戻る</span></a></li>
						<?php/*<li class="next"><a href=""><span>パーティー報告＆参加者の声　一覧へ戻る</span></a></li>*/?>
					</ul>
				</section>

				<aside class="side">
					<div class="side_box">
						<h3 class="cat_title"><span>カテゴリ</span></h3>
						<ul class="cat_link">
							<li class="interview"><a href="./index.php"><span>インタビュー</span></a></li>
							<li class="fashion"><a href="./index.php"><span>ファッション</span></a></li>
							<li class="report"><a href="./index.php"><span>レポート</span></a></li>
						</ul>
					</div>

					<div class="side_box">
						<h3 class="cat_title"><span>最新記事</span></h3>
						<ul class="item_link">
							<li class="interview">
								<a href="./detail.php">
									<p class="category"><span>インタビュー</span></p>
									<h3 class="title">【名古屋】土曜の夜は栄の隠れ家で大人のパーティー♪♪<br>好評トヨタ系企画！</h3>
								</a>
							</li>

							<li class="fashion">
								<a href="./detail.php">
									<p class="category"><span>ファッション</span></p>
									<h3 class="title">【名古屋】土曜の夜は栄の隠れ家で大人のパーティー♪♪<br>好評トヨタ系企画！</h3>
								</a>
							</li>

							<li class="report">
								<a href="./detail.php">
									<p class="category"><span>レポート</span></p>
									<h3 class="title">【名古屋】土曜の夜は栄の隠れ家で大人のパーティー♪♪<br>好評トヨタ系企画！</h3>
								</a>
							</li>

							<li class="fashion">
								<a href="./detail.php">
									<p class="category"><span>ファッション</span></p>
									<h3 class="title">【名古屋】土曜の夜は栄の隠れ家で大人のパーティー♪♪<br>好評トヨタ系企画！</h3>
								</a>
							</li>

							<li class="fashion">
								<a href="./detail.php">
									<p class="category"><span>ファッション</span></p>
									<h3 class="title">【名古屋】土曜の夜は栄の隠れ家で大人のパーティー♪♪<br>好評トヨタ系企画！</h3>
								</a>
							</li>
						</ul>
					</div>

					<div class="side_box">
						<h3 class="cat_title"><span>過去の記事</span></h3>
						<form action="./" class="formArea">
							<div class="itemWrap selectWrap selectMonthWrap">
								<span class="select"><select id="selectMonth" name="selectMonth">
								<option value="">月を検索</option>
								<option value="1月">1月</option>
								<option value="2月">2月</option>
								<option value="3月">3月</option>
								<option value="4月">4月</option>
								<option value="5月">5月</option>
								<option value="6月">6月</option>
								<option value="7月">7月</option>
								<option value="8月">8月</option>
								<option value="9月">9月</option>
								<option value="10月">10月</option>
								<option value="11月">11月</option>
								<option value="12月">12月</option>
								<optgroup label=""></optgroup>
							</select></span>
							</div>
						</form>
					</div>
				</aside>
			</div>
		</div>
	</article>
	<?php require_once('common/inc/bottom.php');?>