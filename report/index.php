<?php
$thisurl=dirname(__FILE__);require_once($_SERVER['DOCUMENT_ROOT'].'/common/inc/config.php');
$page_include_title = 'パーティー報告＆参加者の声｜出会い・婚活パーティーならPREMIUM STATUSPARTY';
$page_include_description = '「プレミアムステイタスクラブ」のパーティー報告＆参加者の声ページです。プライベートライフの向上を目的とした婚活パーティー・出会いをお届け。お気軽にお問い合わせください。';
$page_include_keywords = '異業種交流会,東京,異業種,交流会,出会いパーティー';
$page_include_robots = '';
$page_include_canonical = '/report/index.php';
$page_include_h1 = 'パーティー報告&参加者の声 | 東京、大阪、名古屋での異業種交流・出会い・婚活パーティーならPREMIUM STATUS PARTYへ';
$localCSS = array(
	ROOT.'/s_css/report.css',
);
$localJS = array(
	//ROOT.'/js/index.js',
);

//ページカテゴリ
$page_cat = 'report_top';

// パンくず
 $topicpath = array(
	array(
		'name' => 'パーティー報告＆参加者の声',//名前
		'href' => '/report/',//パス
		'count' => '2',//階層※2階層目から
	),
);
?>

	<?php require_once('common/inc/top.php');?>
	<article id="Report" class="report">
		<header class="mv" data-lf-area data-lf-pc="<?php echo ROOT;?>/img/report/mv.jpg" data-lf-sp="<?php echo ROOT;?>/img/report/mv_sp.jpg">
			<h2 class="page_tit">
					<img src="<?php echo ROOT;?>/img/report/title.png" width="457" height="93" alt="パーティー報告＆参加者の声">
			</h2>
		</header>

		<div class="contentsWrap">
			<div class="mod_wrap01">
				<p class="catch">婚活パーティー「プレミアムステイタス」の<br data-sc-pc>パーティー報告、参加者の声、お勧めパーティーファッションをご覧頂けます。</p>
				
				<section class="contents main index">
				<div class="detail">
					<div class="voice_box interview" data-pc-autoheight="vc">
						<a href="./detail.php" class="wrapLink"></a>
						<figure data-lf-area data-lf="<?php echo ROOT;?>/img/report/ph01.jpg" class="ph"></figure>
						<div class="detail" data-pc-autoheight="vc_wrap">
							<h2 class="tit">【名古屋】土曜日の夜は栄えの隠れ家で大人のパーティ♪♪<br>好評トヨタ系企業企画！</h2>
						</div>
						<p class="link"><a href="./detail.php" class="more_btn"><span>続きを読む</span></a></p>
					</div>
					<div class="voice_box fashion" data-pc-autoheight="vc">
						<a href="./detail.php" class="wrapLink"></a>
						<figure data-lf-area data-lf="<?php echo ROOT;?>/img/report/ph02.jpg" class="ph"></figure>
						<div class="detail" data-pc-autoheight="vc_wrap">
							<h2 class="tit">パーティーお勧めファッション<br>タイトル入ります。</h2>
						</div>
						<p class="link"><a href="./detail.php" class="more_btn"><span>続きを読む</span></a></p>
					</div>
					<div class="voice_box report" data-pc-autoheight="vc">
						<a href="./detail.php" class="wrapLink"></a>
						<figure data-lf-area data-lf="<?php echo ROOT;?>/img/report/ph03.jpg" class="ph"></figure>
						<div class="detail" data-pc-autoheight="vc_wrap">
							<h2 class="tit">【名古屋】土曜日の夜は栄えの隠れ家で大人のパーティ♪♪<br>好評トヨタ系企業企画！</h2>
						</div>
						<p class="link"><a href="./detail.php" class="more_btn"><span>続きを読む</span></a></p>
					</div>
					<div class="voice_box interview" data-pc-autoheight="vc">
						<a href="./detail.php" class="wrapLink"></a>
						<figure data-lf-area data-lf="<?php echo ROOT;?>/img/report/ph01.jpg" class="ph"></figure>
						<div class="detail" data-pc-autoheight="vc_wrap">
							<h2 class="tit">【名古屋】土曜日の夜は栄えの隠れ家で大人のパーティ♪♪<br>好評トヨタ系企業企画！</h2>
						</div>
						<p class="link"><a href="./detail.php" class="more_btn"><span>続きを読む</span></a></p>
					</div>
					<div class="voice_box fashion" data-pc-autoheight="vc">
						<a href="./detail.php" class="wrapLink"></a>
						<figure data-lf-area data-lf="<?php echo ROOT;?>/img/report/ph02.jpg" class="ph"></figure>
						<div class="detail" data-pc-autoheight="vc_wrap">
							<h2 class="tit">パーティーお勧めファッション<br>タイトル入ります。</h2>
						</div>
						<p class="link"><a href="./detail.php" class="more_btn"><span>続きを読む</span></a></p>
					</div>
					<div class="voice_box report" data-pc-autoheight="vc">
						<a href="./detail.php" class="wrapLink"></a>
						<figure data-lf-area data-lf="<?php echo ROOT;?>/img/report/ph03.jpg" class="ph"></figure>
						<div class="detail" data-pc-autoheight="vc_wrap">
							<h2 class="tit">【名古屋】土曜日の夜は栄えの隠れ家で大人のパーティ♪♪<br>好評トヨタ系企業企画！</h2>
						</div>
						<p class="link"><a href="./detail.php" class="more_btn"><span>続きを読む</span></a></p>
					</div>
					<div class="voice_box interview" data-pc-autoheight="vc">
						<a href="./detail.php" class="wrapLink"></a>
						<figure data-lf-area data-lf="<?php echo ROOT;?>/img/report/ph01.jpg" class="ph"></figure>
						<div class="detail" data-pc-autoheight="vc_wrap">
							<h2 class="tit">【名古屋】土曜日の夜は栄えの隠れ家で大人のパーティ♪♪<br>好評トヨタ系企業企画！</h2>
						</div>
						<p class="link"><a href="./detail.php" class="more_btn"><span>続きを読む</span></a></p>
					</div>
					<div class="voice_box fashion" data-pc-autoheight="vc">
						<a href="./detail.php" class="wrapLink"></a>
						<figure data-lf-area data-lf="<?php echo ROOT;?>/img/report/ph02.jpg" class="ph"></figure>
						<div class="detail" data-pc-autoheight="vc_wrap">
							<h2 class="tit">パーティーお勧めファッション<br>タイトル入ります。</h2>
						</div>
						<p class="link"><a href="./detail.php" class="more_btn"><span>続きを読む</span></a></p>
					</div>
					<div class="voice_box report" data-pc-autoheight="vc">
						<a href="./detail.php" class="wrapLink"></a>
						<figure data-lf-area data-lf="<?php echo ROOT;?>/img/report/ph03.jpg" class="ph"></figure>
						<div class="detail" data-pc-autoheight="vc_wrap">
							<h2 class="tit">【名古屋】土曜日の夜は栄えの隠れ家で大人のパーティ♪♪<br>好評トヨタ系企業企画！</h2>
						</div>
						<p class="link"><a href="./detail.php" class="more_btn"><span>続きを読む</span></a></p>
					</div>
					<div class="voice_box interview" data-pc-autoheight="vc">
						<a href="./detail.php" class="wrapLink"></a>
						<figure data-lf-area data-lf="<?php echo ROOT;?>/img/report/ph01.jpg" class="ph"></figure>
						<div class="detail" data-pc-autoheight="vc_wrap">
							<h2 class="tit">【名古屋】土曜日の夜は栄えの隠れ家で大人のパーティ♪♪<br>好評トヨタ系企業企画！</h2>
						</div>
						<p class="link"><a href="./detail.php" class="more_btn"><span>続きを読む</span></a></p>
					</div>
					<div class="voice_box fashion" data-pc-autoheight="vc">
						<a href="./detail.php" class="wrapLink"></a>
						<figure data-lf-area data-lf="<?php echo ROOT;?>/img/report/ph02.jpg" class="ph"></figure>
						<div class="detail" data-pc-autoheight="vc_wrap">
							<h2 class="tit">パーティーお勧めファッション<br>タイトル入ります。</h2>
						</div>
						<p class="link"><a href="./detail.php" class="more_btn"><span>続きを読む</span></a></p>
					</div>
					<div class="voice_box report" data-pc-autoheight="vc">
						<a href="./detail.php" class="wrapLink"></a>
						<figure data-lf-area data-lf="<?php echo ROOT;?>/img/report/ph03.jpg" class="ph"></figure>
						<div class="detail" data-pc-autoheight="vc_wrap">
							<h2 class="tit">【名古屋】土曜日の夜は栄えの隠れ家で大人のパーティ♪♪<br>好評トヨタ系企業企画！</h2>
						</div>
						<p class="link"><a href="./detail.php" class="more_btn"><span>続きを読む</span></a></p>
					</div>
				</div>

				<ul class="pager tp01">
					<?php /*<li class="back"><a href=""><span>前の12件へ</span></a></li>*/ ;?>
					<?php /*<li class="home"><a href=""><span>一覧へ戻る</span></a></li>*/ ;?>
					<li class="next"><a href=""><span>次の12件へ</span></a></li>
				</ul>
				</section>
				
				<aside class="side">
					<div class="side_box">
						<h3 class="cat_title"><span>カテゴリ</span></h3>
						<ul class="cat_link">
							<li class="interview"><a href="./index.php"><span>インタビュー</span></a></li>
							<li class="fashion"><a href="./index.php"><span>ファッション</span></a></li>
							<li class="report"><a href="./index.php"><span>レポート</span></a></li>
						</ul>
					</div>

					<div class="side_box">
						<h3 class="cat_title"><span>最新記事</span></h3>
						<ul class="item_link">
							<li class="interview">
								<a href="./detail.php">
									<p class="category"><span>インタビュー</span></p>
									<h3 class="title">【名古屋】土曜の夜は栄の隠れ家で大人のパーティー♪♪<br>好評トヨタ系企画！</h3>
								</a>
							</li>

							<li class="fashion">
								<a href="./detail.php">
									<p class="category"><span>ファッション</span></p>
									<h3 class="title">【名古屋】土曜の夜は栄の隠れ家で大人のパーティー♪♪<br>好評トヨタ系企画！</h3>
								</a>
							</li>

							<li class="report">
								<a href="./detail.php">
									<p class="category"><span>レポート</span></p>
									<h3 class="title">【名古屋】土曜の夜は栄の隠れ家で大人のパーティー♪♪<br>好評トヨタ系企画！</h3>
								</a>
							</li>

							<li class="fashion">
								<a href="./detail.php">
									<p class="category"><span>ファッション</span></p>
									<h3 class="title">【名古屋】土曜の夜は栄の隠れ家で大人のパーティー♪♪<br>好評トヨタ系企画！</h3>
								</a>
							</li>

							<li class="fashion">
								<a href="./detail.php">
									<p class="category"><span>ファッション</span></p>
									<h3 class="title">【名古屋】土曜の夜は栄の隠れ家で大人のパーティー♪♪<br>好評トヨタ系企画！</h3>
								</a>
							</li>
						</ul>
					</div>

					<div class="side_box">
						<h3 class="cat_title"><span>過去の記事</span></h3>
						<form action="./" class="formArea">
							<div class="itemWrap selectWrap selectMonthWrap">
								<span class="select"><select id="selectMonth" name="selectMonth">
								<option value="">月を検索</option>
								<option value="1月">1月</option>
								<option value="2月">2月</option>
								<option value="3月">3月</option>
								<option value="4月">4月</option>
								<option value="5月">5月</option>
								<option value="6月">6月</option>
								<option value="7月">7月</option>
								<option value="8月">8月</option>
								<option value="9月">9月</option>
								<option value="10月">10月</option>
								<option value="11月">11月</option>
								<option value="12月">12月</option>
								<optgroup label=""></optgroup>
							</select></span>
							</div>
						</form>
					</div>
				</aside>
			</div>
		</div>
	</article>
	<?php require_once('common/inc/bottom.php');?>