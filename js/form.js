$(function() {
	"use strict";

		//カレンダー
		(function(){
			$('[data-calender]').each(function(index, element) {
				$.datepicker.setDefaults($.datepicker.regional['ja']);
				$(element).datepicker({
					dateFormat: 'yy年mm月dd日（D）',
					minDate: '+0d'
				}).attr({readonly:'readonly'});
			});
		})();
		
		//プルダウンを確認
		(function(){
			$('.select').each(function(index, element) {
				var needArea = $(element).closest('[data-need]');
				if(needArea[0]){
					function check(){
						//入力が間違っているかチェック
						if(needArea.find('.select option:selected').val() == ''){//入力されていないとき
							needArea.find('.select').removeAttr('data-need-ok');
						}else{//入力があるとき
							needArea.find('.select').attr('data-need-ok', 1);
						}
					}
					$(element).on('change', check);
					check();
				}
			});
		})();


});

