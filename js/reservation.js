$(function() {
	//"use strict";

		//reservation_step1
		// URLのパラメータを取得
		if($('#Reservation').hasClass('step01')){
			var urlParam = location.search.substring(1);
			if(urlParam) {
				var param = urlParam.split('&');
				var paramArray = [];
				for (i=0; i < param.length; i++) {
					var paramItem = param[i].split('=');
					paramArray[paramItem[0]] = paramItem[1];
				}
				if (paramArray.id == 'male') {
					$('#Member').css('display','block');
					$('.tabMenu li:nth-child(1)').addClass('current');
					$('#Male').css('display','block');
					$('#Female').css('display','none');
					$("#radio_sex0").attr("checked", true);
				} else {
					$('#Member').css('display','block');
					$('.tabMenu li:nth-child(1)').addClass('current');
					$('#Male').css('display','none');
					$('#Female').css('display','block');
					$("#radio_sex1").attr("checked", true);
				}
			}
		}
		
		
		$('.tabMenu li').click(function() {
		var index = $('.tabMenu li').index(this);

		$('.tabArea').css('display','none');
		$('.tabArea').eq(index).css('display','block');
		$('.tabMenu li').removeClass('current');
		$(this).addClass('current')
	});
		
		//reservation_step1 2
		$('input[name="radio_sex"]:radio').change(function(){
			var radioval = $(this).val();
			if(radioval == 'male'){
				$('#Male').css('display','block');
				$('#Female').css('display','none');
			}else	if(radioval == 'female'){
				$('#Male').css('display','none');
				$('#Female').css('display','block');
			}else{
				$('#Male').css('display','none');
				$('#Female').css('display','none');
			}
		});

		//reservation_step4
		$('input[name="radio_pay"]:radio').change(function(){
			var radioval = $(this).val();
			if(radioval == 'cash'){
				$('#Cash').css('display','block');
				$('#Credit').css('display','none');
				$('#Cvs').css('display','none');
			}else if(radioval == 'credit'){
				$('#Cash').css('display','none');
				$('#Credit').css('display','block');
				$('#Cvs').css('display','none');
			}else if(radioval == 'cvs'){
				$('#Cash').css('display','none');
				$('#Credit').css('display','none');
				$('#Cvs').css('display','block');
			}else{
				$('#Cash').css('display','none');
				$('#Credit').css('display','none');
				$('#Cvs').css('display','none');
			}
		});
});

